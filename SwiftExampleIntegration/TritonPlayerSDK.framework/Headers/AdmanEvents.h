/**
* Copyright 2019 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AdmanEvents_h
#define AdmanEvents_h

@import UIKit;

#define NOTIFICATION_ADMAN_MESSAGE @"NotificationMessageEventAdman"
#define NOTIFICATION_ADMAN_MESSAGE_KEY @"adman_message"
typedef NS_ENUM (NSUInteger, AdmanMessageEventType) {
    AdmanMessageEventFetchingInfo = 1,
    AdmanMessageEventPreloading,
    AdmanMessageEventNone,
    AdmanMessageEventError,
    AdmanMessageEventReady,
    AdmanMessageEventStarted,
    AdmanMessageEventStopped,
    AdmanMessageEventAlmostCompleted,
    AdmanMessageEventCompleted,
    AdmanMessageEventSkipped
};

@interface NotificationAdmanBase : NSObject
@property (nonatomic, assign) enum AdmanMessageEventType event;

+ (void)sendEvent:(AdmanMessageEventType)event;
+ (nonnull NSString*)getEventString:(AdmanMessageEventType) event;
@end

#define NOTIFICATION_ADMAN_PLAYER @"NotificationMessageEventAdmanPlayer"
#define NOTIFICATION_ADMAN_PLAYER_KEY @"adman_player"
typedef NS_ENUM (NSUInteger, AdmanPlayerEventType) {
    AdmanPlayerEventPrepare = 1,
    AdmanPlayerEventReady,
    AdmanPlayerEventFailed,
    AdmanPlayerEventPlaying,
    AdmanPlayerEventPlay,
    AdmanPlayerEventPause,
    AdmanPlayerEventProgress,
    AdmanPlayerEventComplete,
    AdmanPlayerEventCloseable,
    AdmanPlayerEventSkippable
};

@interface NotificationAdmanPlayer : NSObject
@property (nonatomic, assign, readonly) enum AdmanPlayerEventType event;
@property (nonatomic, assign, nullable, readonly) id userData;

- (_Null_unspecified id)initWith:(AdmanPlayerEventType)event andUserData:(nullable id)userData;

+ (void)sendEvent:(AdmanPlayerEventType)event;
+ (void)sendEvent:(AdmanPlayerEventType)event userData:(nullable id)userData;
+ (nonnull NSString*)getEventString:(AdmanPlayerEventType)event;
@end

#define NOTIFICATION_ADMAN_VOICE @"NotificationMessageEventAdmanVoice"
#define NOTIFICATION_ADMAN_VOICE_KEY @"adman_voice"
typedef NS_ENUM (NSUInteger, AdmanVoiceEventType) {
    AdmanVoiceEventResponsePlaying = 1,
    AdmanVoiceEventInteractionStarted,
    AdmanVoiceEventInteractionEnded,
    AdmanVoiceEventError,
    AdmanVoiceEventRecognizerStarted,
    AdmanVoiceEventRecognizerStopped
};

@interface NotificationAdmanVoice : NSObject
@property (nonatomic, assign) enum AdmanVoiceEventType event;

+ (void)sendEvent:(AdmanVoiceEventType)event;
+ (nonnull NSString*)getEventString:(AdmanVoiceEventType)event;
@end

#define NOTIFICATION_ADMAN_CONTROL @"NotificationMessageEventAdmanControl"
#define NOTIFICATION_ADMAN_CONTROL_KEY @"adman_control"
typedef NS_ENUM (NSUInteger, AdmanControlEventType) {
    AdmanControlEventPrepare=1,
    AdmanControlEventStart,
    AdmanControlEventPause,
    AdmanControlEventResume,
    AdmanControlEventSkip,
    AdmanControlEventClick,
    AdmanControlEventLink,
    AdmanControlEventSwipeBack,
    AdmanControlEventPositive,
    AdmanControlEventNegative
};

@interface NotificationAdmanControl : NSObject
@property (nonatomic, assign) enum AdmanControlEventType event;
@property (nonatomic, assign, nullable, readonly) id userData;

- (_Null_unspecified id)initWith:(AdmanControlEventType)event andUserData:(nullable id)userData;

+ (void)sendEvent:(AdmanControlEventType)event;
+ (void)sendEvent:(AdmanControlEventType)event userData:(nullable id)userData;
+ (nonnull NSString*)getEventString:(AdmanControlEventType)event;
@end

#endif /* AdmanEvents_h */
