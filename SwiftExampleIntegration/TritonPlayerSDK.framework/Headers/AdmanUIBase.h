/**
* Copyright 2019 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

@import UIKit;
#import <Foundation/Foundation.h>
#import "AdmanBanner.h"

@interface AdmanUIBase : UIViewController

@property (nullable) UIViewController *rootVC;

@property (nullable) UIView *microphoneStatus;
@property (nullable) UIImageView *voiceRequestAnimation;

@property (nullable) UIImageView *bannerView;
@property (nullable) UIImageView *closeView;
@property (nullable) UIImageView *playView;
@property (nullable) UIImageView *rewindView;

@property (nullable) UILabel *remainingTime;

@property (nullable) UILabel *intentLabel;
@property (nullable) UIImageView *positiveView;
@property (nullable) UIImageView *negativeView;

+ (void)addSelector:(nonnull SEL)sel forView:(nonnull UIView *)view withTarget:(nonnull id)delegate;
+ (nonnull UIImage *)loadImageFrom:(nonnull NSString *)base64;

- (void)initBaseView;
- (void)initBaseComponents;

- (void)initVoiceView;
- (void)initVoiceComponents;

- (void)update:(nonnull AdmanBanner *)ad;
- (void)show:(nonnull UIViewController *)rootVC;
- (void)hide;

- (void)rotateByOrientation;
- (void)rotateVertical;
- (void)rotateHorizontal;

- (void)showPauseButton:(nullable UITapGestureRecognizer *)tapGestureRecognizer;
- (void)showPlayButton:(nullable UITapGestureRecognizer *)tapGestureRecognizer;

- (void)admanPlayerNotification:(nonnull NSNotification *)notification;
- (void)admanVoiceNotification:(nonnull NSNotification *)notification;
@end
