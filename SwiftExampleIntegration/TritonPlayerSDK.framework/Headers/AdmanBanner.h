/**
 * Copyright 2019 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@import UIKit;
@import AVFoundation;
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AdmanPreloadMode) {
    AdmanPreloadModeProgressive,
    AdmanPreloadModeFull
};

@interface AdmanMediaFile : NSObject

@property NSInteger bitrate;
@property (nonnull) NSString *type;
@property (nonnull) NSString *url;

- (_Null_unspecified id)initWithSource:(NSString * _Nonnull)source type:(NSString * _Nonnull)type andBitrate:(NSInteger)bitrate;

@end
/**
 Voice action type for voice ad (audio, link, phone)
 */
@interface AdmanResponseValue : NSObject
/**
 Value type
 */
@property (nonnull) NSString *type;
/**
 @property data 
 */
@property (nullable) NSObject *data;

- (id _Nullable)initWithType:(NSString * _Nonnull)type andData:(NSString * _Nullable)data;

@end

@interface AdmanInteraction : NSObject

@property NSInteger priority;
@property (nonnull) NSString *action;
/**
 @property type Possible types: positive, negative, silence, skip, unknown
 */
@property (nonnull) NSString *type;
/**
 @property values collection with core data for action after response
 */
@property (nonnull) NSMutableArray<AdmanResponseValue *> *values;
/**
 @property keywords
 */
@property (nonnull) NSMutableArray<NSString *> *keywords;
- (id _Nullable)initWithAction:(NSString * _Nonnull)action andType:(NSString * _Nonnull)type;

@end

@interface AdmanCreative : NSObject
/**
@property statistics<NSString*, NSArray*<NSString*>> stat data from Creative tracking
*/
@property (nonnull, strong) NSMutableDictionary<NSString *, NSMutableArray<NSString *> *> *tracking;

@property (nonnull) NSString *staticResource;
/**
 @property urlToNavigateOnClick link to open in internal browser
 */
@property (nullable) NSString *urlToNavigateOnClick;

@end

/**
 Instreamatic creative
 */
@interface AdmanBanner : NSObject

@property (nullable) NSString *adId;
@property (nullable) NSString *adSystem;
/**
 @property expirationTime Ad expiration time after preload request, in seconds
 Default value - 5 minuts
 */
@property NSInteger expirationTime;
/**
 @property sourceUrl link to audio ad
 */
@property (nullable) NSString *sourceUrl;
/**
 @property introUrl link to ad intro
 */
@property (nullable) NSString *introUrl;

/**
 @property bitrate audio ad bitrate
 */
@property NSInteger bitrate;
/**
 @property duration of audio ad
 */
@property NSInteger duration;
/**
 @property minVolume audio volume cannot be lower then this value
 */
@property NSInteger minVolume;

@property (nullable) NSString *adText;
/**
 @property image url for banner
 */
@property (nullable) NSString *url;
/**
 @property urlToNavigateOnClick link to open in internal browser
 */
@property (nullable) NSString *urlToNavigateOnClick;
/**
 @property media collection of all media links
 */
@property (nonnull) NSArray<AdmanMediaFile *> *media;

/**
 Companion ads list
    key - string formatted as HxW (e.g. 640x640, 350x200)
    value - image url
 */
@property (nullable) NSDictionary<NSString *, AdmanCreative *> *companionAds;
/**
 @property bannerImage ad image created for default size when ad show on device display
 */
@property (nullable) UIImage *bannerImage;
/**
 @property adCache UIImage in binary format for default image size
 */
@property (nullable) NSData *adCache;

/**
 @property statistics<NSString*, NSArray*<NSString*>> collection of all root VAST stat events to report
 */
@property (nonnull) NSMutableDictionary<NSString *, NSArray<NSString *> *> *statistics;

/**
 @property responses collection for all voice interactions
 */
@property (nullable) NSDictionary<NSString *, NSArray<AdmanInteraction *> *> *responses;

/**
 Time until microphone will be disabled on voice interaction step
 */
@property NSInteger responseTime;

@property (nonnull) NSMutableDictionary<NSString *, id> *assets;

@property bool showControls;
@property bool isClickable;

- (bool)isVoiceAd;

@end

// type NSString *
#define AdmanBannerAssetMicOnUrl @"micOnUrl"
#define AdmanBannerAssetMicOffUrl @"micOffUrl"
// type AVAsset *
#define AdmanBannerAssetMicOnCache @"micOnCache"
#define AdmanBannerAssetMicOffCache @"micOffCache"
