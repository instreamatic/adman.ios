//
//  AdmanAdLoader.h
//  TritonPlayerSDK
//
//  Created by dev account on 13.12.2020.
//  Copyright © 2020 Triton Digital. All rights reserved.
//


#import "Adman.h"

@interface TritonPlayer (AdmanCategory)<AdmanDelegate>

/**
  Init adman sdk player and start preloading content
 */
- (void)initAdmanWithSiteId:(NSInteger)siteId andRegion:(NSInteger)region;
- (void)initAdmanWithURL:(NSString *)url;
/**
   Start adman sdk ad playback, methid [TritonPlayer play] will be called when playback ended
 */
- (void)startAdmanPlayer;
/**
   Stop adman sdk ad playback
 */
- (void)stopAdmanPlayer;

@end
