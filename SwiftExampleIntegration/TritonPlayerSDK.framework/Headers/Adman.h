/**
 * Copyright 2019 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@import AVFoundation;
@import UIKit;
#import "AdmanBanner.h"
#import "AdmanUIBase.h"

typedef NS_ENUM (NSUInteger, AdmanSectionType) {
    sPreroll = 0,
    sMidroll,
    sPostroll
};

@class Adman;

@protocol AdmanDelegate<NSObject>

@required
/**
 Method called when adman state changed
 @param sender Adman instance
 */
- (void)admanStateDidChange:(nonnull Adman *) sender;

@optional
/**
 Only for voice recognition.
 @param result recognized word or phrase, empty string if not
 */
- (void)phraseRecognized:(nonnull NSDictionary *)result;
- (void)customVoiceIntentHandler;

@optional
/**
 Triggered when image tapped by user
 @param urlToNavigate url, opened in internal browser
 */
- (void)bannerTouched:(nullable NSString *)urlToNavigate;
/**
 Triggered when default ad view(with control playback buttons) closed
 */
- (void)viewClosed;
- (void)errorReceived:(nullable NSError *)error;
/**
 Triggered when dtmf label recived
 */
- (void)dtmfIn;
- (void)dtmfOut;
/**
 Triggered only for external debug
 */
- (void)log:(nonnull NSString*)message;
@end

@interface Adman : NSObject<UIGestureRecognizerDelegate, NSXMLParserDelegate>

typedef NS_ENUM (NSUInteger, AdmanState) {
    /** default adman state */
    AdmanStateInitial = 0,
    /** preloading and parse creative from vast/json source */
    AdmanStateFetchingInfo,
    /** ad can be played */
    AdmanStateReadyForPlayback,
    /** If creative contain more than one ad, ad changed when playback completed */
    AdmanStateAdChangedForPlayback,
    AdmanStatePlaying,
    AdmanStatePaused,
    AdmanStatePlaybackCompleted,
    AdmanStateStopped,
    /** only for voice ads */
    AdmanStateVoiceResponsePlaying,
    AdmanStateVoiceInteractionStarted,
    AdmanStateVoiceInteractionEnded,
    /** error occurred during loading/playback, 11 */
    AdmanStateError,
    AdmanStateTypeMismatch,
    AdmanStatePreloading,
    AdmanStateSpeechRecognizerStarted,
    AdmanStateSpeechRecognizerStopped,
    /** no ads for this siteId, region, audio format or other params */
    AdmanStateAdNone
};

typedef NS_ENUM (NSUInteger, AdmanDebugMode) {
    /** test creative with image and audio */
    AdmanDebugBanner = 11,
    /** test creative with audio */
    AdmanDebugAudio = 12,
    /** test voice creative */
    AdmanDebugVoiceAd = 41,
    /** debug mode disabled */
    AdmanDebugNone = 0
};

typedef NS_ENUM (NSUInteger, AdmanRegion) {
    AdmanRegionEU = 0,
    AdmanRegionUS,
    AdmanRegionGlobal,
    AdmanRegionDemo,
    AdmanRegionDemoEu,
    AdmanRegionVoice,
    AdmanRegionIndia,
    AdmanRegionCustom
};

typedef NS_ENUM (NSUInteger, AdmanFormat) {
    AdmanFormatAny = 1,
    AdmanFormatSwiftSponsor = 2,
    AdmanFormatGameVibe = 3
};

typedef NS_ENUM (NSUInteger, AdmanType) {
    AdmanTypeAny = 1,
    AdmanTypeAudioOnly = 7,
    AdmanTypeAudioPlus = 6,
    AdmanTypeVoice = 12,
    AdmanTypeVoiceMultipleAds
};

typedef NS_ENUM(NSUInteger, AdmanAdSlot) {
    AdmanPreroll,
    AdmanMidroll,
    AdmanPostroll,
    AdmanAdSlotDefault
};

typedef NS_ENUM (NSUInteger, AdmanRecognitionBackend) {
    AdmanRecognitionBackendAuto = 0,
    AdmanRecognitionBackendYandex,
    AdmanRecognitionBackendMicrosoft,
    AdmanRecognitionBackendNuanceDev,
    AdmanRecognitionBackendHoundify,
    AdmanRecognitionBackendNuanceRelease
};

/**
 geo location for voice recognition server
 Auto - default value, replaced by VAST responseURL section if present.
 EU
 USA
 Demo - USA demo server
 */
typedef NS_ENUM(NSUInteger, AdmanRecognitionBackendLocation) {
    AdmanRecognitionBackendLocationAuto,
    AdmanRecognitionBackendLocationEU,
    AdmanRecognitionBackendLocationUSA,
    AdmanRecognitionBackendLocationDemo
};

typedef NS_ENUM(NSUInteger, AdmanBannerType) {
    t320x480 = 0,
    t320x50
};

/**
 @property state current Adman state
 */
@property (nonatomic) AdmanState state;
@property (nonatomic) AdmanState fallbackState;

@property (nonatomic, readonly, nullable) NSError *error;

@property (nonatomic, weak, nullable) id <AdmanDelegate> delegate;

/**
 @property adsList<AdmanBanner*> creatives list for show/playback on device
 */
@property (nonatomic, strong, nullable) NSArray<AdmanBanner *> *adsList;
/**
 index of active ad in adsList
 */
@property (nonatomic, readonly) NSInteger activeAd;

/**
 @property mute mute/unmute playback for current audio ad
*/
@property (nonatomic) BOOL mute;
/**
 @property volume audio ad volume
 */
@property (nonatomic) float volume;
/**
 @property elapsedTime time played in seconds from audio start
 */
@property (nonatomic, readonly) long elapsedTime;
/**
 @property elapsedTimeString time played in seconds as string from audio start
 */
@property (strong, nonatomic, readonly, nullable) NSString *elapsedTimeString;
@property (nonatomic, readonly) long estimatedTime;
@property (strong, nonatomic, readonly, nullable) NSString *estimatedTimeString;
@property (nonatomic) float progress;

@property (nonatomic, null_unspecified) NSString *userId;
@property (nonatomic) NSInteger playerId;
@property (nonatomic, readonly, nullable) NSString *transcriptedPhrase;

/** Initialize and return new Adman object */
+ (_Null_unspecified instancetype)sharedManager;
/**
 @param siteId unique partner identifier, obtained from the Instreamatic manager
 */
+ (_Null_unspecified instancetype)sharedManagerWithSiteId:(NSInteger)siteId;
/**
 @param testMode ignore all targeting params
 */
+ (_Null_unspecified instancetype)sharedManagerWithSiteId:(NSInteger)siteId testMode:(BOOL)testMode;
+ (_Null_unspecified instancetype)sharedManagerWithSiteId:(NSInteger)siteId playerId:(NSInteger)playerId testMode:(BOOL)testMode;
+ (_Null_unspecified instancetype)sharedManagerWithSiteId:(NSInteger)siteId region:(AdmanRegion)region testMode:(BOOL)testMode;
+ (_Null_unspecified instancetype)sharedManagerWithSiteId:(NSInteger)siteId region:(AdmanRegion)region playerId:(NSUInteger)playerId testMode:(BOOL)testMode;
+ (_Null_unspecified instancetype)sharedManagerWithSiteId:(NSInteger)siteId withZoneId:(NSInteger)zoneId testMode:(BOOL)testMode;

- (_Null_unspecified id)initWithURL:(nonnull NSString *)url;

- (_Null_unspecified id)initWithSiteId:(NSInteger)siteId testMode:(BOOL)testMode withZoneId:(NSInteger)zoneId region:(AdmanRegion)region playerId:(NSInteger)playerId;

/**
 Send request to server for creative.
 When response will be parsed and audio ad loaded, Adman state changed to ReadyForPlayback
 Adman state may be AdmanStateAdNone if server return empty response
 */
- (void)prepare;
- (void)prepareWithFormat:(AdmanFormat)format;
- (void)prepareWithType:(AdmanType)type;
- (void)prepare:(AdmanFormat)format type:(AdmanType)type;
/**
 @param maxDuration limit in seconds summary ads playback duration. Set 0 for default value, but cannot be lower than 10.
 @param adsCount limit maximum ads count requested from server, 1 by default.
 */
- (void)prepareWithAdLimits:(NSInteger)maxDuration adsCount:(NSInteger)adsCount;
- (void)prepare:(AdmanFormat)format type:(AdmanType)type maxDuration:(NSInteger)maxDuration adsCount:(NSInteger)adsCount;

/**
  Check expiration time for ads list. If ads expired they will be preloaded automatically
 */
- (bool)isValidToPlayback;

/**
   Check if app already in background
 */
- (bool)isInBackground;


/**
   Check mirophone availabillity
 */
- (bool)isMicroEnabled;

/**
 Cancel ad loading and audio buffering
 */
- (void)cancelLoading;

/**
 Start playback, Adman state changed to AdmanStatePlaying
 */
- (void)play;
/**
 Pause playback, state changed to AdmanStatePaused
 */
- (void)pause;
/**
 Resume playback, state changed to AdmanStatePlaying
 Playback cannot be resumed after [adman stop] method
 */
- (void)resume;
/**
 Stop playback and rewind to start.
 State changed to AdmanStatePlaying
 */
- (void)rewind;
/**
 Stop playback.
 State changed to AdmanStateStopped. Playback cannot be resumed.
 */
- (void)stop;

/**
 Place banner image in uiview
 @param spot image view to place in
 @param type image size
 */
- (void)showBannerForSpot:(nonnull UIView *)spot withType:(AdmanBannerType)type;
/**
 Report ad event
 @param eventName VAST-specific event name (see https://www.iab.com/guidelines/digital-video-ad-serving-template-vast-3-0 for more information about ad event names)
    first eventName letter should be always in lowerCase (e.g creativeView, clickTracking)
 */
- (void)reportAdEvent:(nonnull NSString *)eventName;

/**
 Return active Adman creative
 */
- (nullable AdmanBanner *)getActiveAd;

/**
 Enable dtmf ads detection for station name
 @param name radiostation name
 @param preload ad aotimaticaly after short delay
 @param vc ViewController for default sdk banner view to display in
 */
- (void)enableDtmfAdsDetectionForStation:(nonnull NSString *)name preload:(BOOL)preload vc:(nullable UIViewController *)vc;
- (void)disableDtmfAdsDetection;

/**
 Override default banner size on preloading resources phase if creative does not contain 640x640 image
 @param size banner WxH, where W - width, H - size
 */
- (void)setDefaultBannerSize:(nonnull NSString *)size;
- (nonnull NSString *)getDefaultBannerSize;
/**
 @param format any supported on Apple devices media format (e.g. mp3, aac, alac)
 @param bitrate only float or integer (e.g. 128000, 128, 128.419)
 */
- (void)setDefaultAudioFormat:(nonnull NSString *)format withBitrate:(nullable NSNumber *)bitrate;

- (void)setServerVoiceActivityDetection:(bool)state;

+ (void)setPreloadMode:(AdmanPreloadMode)mode;

/**
 Set custom user ip if needed (advanced targeting)
 */
- (void)overrideIP:(nonnull NSString *)ip;
/**
 Set default voice recognition backend
 */
- (void)setRecognitionBackend:(AdmanRecognitionBackend)backend;

/**
 Return current region
 */
- (AdmanRegion)currentRegion;
/**
 Return instance siteId
 */
- (NSInteger)siteId;

/**
 Return current libAdman version in format "X.Y.Z"
 */
+ (nonnull NSString *)getVersion;
/**
 Return Adman ad server for user Region
 */
- (nonnull NSString *)getServer:(AdmanRegion)region;

@end


@interface Adman(AdmanForwardedMethods)
/**
 ##### Common setup methods #####
 */
- (NSInteger)campaignId;
- (void)setCampaignId:(NSInteger)campaignId;
- (NSInteger)creativeId;
- (void)setCreativeId:(NSInteger)creativeId;

- (void)setRecognitionBackendLocation:(AdmanRecognitionBackendLocation)location;
- (AdmanRecognitionBackendLocation)recognitionBackendLocation;

/**
 
 */
- (void)enableGeolocationTargeting;

/**
Disable sdk AVAudioSession setup if app need to keep settings
*/
- (void)disableAudioSessionSetup;

/**
 Enable/disable debug mode
 */
- (void)setPreview:(AdmanDebugMode)mode;

/**
 Enable preroll/midroll/postroll targeting
 */
- (void)setAdSlot:(AdmanAdSlot)slot;

- (AdmanAdSlot)getAdSlot;

- (void)setCustomRegionServer:(nonnull NSString *)adServer;
/**
 Set voice response delay
 */
- (void)setResponseDelay:(float)delay;
- (float)getResponseDelay;

- (void)setResponseTime:(float)duration;
- (float)getResponseTime;

/**
 Set max lenght for server waiting timeout
 */
- (void)setServerTimeout:(NSInteger)delay;
/**
 Set custom siteVariable to all ad requests
 */
- (void)setSiteVariable:(nonnull NSString *)key value:(nonnull NSString *)value;
@end
