/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import UIKit
import AVFoundation

class PlayerVC: UIViewController, AdmanDelegate {
    static var adm: Adman?
    var internalPlayer: AVPlayer?
    var ui: AdmanUIExt?

    @IBOutlet var playRadioBtn: UIButton!
    @IBOutlet var playAdsBtn: UIButton!
    @IBOutlet var preloadAdBtn: UIButton!
    @IBOutlet var bannerPlaceholder: UIStackView!
    @IBOutlet weak var statusLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        PlayerVC.adm = Adman.sharedManager(withSiteId: 1063, region: .global, testMode: false)
        // PlayerVC.adm = Adman.init(url: "https://x3.instreamatic.com/v5/vast/777?type=voice&microphone=1&ads_count=1&advertising_tracking_enabled=1&format=1&idfa=91679CB9-E970-43CB-BC40-9016333A0062&lang=en&version=2.16.17")
        // PlayerVC.adm?.setPreview(.voiceAd)
        ui = AdmanUIExt.init()

        PlayerVC.adm?.delegate = self
        Adman.setPreloadMode(.full)
        AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
        })

        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [AVAudioSession.CategoryOptions.defaultToSpeaker, AVAudioSession.CategoryOptions.allowBluetoothA2DP, AVAudioSession.CategoryOptions.allowAirPlay])
            } else {
                // Fallback on earlier versions
            }
        } catch { }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // self.internalPlayer = AVPlayer.init(url: URL.init(string: "http://192.168.0.175:8899/")!)
        // self.internalPlayer?.volume = 1.0
    }

    func prepare() {
        PlayerVC.adm?.prepare(.any, type: .any, maxDuration: 0, adsCount: 1)
    }

    @objc func onSwipeBack(sender: Any) {

    }

    func admanStateDidChange(_ sender: Adman) {
        NSLog("State: %li", sender.state.rawValue)
        DispatchQueue.main.async {
            switch sender.state {
            case .readyForPlayback:
                self.statusLabel.text = "Ad ready for playback"
                NSLog("Playing next ad after request")
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                    self.playAdsTouch(sender)
                })
                break
            case .playbackCompleted:
                self.prepare()
                self.internalPlayer?.play()
                self.statusLabel.text = "Audio ad ended"
                break
            case .fetchingInfo:
                self.statusLabel.text = "Preloading audio"
                break
            case .error:
                self.statusLabel.text = sender.error?.localizedDescription
                break
            case .adNone:
                self.statusLabel.text = "Nothing to show"
                break
            default:
                break
            }
        }
    }

    @IBAction func playRadioTouch(_ sender: Any) {
        PlayerVC.adm?.pause()
    }

    @IBAction func playAdsTouch(_ sender: Any) {
        if (PlayerVC.adm?.state == AdmanState.readyForPlayback) {
            self.internalPlayer?.pause()
            self.statusLabel.text = "Playing sdk ad"
            PlayerVC.adm?.play()
            PlayerVC.adm?.reportAdEvent("can_show")
            ui?.show(self)
        } else if (PlayerVC.adm?.state == AdmanState.paused) {
            self.internalPlayer?.pause()
            PlayerVC.adm?.resume()
        }
    }

    @IBAction func preloadAdTouched(_ sender: Any) {
        PlayerVC.adm?.pause()
        self.prepare()
        // PlayerVC.adm?.showBanner(forSpot: bannerPlaceholder, with: .t320x480)
    }

    func bannerTouched(_ urlToNavigate: String?) {

    }
}
