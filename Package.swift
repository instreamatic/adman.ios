// swift-tools-version:5.0
import PackageDescription

let package = Package(
   name: "Adman",
   platforms: [
      .iOS(.v10)
   ],
   products: [
      .library(name: "Adman", targets: ["Adman"])
   ],
   dependencies: [
      .package(url: "https://github.com/Fireroman/SocketRocket.git", .branch("master"))
   ],
   targets: [
      .target(
         name: "Adman",
         dependencies: ["SocketRocket"],
         path: "Adman"
      )
   ]
 )