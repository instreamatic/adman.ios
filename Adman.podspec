Pod::Spec.new do |s|

  s.name         = "Adman"
  s.version      = "2.19.3"
  s.summary      = "Adman is used to inject ads, to control audio ad playback and to serve a companion display banner"
  s.description  = <<-DESC
 Voice-Activated Ads allow users to engage with ads using voice commands.
While listening to an audio, an ad user can speak various voice commands which will be introduced to users within the ad creative. When a user says ‘skip it’, ‘next’, ‘cancel’, ‘not interested’, etc, we stop serving the current ad and play the next one if there is one scheduled. 

When a user engages with the ad, we perform the requested action, e.g. open browser, playing a successive audio with ‘more details’, etc.

Actions can be ‘positive’ when users want to ‘know more’ or make a purchase or ‘negative’ when users say ‘no’ or ‘I’m not interested’.
                   DESC
  s.homepage     = "https://help.instreamatic.com/instreamatic-dialogue-ads/ios-manual-for-dialogue-ads"
  s.license      = "GNU AGPLv3"
  s.author             = { "Instreamatic" => "support@instreamatic.com" }
  s.platform     = :ios, "12.0"
  s.source       = { :git => "https://bitbucket.org/instreamatic/adman.ios", :tag => "#{s.version}" }
  s.source_files  = "Adman/*/*.{h,m}", "Adman/*.{h,m}", "AdmanBanner.{h,m}"
  s.dependency "SocketRocket"

end
