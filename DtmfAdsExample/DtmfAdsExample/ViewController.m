/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "ViewController.h"
#import <AVFoundation/AVPlayer.h>

@interface ViewController ()

@property Adman *adman;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _adman = [Adman sharedManagerWithSiteId:1249];
    _adman.delegate = self;
    [_adman prepare];
    [_adman enableDtmfAdsDetectionForStation:@"record" preload:YES vc:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)admanStateDidChange:(Adman *)sender {
    // NSLog(@"Current state: %li", (long) sender.state);
}

- (void)dtmfIn {
    AdmanUIBase *ui = [[AdmanUIBase alloc] init];
    [ui show:self];
}

@end
