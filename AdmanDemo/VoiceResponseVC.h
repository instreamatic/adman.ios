//
//  VoiceResponseVC.h
//  AI-powered Voice-Activated Advertising
//
//  Created by test name on 6/19/17.
//  Copyright © 2017 Instreamatic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Adman.h"

@interface VoiceResponseVC : UIViewController<AdmanDelegate>

@property (strong, nonatomic) IBOutlet UIButton *launchDemo;

@property (strong, nonatomic) IBOutlet UIButton *toggleDemo;
@property (strong, nonatomic) IBOutlet UIScrollView *voiceIntentsView;

- (IBAction)onDemoLaunched:(id)sender;

- (IBAction)onDemoRepeat:(id)sender;
- (IBAction)onPlaybackDown:(id)sender;
- (IBAction)onPlaybackUpOut:(id)sender;

- (IBAction)moreAction:(id)sender;
- (IBAction)skipAction:(id)sender;

@end
