//
//  ViewController.m
//  VoiceActivatedAdsDemo
//
//  Created by test name on 6/19/17.
//  Copyright © 2017 Instreamatic. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "MicrophoneRequestVC.h"
#import "VoiceResponseVC.h"

@interface MicrophoneRequestVC ()

@property VoiceResponseVC *mainController;

@end

@implementation MicrophoneRequestVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showMicrophoneDialog];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)showMicrophoneDialog {
    _mainController = [[VoiceResponseVC alloc] initWithNibName:@"VoiceResponseVC" bundle:nil];
    
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionGranted) {
        [self.navigationController pushViewController:_mainController animated:YES];
        return;
    }
    
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionDenied ||
        [[NSUserDefaults standardUserDefaults] objectForKey:@"lastMicroPermissionNotifyDate"] != nil) {
        NSDate *lastNotifyDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"admanLastMicroPermissionNotifyDate"];
        if (lastNotifyDate != nil) {
            NSDateComponents *diff = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:lastNotifyDate toDate:[NSDate new] options:NSCalendarWrapComponents];
            if ([diff day] <= 6) {
                [self.navigationController pushViewController:_mainController animated:YES];
                return;
            }
        }
        [self showMessageAboutDisabledMicro];
        return;
    }
    
    UIAlertController * infoDialog = [UIAlertController
                                      alertControllerWithTitle:@"Please allow the microphone access in order to interact with ads."
                                      message:@"That includes skipping ads, learning more and other useful commands. Do you want to enable advertising voice control?"
                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                   NSDate *today = [NSDate new];

                                   [defaults setObject:today forKey:@"lastMicroPermissionNotifyDate"];
                                   [defaults synchronize];
                                   [self.navigationController pushViewController:_mainController animated:YES];
                               }];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL permission) {
                                        if (permission == NO)
                                            [self showMessageAboutDisabledMicro];
                                        else
                                            [self.navigationController pushViewController:_mainController animated:YES];
                                    }];
                                }];
    [infoDialog addAction:noButton];
    [infoDialog addAction:yesButton];

    [self presentViewController:infoDialog animated:NO completion:nil];
}

- (void)showMessageAboutDisabledMicro {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Microphone for voice ads control disabled."
                                 message:@"Please allow the microphone access in iOS settings for this app."
                                 preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction
                      actionWithTitle:@"Ok"
                      style:UIAlertActionStyleDefault
                      handler:nil]];

    [self presentViewController:alert animated:NO completion:nil];
    [self.navigationController pushViewController:_mainController animated:YES];
}

@end
