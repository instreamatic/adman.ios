//
//  VoiceResponseVC.m
//  AI-powered Voice-Activated Advertising
//
//  Created by test name on 6/19/17.
//  Copyright © 2017 Instreamatic. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "VoiceResponseVC.h"

@interface VoiceResponseVC ()

@property Adman *adsManager;
@property UIView *statusBarProxyView;

@property NSTimer *responseLogUpdater;
@property NSTimer *loggingTimer;

@end

@implementation VoiceResponseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _adsManager = [Adman sharedManagerWithSiteId:264 testMode:false];
    _adsManager.delegate = self;
    [_adsManager setPreview:AdmanDebugBanner];

    [self setAutomaticallyAdjustsScrollViewInsets:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _statusBarProxyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    _statusBarProxyView.backgroundColor = [UIColor redColor];
    [self.view addSubview:_statusBarProxyView];
    [_statusBarProxyView setHidden:YES];
    [_voiceIntentsView setHidden:YES];

    [self prepare];
}

- (void)prepare {
    [_adsManager prepare];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)stop {
    [_adsManager stop];
    [_adsManager cancelLoading];
}



- (void)enableDebugging {
    NSLog(@"Logging to remote host enabled");
    [_adsManager setEnableTelemetry:true];
}

- (void)hideMicrophoneStatusView {
    // [_voiceIntentsView setHidden:YES];
    [_statusBarProxyView setHidden:YES];
}

- (void)hideRecognizedPhrase {
}

- (void)phraseRecognized:(nonnull NSString *)phrase {
    /** 
     dispatch_async(dispatch_get_main_queue(), ^(void){

        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideRecognizedPhrase) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    });
     */
}

- (void)userPhraseValid:(nonnull NSString *)phrase {

}

- (void)userPhraseInvalid:(nonnull NSString *)phrase {

}

// MARK: - UI Actions


- (IBAction)onDemoLaunched:(id)sender {
    [_launchDemo setHidden:YES];
    [_voiceIntentsView setHidden:NO];
    [_toggleDemo setHidden:NO];
    [_adsManager play];
}

- (IBAction)onDemoRepeat:(id)sender {
    if (_loggingTimer) {
        [_loggingTimer invalidate];
        _loggingTimer = nil;
    }

    if ([_toggleDemo.currentTitle isEqualToString:@"Stop playback"]) {
        [_toggleDemo setTitle:@"Start playback" forState:UIControlStateNormal];
        [self stop];
    } else {
        [_toggleDemo setTitle:@"Stop playback" forState:UIControlStateNormal];
        [self prepare];
    }
}

- (IBAction)onPlaybackDown:(id)sender {
    _loggingTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(enableDebugging) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_loggingTimer forMode:NSRunLoopCommonModes];
}

- (IBAction)onPlaybackUpOut:(id)sender {
    if (_loggingTimer) {
        [_loggingTimer invalidate];
        _loggingTimer = nil;
    }
}

- (IBAction)moreAction:(id)sender {

}

- (IBAction)skipAction:(id)sender {

}


// MARK: - Adman Delegate

- (void) admanStateDidChange:(Adman *)sender {
    switch (sender.state) {
        case AdmanStateAdChangedForPlayback:
            break;

        case AdmanStatePlaying:
            break;

        case AdmanStatePlaybackCompleted:
            [self prepare];
            break;

        case AdmanStateFetchingInfo:
            [self hideMicrophoneStatusView];

            break;

        case AdmanStateVoiceInteractionStarted:
            NSLog(@"Voice recognizer started");
            // [_statusBarProxyView setHidden:NO];
            // [_voiceIntentsView setHidden:NO];
            break;

        case AdmanStateStopped:
            [_toggleDemo setTitle:@"Start playback" forState:UIControlStateNormal];
        case AdmanStateVoiceInteractionEnded:
            [self hideMicrophoneStatusView];
            NSLog(@"Voice recognizer stopped");
            break;

        case AdmanStateVoiceResponsePlaying:
            [self hideMicrophoneStatusView];
            NSLog(@"Playing response to voice intent");
            break;

        case AdmanStateReadyForPlayback:
            if (_launchDemo.hidden) {
                [_adsManager play];
            } else {
                [_launchDemo setUserInteractionEnabled:YES];
                [_launchDemo setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            }
            break;
        case AdmanStateAdNone:
        case AdmanStateError:
        default:
            break;
    }
}

@end
