#import <AVFoundation/AVPlayer.h>
#import <UIKit/UIKit.h>
#import <Adman_framework/Adman.h>

@interface ViewController : UIViewController<AdmanDelegate>

@end


@interface ViewController ()

@property Adman *adman;
@property AVPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // init media player
    _player = [AVPlayer playerWithURL:[NSURL URLWithString:@"https://playlist.pls"]];;

    // init libAdman
    _adman = [Adman sharedManagerWithSiteId:1063 region:AdmanRegionUS testMode:false];
    _adman.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_player setVolume:1.0];
    // init ads preloading
    [_adman prepareWithType:AdmanTypeAny];
    // start audio playback
    [_player play];

    // play ads each 90 seconds
    [NSTimer timerWithTimeInterval:90 repeats:YES block:^(NSTimer *timer) {
        // report can_show stat event
        [self.adman reportAdEvent:@"can_show"];
        // start ad playback
        if (self.adman.state == AdmanStateReadyForPlayback) {
            [self.adman play];
            [self.player pause];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)admanStateDidChange:(Adman *)sender {

    switch (sender.state) {
        case AdmanStateAdChangedForPlayback:
            break;

        case AdmanStatePlaying:
            break;

        case AdmanStatePlaybackCompleted:
            [_player play];
            [_adman prepareWithType:AdmanTypeAny];
            break;

        case AdmanStateFetchingInfo:
            break;

        case AdmanStateSpeechRecognizerStarted:
           break;
        case AdmanStateSpeechRecognizerStopped:
            break;

        case AdmanStateVoiceInteractionStarted:
            break;

        case AdmanStateStopped:
        case AdmanStateVoiceInteractionEnded:
            break;

        case AdmanStateVoiceResponsePlaying:
            break;

        case AdmanStateReadyForPlayback:
            break;
        case AdmanStateError:
        case AdmanStateAdNone:
            [_player play];
            [_adman prepareWithType:AdmanTypeAny];
            break;
        default:
            break;
    }
}

@end
