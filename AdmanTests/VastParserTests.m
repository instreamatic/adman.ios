/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define AdfoxRedirect @"https://ads.adfox.ru/168662/getCode?yandexuid=5616323942036404509&sign=ffc607267bfea68ddcdba81d04a4ff97&pp=i&ps=bfcd&p2=epaj&pfc=a&pfb=a&plp=a&pli=a&pop=a"
#define InstreamaticTestCreative @"http://x.instreamatic.com/web/777.xml?preview=11"
#define InstreamatocAdPodsSequeence @"https://d2975e0yphvxch.cloudfront.net/v2/vast/1013.xml?microphone=1&type=voice&ads_count=4"

#import <XCTest/XCTest.h>

#import "AFHTTPSessionManager.h"
#import "AdmanVastParser.h"
#import "AdmanVastParserDelegate.h"
#import "Adman.h"

@interface VastParserTests : XCTestCase
@property (strong) AdmanVastParser *parser;
@property (nonatomic) NSTimeInterval timeout;
@end

@implementation VastParserTests

- (void)setUp {
    [super setUp];
    _parser = [[AdmanVastParser alloc] init];
    _timeout = 5;
}

- (void)tearDown {
    [super tearDown];
}

- (void)testVastParserWrapper {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Ad request timed out."];

    [self.parser parse:AdfoxRedirect params:nil success:^(NSArray *ads) {
        XCTAssert([ads count] > 0, @"Ads list empty");
        AdmanBanner *banner = [ads objectAtIndex:0];
        XCTAssert([banner.media count] > 0, @"Failed to parse wrapped media");
        XCTAssert([banner.statistics[@"impression"] count] > 1, @"Failed to parse wrapped stat events");
        XCTAssert([banner.statistics[@"thirdQuartile"] count] > 1, @"Failed to parse wrapped stat events");
        [expectation fulfill];
    } failure:^(NSURLSessionTask *operation, NSError *err) {
        XCTAssert(err == nil, @"Network error");
        [expectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:_timeout handler:nil];
}

- (void)testVastParserDefault {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Ad request timed out"];

    [self.parser parse:InstreamaticTestCreative params:nil success:^(NSArray *ads) {
        XCTAssert([ads count] > 0, @"No ads");
        AdmanBanner *banner = ads.firstObject;
        XCTAssert(banner.sourceUrl != nil, @"Default mp3 empty");
        XCTAssert([banner.media count] > 0, @"Media empty");
        XCTAssert([banner.statistics count] > 0, @"Failed to parse stat events");
        [expectation fulfill];
    } failure:^(NSURLSessionTask *operation, NSError *err) {
        XCTAssert(err == nil, @"Network error");
        [expectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:_timeout handler:nil];
}

- (void)testVastParserAdPods {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Ad request timed out."];

    [self.parser parse:InstreamatocAdPodsSequeence params:nil success:^(NSArray *ads) {
        XCTAssert([ads count] > 0, @"adsList empty");
        XCTAssert([ads count] >= 2, @"Invalid parsed ads cound");
        NSInteger sequence[4] = {0, 0, 0, 0};
        for (NSInteger i = 0; i < [ads count]; i++) {
            AdmanBannerWrapper *banner = [ads objectAtIndex:i];
            XCTAssert([banner.statistics count] > 0, @"Failed to parse stat events");
            sequence[i] = banner.sequence;
        }

        NSInteger min = sequence[0],
                  max = sequence[0];
        for (NSInteger i = [ads count] - 1; i > 0; i--) {
            sequence[i] = sequence[i] - sequence[i - 1];
            if (sequence[i] > max)
                max = sequence[i];
            if (sequence[i] < min)
                min = sequence[i];
        }
        XCTAssert(max == min, @"Invalid ad pods sequence");
        [expectation fulfill];
    } failure:^(NSURLSessionTask *operation, NSError *err) {
        NSLog(@"%@", [err localizedDescription]);
        XCTAssert(err == nil, @"Network error");
        [expectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:_timeout handler:nil];
}

@end
