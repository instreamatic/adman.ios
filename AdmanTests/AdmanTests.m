/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define TestSiteId 777

#import <XCTest/XCTest.h>
#import "Adman.h"
#import "AdmanVastParser.h"

typedef NS_ENUM(NSUInteger, AdmanTest) {
    AdmanTestPrepareSimple,
    AdmanTestPrepareRu,
    AdmanTestPrepareGlobal,
    AdmanTestPrepareWithLimits,
    AdmanTestPrepareWithPreview,
    AdmanTestNoAds,
    AdmanTestNoAdsGlobal
};

@interface AdmanTests : XCTestCase<AdmanDelegate>

@property (nonatomic, strong) Adman *adm;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic) XCTestExpectation *expectation;
@property (nonatomic) AdmanTest test;
@end

@implementation AdmanTests

- (void)setUp {
    [super setUp];
    _timeout = 12.0;
}

- (void)tearDown {
    [super tearDown];
}

- (void)testAdmanSimplePrepare {
    _test = AdmanTestPrepareSimple;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:TestSiteId testMode:true];
    _adm.delegate = self;
    [_adm prepare:AdmanFormatAny type:AdmanTypeAny];

    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)testAdmanRuPrepare {
    _test = AdmanTestPrepareRu;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:TestSiteId region:AdmanRegionEU testMode:true];
    _adm.delegate = self;
    [_adm prepare];

    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)testAdmanGlobalPrepare {
    _test = AdmanTestPrepareGlobal;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:TestSiteId region:AdmanRegionGlobal testMode:true];
    _adm.delegate = self;
    [_adm prepare];
    
    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)testAdmanWithAdLimits {
    _test = AdmanTestPrepareWithLimits;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:TestSiteId testMode:true];
    _adm.delegate = self;
    [_adm setPreview:AdmanDebugBanner];
    [_adm prepareWithAdLimits:30 adsCount:2];
    
    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)testAdmanWithPreview {
    _test = AdmanTestPrepareWithPreview;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:TestSiteId testMode:true];
    _adm.delegate = self;
    [_adm setPreview:AdmanDebugBanner];
    [_adm prepare];
    
    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)testNoAds {
    _test = AdmanTestNoAds;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:123 testMode:false];
    _adm.delegate = self;
    [_adm prepare];
    
    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)testNoAdsGlobal {
    _test = AdmanTestNoAdsGlobal;
    _expectation = [self expectationWithDescription:@"Ad request timed out."];

    _adm = [Adman sharedManagerWithSiteId:778 region:AdmanRegionGlobal testMode:false];
    _adm.delegate = self;
    [_adm prepare];
    
    [self waitForExpectationsWithTimeout:_timeout handler:^(NSError *error) {
        if (error)
            NSLog(@"Error: %@", error);
    }];
}

- (void)viewClosed {

}

- (void)stateDidChange:(Adman *)sender {

}

- (void)bannerTouched:(NSString *)urlToNavigate {

}

- (void)admanStateDidChange:(nonnull Adman *)sender {
    if (sender.state == AdmanStateError || sender.state == AdmanStateAdNone || sender.state == AdmanStateReadyForPlayback) {
        switch (_test) {
            case AdmanTestPrepareSimple:
            case AdmanTestPrepareRu:
            case AdmanTestPrepareGlobal:
                XCTAssertTrue(_adm.state == AdmanStateReadyForPlayback || _adm.state == AdmanStateAdNone);
                break;
            case AdmanTestPrepareWithLimits:
                XCTAssertTrue(_adm.state == AdmanStateReadyForPlayback);
                XCTAssertTrue([_adm.adsList count] >= 1);
                break;
            case AdmanTestPrepareWithPreview:
                XCTAssertTrue([_adm.adsList count] > 0);
                XCTAssertTrue(_adm.state == AdmanStateReadyForPlayback);
                break;
            case AdmanTestNoAds:
            case AdmanTestNoAdsGlobal:
                XCTAssertTrue(_adm.state == AdmanStateAdNone);
                break;
            default:
                XCTAssertTrue(_adm.state == AdmanStateError);
                break;
        }
        [_expectation fulfill];
    }
}


@end
