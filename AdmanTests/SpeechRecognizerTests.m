/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <XCTest/XCTest.h>
#import "VACNetworkManager.h"

@interface SpeechRecognizerTests : XCTestCase<VACNetworkManagerDelegate>

@property bool connectionIsRunning;
@property VACNetworkManager *sock;

@end

@implementation SpeechRecognizerTests

- (void)setUp {
    [super setUp];
    
}

- (void)tearDown {
    [super tearDown];
}

- (void)testWebsocketAPI {
    _sock = [[VACNetworkManager alloc] init];
    _sock.delegate = self;
    [_sock startConnection:nil withOptions:@{}];

    _connectionIsRunning = true;
    while(_connectionIsRunning)
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2]];
}

- (void)networkManagerConnectionDidOpened:(VACNetworkManager *)manager {
    NSBundle *b = [NSBundle bundleForClass:self.class];

    for( int i=1; i <=94; i++ ) {
        NSString *filename = [b pathForResource:[NSString stringWithFormat:@"testdata/%d.bin", i] ofType:nil];
        NSData *data = [NSData dataWithContentsOfFile:filename];
        [_sock send:data];
    }
}

- (void)networkManager:(VACNetworkManager *)manager didResponseRecived:(NSString *)response {
    //[_sock closeConnection];
    //_connectionIsRunning = false;
}

- (void)networkManager:(VACNetworkManager *)manager didFailWithError:(NSError *)error {
    [_sock closeConnection];
    _connectionIsRunning = false;
}

- (void)networkManager:(VACNetworkManager *)manager didSessionEnded:(NSDictionary *)response {
    [_sock closeConnection];
    _connectionIsRunning = false;
}

- (void)networkManager:(VACNetworkManager *)manager didCommandRecived:(NSString *)command {

}

@end
