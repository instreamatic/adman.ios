# iOS SDK manual

libAdman features:

- Requesting and processing VAST from Instreamatic Ad Server (adman.admin repository).
- Audio Ads playback (with or without companion banner) on iOS devices.
- Voice-Enabled Ads playback on iOS devices.

Integration manual: https://help.instreamatic.com/instreamatic-dialogue-ads/ios-manual-for-dialogue-ads

master - the main branch of the project.
x.y.z - stable library version

## XCode code build targets

- Adman - The main target to build the library (no dependencies).
- Adman_SR - SDK with SocketRocket library.
- Adman.universal - Target script that should be used to build the universal library with bitcode support. Generates the following files to ~/unisound/Adman/ : libAdman.a, libAdman_SR.a, libAdman_AF.a , then copies the headers to 'include' directory.
- Adman.framework - Framework with all the basic architecture patterns support.
- Adman.framework.universal - Framework for all the basic architecture patterns with added simulator support (x86_64).
- Instreamatic Voice Ads - demo app that allows to demonstrate and test Voice-Enabled Ads.
- SwiftExampleIntegration - SDK usage example for Swift Apps.
- DtmfAdsExample - Simple single-view app, that demonstrates Ad Injection after dtmf-marking.
