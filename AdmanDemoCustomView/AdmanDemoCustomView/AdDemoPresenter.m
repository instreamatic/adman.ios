//
//  AdDemoPresenter.m
//  AdDemo
//
//  Created by test name on 6/17/19.
//  Copyright © 2019 Instreamatic. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AdDemoPresenter.h"

#import <AVFoundation/AVPlayer.h>
#import <Adman_framework/Adman.h>
#import <Adman_framework/AdmanUI.h>
#import <Adman_framework/AdmanEvents.h>
#import "DefaultAdmanViewControllerVoicePortrait.h"

@interface AdDemoPresenter()<AdmanDelegate>

@property Adman *adman;
@property AdmanUIManager* uiManager;
@property AVPlayer *player;

@end


@implementation AdDemoPresenter

UIViewController* viewController;
UIView* adContainer;

-(void) addEvents :(NSString*) eventText{
    if(viewController == nil) return;
    id<AdDemoView> demoView = (id<AdDemoView>)viewController;
    [demoView addEventToList: eventText];
}

//AdDemoViewPresenter
//
- (void)requestAdAudioAuthorization{
    [self getMicPermission];
}

- (void)setView :(UIViewController*) view{
    
    viewController = view;
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:NOTIFICATION_ADMAN_MESSAGE object:nil];
}

- (void)setAdContainer :(UIView*) view{
    adContainer = view;
    [self updateViewContainerAdman];
}


- (void)doStart{
    [self startPlayer];
    [self startAdman];
}

- (void)doStop{
    [self stopPlayer];
    [self stopAdman];
}

//player
- (void)initPlayer {
    if (_player != nil) return;
    // init media player
    _player = [AVPlayer playerWithURL:[NSURL URLWithString:@"https://playlist.pls"]];;
    [_player setVolume:1.0];
}

- (void)startPlayer {
    NSLog(@"startPlayer");
    [self initPlayer];
    // start audio playback
    [_player play];
}

- (void)stopPlayer {
    NSLog(@"stopPlayer");
    if (_player == nil) return;
    [_player pause];
}

//Adman
- (void)initAdman {
    if (_adman != nil) return;
    
    
    // init libAdman
    _adman = [Adman sharedManagerWithSiteId:1063 region:AdmanRegionGlobal testMode:false];
    
    //_adman = [Adman sharedManagerWithSiteId: 264 testMode: false];
    //_adman = [Adman sharedManagerWithSiteId:777 region:AdmanRegionDemoEu testMode:false];
    //_adman.delegate = self;
    
    [_adman prepareWithType:AdmanTypeAny consentString:nil];
    
    // init UIAdman
    id<IAdmanViewBundle> bundle = [self buildViewBundle];
   
    _uiManager = [[AdmanUIManager alloc] init];
    _uiManager.adman = _adman;
    _uiManager.bannerParentVC = viewController;
    _uiManager.viewParent = adContainer;
    [self.uiManager setAdmanViewBundle: bundle];

}

- (void)startAdman {
    NSLog(@"startAdman");
    [self initAdman];
    // start audio playback
    [_adman play];
}

- (void)stopAdman {
    NSLog(@"stopAdman");
    if (_adman != nil) return;
    [_adman pause];
}

- (void)updateViewContainerAdman {
    if (_adman == nil) return;
    //id<IAdmanViewBundle> bundle = [self buildViewBundle];
    //[self.adman setAdmanViewBundle: bundle];


    //if (self.adman.state == AdmanStateReadyForPlayback){
    //}
}

//AdmanDelegate
- (void)admanStateDidChange:(Adman *)sender {
    NSString *stateStr = [NSString stringWithFormat:@"startAdman state: %@\n", [Adman getStateString:sender.state]];
    [self addEvents: stateStr];
    
    switch (sender.state) {
        case AdmanStateAdChangedForPlayback:
            break;
            
        case AdmanStatePlaying:
            break;
            
        case AdmanStatePlaybackCompleted:
            [_player play];
            [_adman prepareWithType:AdmanTypeAny consentString:nil];
            break;
            
        case AdmanStateFetchingInfo:
            break;
            
        case AdmanStateSpeechRecognizerStarted:
            break;
        case AdmanStateSpeechRecognizerStopped:
            break;
            
        case AdmanStateVoiceInteractionStarted:
            break;
            
        case AdmanStateStopped:
        case AdmanStateVoiceInteractionEnded:
            break;
            
        case AdmanStateVoiceResponsePlaying:
            break;
            
        case AdmanStateReadyForPlayback:
            if (self.adman.state == AdmanStateReadyForPlayback) {
                // report can_show stat event
                [self.adman reportAdEvent:@"can_show"];
                            dispatch_async(dispatch_get_main_queue(), ^{
//                                id<IAdmanViewBundle> bundle = [self buildViewBundle];
//                                [self.adman setAdmanViewBundle: bundle];
//                                AdmanViewBaseView* admanContainer = [bundle get: CONTAINER];
//                                UIView* viewNib= [admanContainer getView];
//                                [adContainer addSubview: viewNib];
//
//                                [self.adman showDefaultView: viewNib withAutoplay:false];
                                
                                
                                //[self.adman showDefaultView: adContainer];
                                [self.adman play];
                            });
                [self stopPlayer];
            }
            break;
        case AdmanStateError:
        case AdmanStateAdNone:
            [self startPlayer];
            break;
            
        default:
            break;
    }
}

- (id<IAdmanViewBundle>)buildViewBundle{
    return [[DefaultAdmanViewControllerVoicePortrait alloc] initWithFrame:CGRectMake(0, 0, adContainer.frame.size.width, adContainer.frame.size.height)];

//    if ([[self.adman getActiveAd] isVoiceAd] ) {
//        return [[DefaultAdmanViewControllerVoicePortrait alloc] initWithFrame:CGRectMake(0, 0, adContainer.frame.size.width, adContainer.frame.size.height)];
//    }
//    UIInterfaceOrientation toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
//    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
//        return [[DefaultAdmanViewControllerPortrait alloc] initWithFrame:CGRectMake(0, 0, adContainer.frame.size.width, adContainer.frame.size.height)];
//    }
//    else{
//        return [[DefaultAdmanViewControllerLandscape alloc] initWithFrame:CGRectMake(0, 0, adContainer.frame.size.width, adContainer.frame.size.height)];
//    }
}

- (void)getMicPermission {
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionGranted)
        return;
    
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionDenied ||
        [[NSUserDefaults standardUserDefaults] objectForKey:@"admanLastMicroPermissionNotifyDate"] != nil) {
        NSDate *lastNotifyDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"admanLastMicroPermissionNotifyDate"];
        
        NSDateComponents *diff = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:lastNotifyDate toDate:[NSDate new] options:NSCalendarWrapComponents];
        if ([diff day] <= 6)
            return;
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Microphone for voice ads control disabled."
                                     message:@"If you want to enable advertising voice control allow microphone permission in Settings for this app."
                                     preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction
                          actionWithTitle:@"Ok"
                          style:UIAlertActionStyleDefault
                          handler:nil]];
        
        [viewController presentViewController:alert animated:NO completion:nil];
        return;
    }
    
    UIAlertController * infoDialog = [UIAlertController
                                      alertControllerWithTitle:@"Now you can control advertising with your voice."
                                      message:@"That includes skipping ads, learning more and other useful commands. Do you want to enable advertising voice control?"
                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                   NSDate *today = [NSDate new];
                                   
                                   [defaults setObject:today forKey:@"admanLastMicroPermissionNotifyDate"];
                                   [defaults synchronize];
                               }];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL permission) {}];
                                }];
    [infoDialog addAction:noButton];
    [infoDialog addAction:yesButton];
    
    [viewController presentViewController:infoDialog animated:NO completion:nil];
}

-(void) triggerAction:(NSNotification *) notification
{
    NSDictionary *dict = notification.userInfo;
    NotificationAdmanBase *message = [dict valueForKey:NOTIFICATION_ADMAN_MESSAGE_KEY];
    if (message != nil) {
        NSLog([NotificationAdmanBase getEventString: message.event]);
        switch (message.event) {
            case AME_FETCHING_INFO:
                break;
            case AME_NONE:
                break;
            case AME_ERROR:
                break;
            case AME_READY:
                if (self.adman.state == AdmanStateReadyForPlayback) {
                    // report can_show stat event
                    [self.adman reportAdEvent:@"can_show"];
                    [self.adman play];
                    [self stopPlayer];
                }
                break;
            case AME_STARTED:
                break;
            case AME_COMPLETED:
                [_adman prepareWithType:AdmanTypeAny consentString:nil];
                [self startPlayer];
                break;
            case AME_SKIPPED:
                break;
            default:
                break;
        }
        // do stuff here with your message data
    }
}

@end
