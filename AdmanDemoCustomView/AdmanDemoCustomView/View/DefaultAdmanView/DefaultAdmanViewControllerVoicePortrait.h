//
//  Created by ajjnix on 30/08/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Adman_framework/AdmanViewType.h"
#import "CustomViewWithXib.h"

//@class RXNote;

//typedef void (^RXNoteListViewControllerCreateNoteBlock)();
//typedef void (^RXNoteListViewControllerDetailNoteBlock)(RXNote *note);


IB_DESIGNABLE @interface DefaultAdmanViewControllerVoicePortrait : CustomViewWithXib <IAdmanViewBundle>

//@property (copy, nonatomic) RXNoteListViewControllerCreateNoteBlock createNoteBlock;
//@property (copy, nonatomic) RXNoteListViewControllerDetailNoteBlock detailNoteBlock;

//- (void)addNote:(RXNote *)note;

@end
