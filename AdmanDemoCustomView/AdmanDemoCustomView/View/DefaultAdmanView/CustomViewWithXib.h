//
//  CustomViewWithXib.h
//  CustomViews
//

#import <UIKit/UIKit.h>

/**
 *  All classes inherite from CustomViewWithXib should have the same xib file name and class name (.h and .m)
 MyCustomView.h
 MyCustomView.m
 MyCustomView.xib
 */
@protocol ICustomViewWithXib<NSObject>
- (void) initAdditional;
@end

// This allows seeing how your custom views will appear without building and running your app after each change.
IB_DESIGNABLE
@interface CustomViewWithXib : UIView<ICustomViewWithXib>

@end
