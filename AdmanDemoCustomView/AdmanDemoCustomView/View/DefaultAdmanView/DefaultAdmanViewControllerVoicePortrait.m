//
//  Created by ajjnix on 30/08/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "DefaultAdmanViewControllerVoicePortrait.h"


@interface DefaultAdmanViewControllerVoicePortrait ()
@property (strong, nonatomic) IBOutlet UIButton *adman_close;

@property (strong, nonatomic) IBOutlet UIImageView *adman_banner;
@property (strong, nonatomic) IBOutlet UIView *adman_container;

@property (strong, nonatomic) IBOutlet UILabel *adman_left;
@property (strong, nonatomic) IBOutlet UILabel *adman_mic_active;
@property (strong, nonatomic) IBOutlet UIButton *adman_response_negative;
@property (strong, nonatomic) IBOutlet UIButton *adman_response_positive;
@property (strong, nonatomic) IBOutlet UIView *adman_response_container;
@property (strong, nonatomic) IBOutlet UIImageView *adman_voice_progress;

@end


@implementation DefaultAdmanViewControllerVoicePortrait


#pragma mark - IAdmanViewBundle

- (bool) contains:(AdmanViewType) type{
    AdmanViewBase* view = [self get: type];
    return [view getView] != NULL;
}

- (nullable AdmanViewBase*) get:(AdmanViewType) type{
    switch (type) {
        case CONTAINER:
            return [[AdmanViewBaseView alloc] init: self];
            break;
        case BANNER:
            return [[AdmanViewBaseImageView alloc] init: _adman_banner];
            break;
        case CLOSE:
            return [[AdmanViewBaseView alloc] init: _adman_close];
            break;
        case VOICE_CONTAINER:
            return [[AdmanViewBaseView alloc] init: _adman_response_container];
            break;
        case VOICE_POSITIVE:
            return [[AdmanViewBaseButton alloc] init: _adman_response_positive];
            break;
        case VOICE_NEGATIVE:
            return [[AdmanViewBaseButton alloc] init: _adman_response_negative];
            break;
        case VOICE_MIC:
            return [[AdmanViewBaseLabel alloc] init: _adman_mic_active];
            break;
        case VOICE_PROGRESS:
            return [[AdmanViewBaseImageView alloc] init: _adman_voice_progress];
            break;
        case LEFT:
            return [[AdmanViewBaseLabel alloc] init: _adman_left];
            break;
    }
    return [[AdmanViewBase alloc]init];
}

#pragma mark - ICustomViewWithXib

-(void)initAdditional {
    UIImage *icon;
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:41];
    for (int i = 1; i <= 40; i++) {
        if (i < 10)
            icon = [UIImage imageNamed:[NSString stringWithFormat:@"loading_1000%i.gif", i]];
        else
            icon = [UIImage imageNamed:[NSString stringWithFormat:@"loading_100%i.gif", i]];
        if (icon != nil)
            [images addObject:icon];
    }
    
    [_adman_voice_progress setAnimationImages:images];
    _adman_voice_progress.animationDuration = 1.0;
    _adman_voice_progress.animationRepeatCount = 0;
    [_adman_voice_progress startAnimating];
}

@end
