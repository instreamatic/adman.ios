//
//  main.m
//  AdmanDemoCustomView
//
//  Created by test name on 9/5/19.
//  Copyright © 2019 Instreamatic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
