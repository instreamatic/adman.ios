//
//  ViewController.m
//  AdmanDemoCustomView
//
//  Created by test name on 9/5/19.
//  Copyright © 2019 Instreamatic. All rights reserved.
//

#import "ViewController.h"
#import "AdDemoPresenter.h"

@interface ViewController ()<AdDemoView>
{
    id<AdDemoViewPresenter> adDemoPresenter;
}
@property (strong, nonatomic) IBOutlet UIButton *start;
@property (strong, nonatomic) IBOutlet UIView *containerAd;
@property (strong, nonatomic) IBOutlet UITextView *tvEvents;
- (IBAction)onStart:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"viewDidLoad");
    id<AdDemoViewPresenter> presenter = [self instanceAdDemoPresenter];
    [presenter setView: (UIViewController*)self];
    [presenter setAdContainer:self.containerAd];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear :animated];
    NSLog(@"viewDidDisappear");
    id<AdDemoViewPresenter> presenter = [self instanceAdDemoPresenter];
    [presenter setView:nil];
}

//AdDemoView
- (void)addEventToList :(NSString*) textMsg{
    NSLog(@"addEventToList %@", textMsg);
    NSAssert(self.tvEvents != nil, @"self.textView is nil");
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSString *newText = [self.tvEvents.text stringByAppendingString:textMsg];
        self.tvEvents.text = newText;
        
        if (true){//(self.autoScrollsToBottom) {
            [self.tvEvents scrollRangeToVisible:NSMakeRange(newText.length, 0)];
        }
    });
    
}

//helper methods
- (id<AdDemoViewPresenter>)instanceAdDemoPresenter{
    if (adDemoPresenter == nil){
        adDemoPresenter = [[AdDemoPresenter alloc] init];
    }
    return adDemoPresenter;
}

- (void)destroyAdDemoPresenter{
    adDemoPresenter = nil;
}

//form widgets item
- (IBAction)onStart:(id)sender {
    NSLog(@"onStart");
    id<AdDemoViewPresenter> presenter = [self instanceAdDemoPresenter];
    
    [presenter requestAdAudioAuthorization];
    [presenter doStart];
}

@end
