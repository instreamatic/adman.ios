//
//  AppDelegate.h
//  AdmanDemoCustomView
//
//  Created by test name on 9/5/19.
//  Copyright © 2019 Instreamatic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

