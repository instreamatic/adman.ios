//
//  AdDemoPresenter.h
//  AdDemo
//
//  Created by test name on 6/17/19.
//  Copyright © 2019 Instreamatic. All rights reserved.
//

#ifndef AdDemoPresenter_h
#define AdDemoPresenter_h

@import AVFoundation;
@import UIKit;

@protocol AdDemoView
- (void)addEventToList :(NSString*) textMsg;
@end


@protocol AdDemoViewPresenter
- (void)requestAdAudioAuthorization;
- (void)setView :(UIViewController *) view;
- (void)setAdContainer :(UIView *) view;
- (void)doStart;
- (void)doStop;
@end

@interface AdDemoPresenter : NSObject <AdDemoViewPresenter>

@end

#endif /* AdDemoPresenter_h */
