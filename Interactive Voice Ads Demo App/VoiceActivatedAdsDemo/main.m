//
//  main.m
//  VoiceActivatedAdsDemo
//
//  Created by test name on 6/19/17.
//  Copyright © 2017 Instreamatic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

void registerDefaultsFromSettingsBundle() {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];

    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key && [[prefSpecification allKeys] containsObject:@"DefaultValue"]) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
    }

    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
}

int main(int argc, char * argv[]) {
    registerDefaultsFromSettingsBundle();
    // setup lang from settings.bundle
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"lang"] != nil && [[NSUserDefaults standardUserDefaults] stringForKey:@"lang"].length > 0) {
        NSArray *languages = [NSArray arrayWithObjects:[[NSUserDefaults standardUserDefaults] stringForKey:@"lang"], nil];
        [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
