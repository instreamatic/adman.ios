/**
* Copyright 2018 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import <Foundation/Foundation.h>
#import <Adman_framework/AdmanUIBase.h>
#import <Adman_framework/AdmanEvents.h>

@protocol AdmanUIExtensionDelegate <NSObject>
@required
- (void)refresh;
@end

@interface AdmanUIExtension : AdmanUIBase

@property id delegate;
@property bool devMode;

@end

#define REFRESH_BTN_SOURCE @"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA/1BMVEUAAAD//////Pz//f3//Pz//Pz//Pz//Pz/+/v//Pz//Pz/+/v//////////f3//Pz//Pz/+/v//////f3//Pz//Pz//f3//////Pz//////Pz//Pz//////////Pz/+vr//////Pz//Pz/////+/v//Pz/+/v//Pz/+vr/+vr//Pz/+/v//Pz//Pz//Pz/+/v/+/v//f3//Pz//Pz//Pz//f3//Pz//Pz//////Pz//Pz//Pz//Pz//f3/+/v//Pz//Pz//////////f3//Pz//Pz//f3//Pz/+/v//Pz//Pz//////Pz/////////+fn//f3/+/v//Pz//PwAAACwjZ4rAAAAU3RSTlMABk94n8Xs89rBlUUFCW7k75AVdujyfhpKC7fDFxv+OBTJVx3YxINbMzZjkchWvjxGzlX8YWzgVA/n+VJTZkD12wESe1HG1FBIu/0qTRwpLmc+uiOLZd0AAAABYktHRACIBR1IAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4goZDyQa9y9PfAAAAWZJREFUOMvNU9eCgjAQxIK9YO+9gKKeDXvvXe/y//9ySSAYkHu/eWF2Z9gkmyzDqDCZLVbWZnc4XW4P8wmvzw9UcIGgTvaEwkCDSBRWicWJnkiCD6TSGS6bk/V8ARjAzwJQxHqprKQqVV6o1cXG29TEhpYcJL/Iiu0OMXRRyGPa61MHlgaKYQij0RixyZQ6k6guMVOjOX3oxZIY0G8rRAprTVs227Fs2MFgjzYg6Btr6h+Q4YjqRQCQDHrPnM7gckUkeLszxng8mf+C5+MP4X7Dz+Z6AeeTkS7BZ7OA3yNqyKFv0utCD+b3kOzkpo63G42+xo9oBdmUXMxyQRvmOCdCNlPvVqT04QSXHSGqyAOJ2sbrgnM84l1SoNMm8vePnGnhqPl+og2xXhP4akUJyyVsKALAUkP1RiEv18tluUw69aknE2TFeAyOXjSilcMh/QAHAxw1VD6vQe89bpfTYbexVouZOvAvsbqBlmHbnCwAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMTAtMjVUMTU6MzY6MjYrMDI6MDCXGLG3AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTEwLTI1VDE1OjM2OjI2KzAyOjAw5kUJCwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII="
