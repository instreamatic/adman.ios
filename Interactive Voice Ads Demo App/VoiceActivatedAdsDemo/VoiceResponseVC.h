/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Adman_framework/Adman.h>
#import "AdmanUIExtension.h"

typedef NS_ENUM(NSUInteger, AdmanDemoMode) {
    AdmanDemoModeSimple,
    AdmanDemoModeDefault,
    AdmanDemoModeDefaultRUEu,
    AdmanDemoModePG
};

@interface VoiceResponseVC : UIViewController<AdmanDelegate, CLLocationManagerDelegate, AdmanUIExtensionDelegate>

@property (strong, nonatomic) IBOutlet UIButton *launchDemo;

@property (strong, nonatomic) IBOutlet UILabel *introTitle;
@property (strong, nonatomic) IBOutlet UITextView *voiceActionsText;
@property (strong, nonatomic) IBOutlet UITextView *voiceHelp;
@property (strong, nonatomic) IBOutlet UITextView *debugView;
@property (weak, nonatomic) IBOutlet UIScrollView *debugScrpllView;
@property (weak, nonatomic) IBOutlet UILabel *endInteractionLabel;

@property (strong, nonatomic) IBOutlet UIButton *toggleDemo;
@property (strong, nonatomic) IBOutlet UIScrollView *voiceIntentsView;
@property (weak, nonatomic) IBOutlet UIImageView *loadingGif;

- (IBAction)onDemoLaunched:(id)sender;

- (IBAction)onDemoRepeat:(id)sender;
- (IBAction)onPlaybackDown:(id)sender;
- (IBAction)onPlaybackUpOut:(id)sender;

- (IBAction)moreAction:(id)sender;
- (IBAction)skipAction:(id)sender;

- (void)update:(AdmanBanner *)ad;

@end
