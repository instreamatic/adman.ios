/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#define BUILD_MODE_TRITON 4004
#define BUILD_MODE_FULL 4005
#define ADMAN_BUILD_MODE BUILD_MODE_TRITON

#import <AVFoundation/AVFoundation.h>
#import "VoiceResponseVC.h"

#import <Adman_framework/AdmanEvents.h>

@interface VoiceResponseVC ()

@property AdmanUIExtension *admanUI;
@property Adman *mainAdsManager;
@property Adman *activeAdPlayer;
@property NSMutableArray<Adman *> *campaigns;

@property NSString *reaction;
@property AdmanState _lastState;
@property AdmanDemoMode demoMode;
@property bool pauseNextAd;

@property NSTimer *responseLogUpdater;
@property NSTimer *loggingTimer;
@property NSString *demoLang;
@property AdmanRegion region;

@end

@implementation VoiceResponseVC

- (void)initAdmanFull {
    _region = AdmanRegionDemo;
    NSString *ads_region = [[NSUserDefaults standardUserDefaults] stringForKey:@"ads_region"];
    NSInteger siteId = 1043;
    if ([ads_region isEqualToString:@"us_demo"])
        _region = AdmanRegionUS;
    else if ([ads_region isEqualToString:@"eu_demo"]) {
        _region = AdmanRegionDemoEu;
        siteId = 777;
    } else if ([ads_region isEqualToString:@"global"]) {
        _region = AdmanRegionGlobal;
    }
    
    _demoMode = AdmanDemoModeSimple;
    NSString *demo_mode = [[NSUserDefaults standardUserDefaults] stringForKey:@"demo_mode"];
    if ([demo_mode isEqualToString:@"default"]) {
        _demoMode = AdmanDemoModeDefault;
        if (_region == AdmanRegionUS || _region == AdmanRegionGlobal)
            siteId = ([_demoLang isEqualToString:@"en"]) && (_region == AdmanRegionGlobal) ? 1217 : 1043;
    } else if ([demo_mode isEqualToString:@"pg"]){
        _demoMode = AdmanDemoModePG;
        if (_region == AdmanRegionDemo)
            siteId = 1044;
    }
    
    if ([_demoLang isEqualToString:@"ru"])
        siteId = 1014;
    else if ([_demoLang isEqualToString:@"fr"])
        siteId = 1024;
    else if ([_demoLang isEqualToString:@"ja"])
        siteId = 1039;
    else if ([_demoLang isEqualToString:@"ja_cu"]){
        siteId = 1076;
        _region = AdmanRegionGlobal;
        _demoLang = @"ja";
    }

    if (_demoMode == AdmanDemoModeDefault && _region == AdmanRegionDemoEu && [_demoLang isEqualToString:@"ru"]) {
        _demoMode = AdmanDemoModeDefaultRUEu;
        _region = AdmanRegionEU;
        siteId = 332;
    }

    // _mainAdsManager = [Adman sharedManagerWithSiteId:912 region:AdmanRegionEU testMode:FALSE];
    // Initialization example without static object
    _mainAdsManager = [[Adman alloc] initWithSiteId:siteId testMode:NO withZoneId:nil region:_region playerId:nil];
    _mainAdsManager.delegate = self;
    _admanUI = [[AdmanUIExtension alloc] init];
    _admanUI.delegate = self;
    // [_adsManager overrideIP:@"45.58.124.166"];

    NSString *backendLocation = [[NSUserDefaults standardUserDefaults] stringForKey:@"backend_loc"];
    if ([backendLocation isEqualToString:@"usa"])
        [_mainAdsManager setRecognitionBackendLocation:AdmanRecognitionBackendLocationUSA];
    else if ([backendLocation isEqualToString:@"eu"])
        [_mainAdsManager setRecognitionBackendLocation:AdmanRecognitionBackendLocationEU];
    else if ([backendLocation isEqualToString:@"demo"])
        [_mainAdsManager setRecognitionBackendLocation:AdmanRecognitionBackendLocationDemo];

    AdmanRecognitionBackend backend;
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"backend"] length] > 0) {
        backend = [[NSUserDefaults standardUserDefaults] integerForKey:@"backend"];
        [_mainAdsManager setRecognitionBackend:backend];
    }

    float delay = fabs([[[NSUserDefaults standardUserDefaults] stringForKey:@"response_delay"] floatValue]);
    if (delay > 0) {
        [_mainAdsManager setResponseDelay:delay * -1];
        NSLog(@"Current voice response delay: %.4g", delay);
    }

    NSInteger timeout = [[[NSUserDefaults standardUserDefaults] stringForKey:@"server_timeout"] integerValue];
    [_mainAdsManager setServerTimeout:timeout];

    [_mainAdsManager setResponseTime:[[NSUserDefaults standardUserDefaults] integerForKey:@"response_time"]];
    [_mainAdsManager setServerVoiceActivityDetection:[[NSUserDefaults standardUserDefaults] boolForKey:@"vad"]];
    [_mainAdsManager setSiteVariable:@"consent_string" value:@"BOEFEAyOEFEAyAHABDENAI4AAAB9vABAASA"];

    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    // setup same prefferences for other adman instances
    if (_demoMode != AdmanDemoModeSimple) {
        _campaigns = [NSMutableArray arrayWithCapacity:6];
        for (int i = 0; i <= 5; i++) {
            // init instance
            [_campaigns addObject:[[Adman alloc] initWithSiteId:siteId testMode:NO withZoneId:nil region:_region playerId:nil]];
            // change default backend
            [_campaigns.lastObject setRecognitionBackendLocation:[_mainAdsManager recognitionBackendLocation]];
            if (backend)
                [_campaigns.lastObject setRecognitionBackend:backend];
            if (delay > 0)
                [_campaigns.lastObject setResponseDelay:delay * -1];
            [_campaigns.lastObject setServerTimeout:timeout];
            [_campaigns.lastObject setResponseTime:[[NSUserDefaults standardUserDefaults] integerForKey:@"response_time"]];
            [_campaigns.lastObject setServerVoiceActivityDetection:[[NSUserDefaults standardUserDefaults] boolForKey:@"vad"]];
            [_campaigns.lastObject setSiteVariable:@"consent_string" value:@"BOEFEAyOEFEAyAHABDENAI4AAAB9vABAASA"];
        }

        [self setUpDemoModeCampaigns];
    }
}

- (void)initAdmanBase {
    _mainAdsManager = [[Adman alloc] initWithSiteId:892 testMode:NO withZoneId:nil region:AdmanRegionEU playerId:nil];
    _mainAdsManager.delegate = self;
    //[_mainAdsManager setDefaultBannerSize:@"300x300"];
    _admanUI = [[AdmanUIExtension alloc] init];
    _admanUI.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *icon;
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:41];
    for (int i = 1; i <= 40; i++) {
        if (i < 10)
            icon = [UIImage imageNamed:[NSString stringWithFormat:@"loading_1000%i.gif", i]];
        else
            icon = [UIImage imageNamed:[NSString stringWithFormat:@"loading_100%i.gif", i]];
        if (icon != nil)
            [images addObject:icon];
    }

    [_loadingGif setAnimationImages:images];
    _loadingGif.animationDuration = 1.0;
    _loadingGif.animationRepeatCount = 0;
    [_loadingGif startAnimating];
    _demoLang = [[NSUserDefaults standardUserDefaults] stringForKey:@"lang"];
    if (_demoLang == nil || _demoLang.length == 0)
        _demoLang = [NSLocale currentLocale].languageCode;

    if (ADMAN_BUILD_MODE == BUILD_MODE_TRITON)
        [self initAdmanBase];
    else
        [self initAdmanFull];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _voiceActionsText.text = NSLocalizedStringFromTable(@"mtR-xC-RQP.text", @"Main",  @"mtR-xC-RQP.text");
    _voiceHelp.text = NSLocalizedStringFromTable(@"PDf-y7-rHG.text", @"Main",  @"PDf-y7-rHG.text");

    NSString *fontName = _voiceHelp.font.fontName;
    if ([_demoLang isEqualToString:@"ru"] || [[NSLocale currentLocale].languageCode isEqualToString:@"ja"]) {
        [_introTitle setFont:[UIFont fontWithName:fontName size:13]];
        [_voiceHelp setFont:[UIFont fontWithName:fontName size:19]];
        [_endInteractionLabel setFont:[UIFont fontWithName:fontName size:13]];
    }
    
    if ([[NSLocale currentLocale].languageCode isEqualToString:@"ja"])
        [_introTitle setFont:[UIFont fontWithName:fontName size:15]];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"debug"]) {
        [_debugView setHidden:NO];
        [_debugView.layer setBorderWidth:0.0f];
        [_debugView.layer setBorderColor:[[UIColor blackColor] CGColor]];

        [_voiceIntentsView setHidden:YES];
    }

    if (ADMAN_BUILD_MODE == BUILD_MODE_TRITON)
        [_voiceIntentsView setHidden:YES];

    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionGranted || ADMAN_BUILD_MODE == BUILD_MODE_TRITON)
        [self prepare];
    else
        [self showMicrophoneDialog];
}

- (void)presentNetworkErrorPopup {
    UIAlertController *restartView = [UIAlertController alertControllerWithTitle:@"Network error"
                                                                         message:@"Recived network/server error, please try again next time"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    [_activeAdPlayer stop];
    [restartView addAction:[UIAlertAction actionWithTitle:@"Restart"
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      [_toggleDemo setHidden:NO];
                                                      [self playbackSeekToStart];
                                                      [restartView removeFromParentViewController];
                                                  }]];
    [restartView addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      [_toggleDemo setHidden:NO];
                                                      [restartView removeFromParentViewController];
                                                  }]];
    [self presentViewController:restartView animated:YES completion:nil];
}

- (void)showMicrophoneDialog {
    if ([[AVAudioSession sharedInstance] recordPermission] == AVAudioSessionRecordPermissionDenied ||
        [[NSUserDefaults standardUserDefaults] objectForKey:@"lastMicroPermissionNotifyDate"] != nil) {
        NSDate *lastNotifyDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"admanLastMicroPermissionNotifyDate"];
        if (lastNotifyDate != nil) {
            NSDateComponents *diff = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:lastNotifyDate toDate:[NSDate new] options:NSCalendarWrapComponents];
            if ([diff day] <= 6) {
                [self.navigationController pushViewController:self animated:YES];
                return;
            }
        }
        [self showMessageAboutDisabledMicro];
       return;
    }

    UIAlertController *infoDialog = [UIAlertController
                                      alertControllerWithTitle:NSLocalizedStringFromTable(@"Microphone request title", @"Main", @"Microphone request title")
                                      message:NSLocalizedStringFromTable(@"Microphone access", @"Main", @"Microphone access")
                                      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:NSLocalizedStringFromTable(@"No", @"Main", @"No")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                   NSDate *today = [NSDate new];

                                   [defaults setObject:today forKey:@"lastMicroPermissionNotifyDate"];
                                   [defaults synchronize];
                                   [self.navigationController pushViewController:self animated:YES];
                                   [self showMessageAboutDisabledMicro];
                               }];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:NSLocalizedStringFromTable(@"Yes", @"Main", @"Yes")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [self.navigationController pushViewController:self animated:YES];
                                    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL permission) {
                                        if (permission == NO)
                                            [self showMessageAboutDisabledMicro];
                                        else {
                                            [self prepare];
                                        }
                                    }];
                                }];
    [infoDialog addAction:noButton];
    [infoDialog addAction:yesButton];

    [self presentViewController:infoDialog animated:NO completion:nil];
}

- (void)showMessageAboutDisabledMicro {
    UIAlertController * alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedStringFromTable(@"Microphone notify", @"Main", @"Microphone notify")
                                 message:@"Please allow the microphone access in order to activate voice control. That includes skipping ads, learning more and other useful commands. Do you want to enable voice control?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction
                      actionWithTitle:@"Ok"
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction *action) {
                      }]];

    [self presentViewController:alert animated:NO completion:nil];
    [self.navigationController pushViewController:self animated:YES];
}

- (void)prepareAdditionalCampaign:(NSTimer *)timer {
    NSNumber *index = timer.userInfo;
    Adman *instance = _campaigns[index.integerValue];
    [instance prepare:AdmanFormatAny type:AdmanTypeVoice];
}

- (void)prepare {
    _activeAdPlayer = _mainAdsManager;

    dispatch_async(dispatch_get_main_queue(), ^(void){
        NSInteger adsCount = [[[NSUserDefaults standardUserDefaults] stringForKey:@"ads_count"] integerValue];
        [_mainAdsManager prepare:AdmanFormatAny type:AdmanTypeVoice maxDuration:0 adsCount:adsCount];
    });

    if (_demoMode != AdmanDemoModeSimple) {
        [_campaigns.firstObject prepare:AdmanFormatAny type:AdmanTypeVoice];
        [_campaigns[1] prepare:AdmanFormatAny type:AdmanTypeVoice];
    }
}

- (int)getNumerCurrentCampaings {
    if (_activeAdPlayer == _mainAdsManager) {
        return 0;
    }
    return (_activeAdPlayer == NULL) ? -1 : ([_campaigns indexOfObject: _activeAdPlayer] + 1);
}

- (void)setUpDemoModeCampaigns {
    if (_demoMode == AdmanDemoModeDefault) {
        if (_region == AdmanRegionGlobal) {
            [_mainAdsManager setCampaignId:173];
            [_mainAdsManager setCreativeId:151];
            [_campaigns.firstObject setCampaignId:174];
            [_campaigns.firstObject setCreativeId:152];
            
            [_campaigns[1] setCampaignId:174];
            [_campaigns[1] setCreativeId:153];

            [_campaigns[2] setCampaignId:175];
            [_campaigns[2] setCreativeId:154];

            [_campaigns[3] setCampaignId:175];
            [_campaigns[3] setCreativeId:155];

            [_campaigns[4] setCampaignId:175];
            [_campaigns[4] setCreativeId:156];

            [_campaigns.lastObject setCampaignId:175];
            [_campaigns.lastObject setCreativeId:157];
        } else {
            [_mainAdsManager setCampaignId:191];

            [_campaigns.firstObject setCampaignId:192];
            [_campaigns.firstObject setCreativeId:120];
            
            [_campaigns[1] setCampaignId:192];
            [_campaigns[1] setCreativeId:121];
            
            [_campaigns[2] setCampaignId:193];
            [_campaigns[2] setCreativeId:122];
            
            [_campaigns[3] setCampaignId:193];
            [_campaigns[3] setCreativeId:123];
            
            [_campaigns[4] setCampaignId:193];
            [_campaigns[4] setCreativeId:124];
            
            [_campaigns.lastObject setCampaignId:193];
            [_campaigns.lastObject setCreativeId:125];
        }
    } else if (_demoMode == AdmanDemoModePG) {
        [_mainAdsManager setCampaignId:194];

        [_campaigns.firstObject setCampaignId:195];
        [_campaigns.firstObject setCreativeId:127];

        [_campaigns[1] setCampaignId:195];
        [_campaigns[1] setCreativeId:128];
    } else if (_demoMode == AdmanDemoModeDefaultRUEu) {
        [_mainAdsManager setCampaignId:8567];
        [_mainAdsManager setCreativeId:19079];

        [_campaigns.firstObject setCampaignId:8567];
        [_campaigns.firstObject setCreativeId:19081];

        [_campaigns[1] setCampaignId:8567];
        [_campaigns[1] setCreativeId:19082];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onDemoLaunched:(id)sender {
    [_launchDemo setHidden:YES];
    [_loadingGif setHidden:YES];
    [_voiceIntentsView setHidden:YES];
    [_toggleDemo setHidden:YES];

    if (_mainAdsManager.state == AdmanStateReadyForPlayback)
        [self play];
}

- (void)stop {
    [_activeAdPlayer stop];
    [_activeAdPlayer cancelLoading];
}

- (IBAction)onDemoRepeat:(id)sender {
    if (_loggingTimer) {
        [_loggingTimer invalidate];
        _loggingTimer = nil;
    }

    if ([_toggleDemo.currentTitle isEqualToString:NSLocalizedStringFromTable(@"Stop playback", @"Main", @"Stop playback")]) {
        [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Start playback", @"Main",  @"Start playback") forState:UIControlStateNormal];
        [self stop];
    } else {
        [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Stop playback", @"Main",  @"Stop playback") forState:UIControlStateNormal];
        [_endInteractionLabel setHidden:YES];
        if (_activeAdPlayer.state == AdmanStateReadyForPlayback)
            [self play];
        else {
            [_toggleDemo setHidden:YES];
            [_loadingGif setHidden:NO];
            [self prepare];
        }
    }
}

- (IBAction)onPlaybackDown:(id)sender {
    _loggingTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(enableDebugging) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_loggingTimer forMode:NSRunLoopCommonModes];
}

- (IBAction)onPlaybackUpOut:(id)sender {
    if (_loggingTimer) {
        [_loggingTimer invalidate];
        _loggingTimer = nil;
    }
}

- (IBAction)moreAction:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"url"]];
    [request setHTTPMethod: @"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request];
}

- (IBAction)skipAction:(id)sender {

}

- (void)refresh {
    [self playbackSeekToStart];
}

- (void)playbackSeekToStart {
    [_activeAdPlayer stop];
    [_admanUI hide];

    [self prepare];
    [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Start Over", @"Main",  @"Start Over") forState:UIControlStateNormal];
    [_toggleDemo setHidden:NO];
}

- (void)admanStateDidChange:(Adman *)sender {
    switch (sender.state) {
        case AdmanStatePlaying:
            [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Stop playback", @"Main",  @"Stop playback") forState:UIControlStateNormal];
            /*if (__lastState == AdmanStateAdChangedForPlayback) {
                [sender pause];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self.admanUI showPlayButton:nil];
                });
            }*/
            
            if (!self.pauseNextAd)
                break;

            const int numCampaign = [self getNumerCurrentCampaings];
            NSLog([NSString stringWithFormat:@"admanStateDidChange, number current campaings: %d", numCampaign]);
            if (_demoMode == AdmanDemoModeDefault) {
                if (numCampaign == 1) {//([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 120) {
                    [_campaigns[2] prepare:AdmanFormatAny type:AdmanTypeVoice];
                    [_campaigns[3] prepare:AdmanFormatAny type:AdmanTypeVoice];
                }
                else if (numCampaign == 2) {//([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 121) {
                    [_campaigns[4] prepare:AdmanFormatAny type:AdmanTypeVoice];
                    [_campaigns[5] prepare:AdmanFormatAny type:AdmanTypeVoice];
                }
                if (numCampaign > 0
                    ) {//([self.activeAdPlayer campaignId] >= 192) {
                    self.pauseNextAd = NO;
                    [sender pause];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [self.admanUI showPlayButton:nil];
                    });
                }
            } else if (_demoMode == AdmanDemoModePG && numCampaign >= 1) {//(_demoMode == AdmanDemoModePG && [_activeAdPlayer campaignId] >= 195) {
                self.pauseNextAd = NO;
                [sender pause];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self.admanUI showPlayButton:nil];
                });
            } else if (_demoMode == AdmanDemoModeDefaultRUEu && numCampaign >= 1) {
                self.pauseNextAd = NO;
                [sender pause];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self.admanUI showPlayButton:nil];
                });
            }
            break;

        case AdmanStatePlaybackCompleted:
            if (_demoMode != AdmanDemoModeSimple)
                [self choosePlaybackCompletedAction];
            else {
                [self setHidden:NO forUIView:_toggleDemo];
                [self adNone];
            }
            break;

        case AdmanStateFetchingInfo:
            if ([_launchDemo isHidden] && [_loadingGif isHidden])
                [self setHidden:NO forUIView:_toggleDemo];
            [_debugView setText:@""];
            break;

        case AdmanStateSpeechRecognizerStarted:
           break;

        case AdmanStateSpeechRecognizerStopped:
            break;

        case AdmanStateVoiceInteractionStarted:
            break;

        case AdmanStateStopped:
            [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Start playback", @"Main",  @"Start playback") forState:UIControlStateNormal];
            [_toggleDemo setHidden:NO];
            [self setHidden:NO forUIView:_toggleDemo];
        case AdmanStateVoiceInteractionEnded:
            break;

        case AdmanStateVoiceResponsePlaying:
            break;

        case AdmanStateReadyForPlayback:
            if ([_launchDemo isHidden] && [_loadingGif isHidden])
                [self play];
            else {
                [_loadingGif setHidden:YES];
                [_launchDemo setHidden:NO];
            }
            break;
        case AdmanStateError:
            [self setHidden:NO forUIView:_toggleDemo];
            sender.delegate = nil;
            if (sender.error.code == 504) {
                [self presentNetworkErrorPopup];

                _activeAdPlayer = _mainAdsManager;
                _activeAdPlayer.delegate = self;
            }
        case AdmanStateAdNone:
            [self adNone];
            [self setHidden:YES forUIView:_loadingGif];
            break;
        default:
            break;
    }
    __lastState = sender.state;
}

- (void)choosePlaybackCompletedAction {
    const int numCampaign = [self getNumerCurrentCampaings];
    NSLog([NSString stringWithFormat:@"choosePlaybackCompletedAction, number current campaings: %d", numCampaign]);
    if ([_reaction isEqualToString:@"silence"] || [_reaction isEqualToString:@"skip"] || [_reaction isEqualToString:@"unknown"]) {
        [self showTryAgainView];
        return;
    }
    if (_demoMode == AdmanDemoModeDefault && numCampaign >= 3) {
        // creativeId >= 122
        [self showTryAgainView];
        return;
    }
    
    if (_demoMode == AdmanDemoModePG) {
        // campaignId == 195
        if (numCampaign > 0 && numCampaign < 3) {
            [self showTryAgainView];
            return;
        }
    }

    if (_demoMode == AdmanDemoModeDefaultRUEu) {
        if (numCampaign == 1) {
            [self showTryAgainView];
            return;
        }
    }

    _activeAdPlayer.delegate = nil;
    [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Start playback", @"Main",  @"Start playback") forState:UIControlStateNormal];

    if (_demoMode == AdmanDemoModeDefault) {
        if (numCampaign == 0) {
            //([_activeAdPlayer campaignId] == 191) {
            if ([_reaction isEqualToString:@"positive"])
                _activeAdPlayer = _campaigns.firstObject;
            else
                _activeAdPlayer = _campaigns[1];
        } else if (numCampaign == 1) {
            //([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 120) {
            if ([_reaction isEqualToString:@"positive"])
                _activeAdPlayer = _campaigns[2];
            else
                _activeAdPlayer = _campaigns[3];
        } else if (numCampaign == 2) {
            //([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 121) {
            if ([_reaction isEqualToString:@"positive"])
                _activeAdPlayer = _campaigns[4];
            else
                _activeAdPlayer = _campaigns[5];
        }
    } else if (_demoMode == AdmanDemoModePG) {
        if (numCampaign == 0)
            // campaignId == 194
            if ([_reaction isEqualToString:@"positive"])
                _activeAdPlayer = _campaigns.firstObject;
            else {
                _activeAdPlayer = _campaigns[1];
            }
    } else if (_demoMode == AdmanDemoModeDefaultRUEu) {
        if (numCampaign == 0) {
            // creativeID == 19079
            if ([_reaction isEqualToString:@"positive"])
                _activeAdPlayer = _campaigns.firstObject;
            else
                _activeAdPlayer = _campaigns[1];
        } else if (numCampaign == 2) {
            // creativeId == 19082
            _activeAdPlayer = _campaigns.firstObject;
        }
    }

    _activeAdPlayer.delegate = self;
    [self play];
}

- (void)showTryAgainView {
    _activeAdPlayer.delegate = nil;
    _activeAdPlayer = _mainAdsManager;
    _activeAdPlayer.delegate = self;
    [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Start Over", @"Main",  @"Start Over") forState:UIControlStateNormal];
    [_toggleDemo setHidden:NO];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"debug"])
        [_endInteractionLabel setHidden:NO];
}

- (void)setHidden:(bool)hidden forUIView:(UIView *)view {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [view setHidden:hidden];
    });
}

- (void)play {
    AVAsset *micOnAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Mic_on_new" ofType:@"mp3"]]];
    AVAsset *micOffAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Mic_off_new" ofType:@"mp3"]]];

    for (AdmanBanner *ad in _activeAdPlayer.adsList) {
        [ad.assets setObject:[((AVURLAsset *) micOnAsset).URL absoluteString] forKey:AdmanBannerAssetMicOnUrl];
        [ad.assets setObject:micOnAsset forKey:AdmanBannerAssetMicOnCache];
        [ad.assets setObject:[((AVURLAsset *) micOffAsset).URL absoluteString] forKey:AdmanBannerAssetMicOffUrl];
        [ad.assets setObject:micOffAsset forKey:AdmanBannerAssetMicOffCache];
    }
    [_toggleDemo setHidden:NO];

    [self log:@"\nCreative loaded"];
    float delay = fabs([[[NSUserDefaults standardUserDefaults] stringForKey:@"response_delay"] floatValue]);
    if (delay > 0)
        [self log:[NSString stringWithFormat:@"Voice response delayed for %.4g seconds", delay]];
    
    if ([_launchDemo isHidden] && [_loadingGif isHidden]) {
        [_activeAdPlayer reportAdEvent:@"can_show"];
        self.pauseNextAd = YES;

        [_toggleDemo setHidden:YES];
        [_admanUI show:self];
        [_activeAdPlayer play];
        [self.view bringSubviewToFront:_toggleDemo];

        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"debug"]) {
            [_toggleDemo setHidden:YES];
            [self.view bringSubviewToFront:_debugScrpllView];
        }
    }
}

- (void)adNone {
    dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self log:@"\nDemo app stopped"];
            [_toggleDemo setTitle:NSLocalizedStringFromTable(@"Start playback", @"Main",  @"Start playback") forState:UIControlStateNormal];
        });
        [_launchDemo setHidden:YES];
        [_loadingGif setHidden:YES];
        [_voiceIntentsView setHidden:YES];
        [_toggleDemo setHidden:YES];
        [self showTryAgainView];
    });
}

- (void)log:(NSString *)message {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss:SSS"];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _debugView.text = [_debugView.text stringByAppendingString:[NSString stringWithFormat:@"\n%@: %@", [formatter stringFromDate:[NSDate date]], message]];
    });

}

- (void)phraseRecognized:(nonnull NSDictionary *)result {
    _reaction = [result objectForKey:@"action"];
}

- (void)errorReceived:(NSError *)error {
    [self log:[error localizedDescription]];
}

- (IBAction)settingsBtnTouched:(id)sender {

}

@end
