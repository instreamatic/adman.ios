/**
* Copyright 2018 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import "AdmanUIExtension.h"
#import "Adman_framework/AdmanBanner.h"

@implementation AdmanUIExtension

- (id)init {
    self = [super init];
    if (self) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"debug"])
            _devMode = YES;
    }
    return self;
}

- (void)initBaseView {
    [super initBaseView];

    [self.rewindView setImage:[AdmanUIExtension loadImageFrom:REFRESH_BTN_SOURCE]];
    for (UIGestureRecognizer *rec in self.rewindView.gestureRecognizers)
        [self.rewindView removeGestureRecognizer:rec];
    [AdmanUIExtension addSelector:@selector(refreshDelegate:) forView:self.rewindView withTarget:self];
}

- (void)initVoiceView {
    [super initVoiceView];
    [self.positiveView setImage:nil];
    [self.negativeView setImage:nil];
    [self.intentLabel setText:@""];
}

- (void)show:(UIViewController *)rootVC {
    [super show:rootVC];

    //[self.bannerView removeGestureRecognizer:self.bannerView.gestureRecognizers.firstObject];

    if (_devMode) {
        [self.remainingTime setHidden:YES];
        [self.view setBackgroundColor:nil];
    }
}

- (void)update:(AdmanBanner *)ad {
    [super update:ad];
    
    if (_devMode)
        [self.bannerView setImage:nil];
}

- (void)refreshDelegate:(UIGestureRecognizer *)uiGestureRecognizer {
    [self.delegate refresh];
}

- (void)admanVoiceNotification:(NSNotification *)notification {
    [super admanVoiceNotification:notification];
    NSDictionary *dict = notification.userInfo;
    NotificationAdmanVoice *message = [dict valueForKey:NOTIFICATION_ADMAN_VOICE_KEY];
    if (message == nil)
        return;

    switch (message.event) {
        case AdmanVoiceEventRecognizerStarted:{
            break;
        }
        default:
            break;
    }

}

@end
