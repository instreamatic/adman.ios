/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "VACRecorder.h"

#define VACRecorderNumberBuffers 3

static const int kNumberOfChanels = 1;
static const float kSampleRate = 16000.0;
static const int kBitsPerChannel = 32;

typedef struct {
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue;
    AudioQueueBufferRef          buffers[VACRecorderNumberBuffers];
    AudioFileID                  audioFile;
    UInt32                       bufferByteSize;
    SInt64                       currentPacket;
    bool                         running;
} VACRecorderState;

@implementation VACRecorder {
    VACRecorderState state;
    AudioFileID audioFile;
    SInt64 recordPackets;
}

void VACRecorderAudioInputCallback(void *inUserData,
                                   AudioQueueRef inAQ,
                                   AudioQueueBufferRef inBuffer,
                                   const AudioTimeStamp *inStartTime,
                                   UInt32 inNumberPackets,
                                   const AudioStreamPacketDescription *inPacketDescs) {
    
    VACRecorder *recorder = (__bridge VACRecorder *)(inUserData);
    [recorder.delegate processSampleWithCapacity:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
    recorder->recordPackets += inNumberPackets;
    
    if (recorder.isRunning) {
        AudioQueueEnqueueBuffer(recorder->state.queue, inBuffer, 0, NULL);
    }
}

void VACRecorderCalculateBufferSize (AudioStreamBasicDescription *audioStreamDescription,
                                     AudioQueueRef audioQueue,
                                     Float64 seconds,
                                     UInt32 *outBufferSize)
{
    static const int maxBufferSize = 0x50000;
    int maxPacketSize = audioStreamDescription->mBytesPerPacket;

    if (maxPacketSize == 0) {
        UInt32 maxVBRPacketSize = sizeof(maxPacketSize);
        AudioQueueGetProperty ( audioQueue,
                               kAudioQueueProperty_MaximumOutputPacketSize,
                               &maxPacketSize,
                               &maxVBRPacketSize );
    }

    Float64 numBytesForTime = audioStreamDescription->mSampleRate * maxPacketSize * seconds;
    *outBufferSize = (UInt32)(numBytesForTime < maxBufferSize
                              ? numBytesForTime
                              : maxBufferSize);
}

-(BOOL)isRunning
{
    return state.running;
}

- (void)setupAudioFormat16bit:(AudioStreamBasicDescription *)format {
    format->mFormatID = kAudioFormatLinearPCM;
    format->mFormatFlags = kAudioFormatFlagIsSignedInteger;
    format->mBitsPerChannel = 16;
    format->mChannelsPerFrame = 1;
    format->mBytesPerPacket = format->mBytesPerFrame = 2;  // (format->mBitsPerChannel / 8) * format->mChannelsPerFrame
    format->mFramesPerPacket = 1;
    format->mSampleRate = kSampleRate;
}

- (void)setupAudioFormat:(AudioStreamBasicDescription *)format {
    format->mFormatID = kAudioFormatLinearPCM;
    format->mFormatFlags = kAudioFormatFlagsNativeFloatPacked;
    format->mBitsPerChannel = kBitsPerChannel;
    format->mChannelsPerFrame = kNumberOfChanels;
    format->mBytesPerPacket = format->mBytesPerFrame = 4;  // (format->mBitsPerChannel / 8) * format->mChannelsPerFrame
    format->mFramesPerPacket = 1;
    format->mSampleRate = kSampleRate;
}

- (void)startRecording {
    [self setupAudioFormat16bit:&state.dataFormat];

    state.currentPacket = 0;
    OSStatus status = AudioQueueNewInput(&state.dataFormat,
                                         VACRecorderAudioInputCallback,
                                         (__bridge void *)(self),
                                         CFRunLoopGetCurrent(),
                                         kCFRunLoopCommonModes,
                                         0,
                                         &state.queue);
    VACRecorderCalculateBufferSize(&state.dataFormat,
                                   state.queue,
                                   0.02,
                                   &state.bufferByteSize);


    if (status == 0) {

        for (int i = 0; i < VACRecorderNumberBuffers; i++) {
            AudioQueueAllocateBuffer(state.queue, state.bufferByteSize, &state.buffers[i]);
            AudioQueueEnqueueBuffer(state.queue, state.buffers[i], 0, NULL);
        }

        state.running = true;
        status = AudioQueueStart(state.queue, NULL);
    }
}

- (void)stopRecording {
    state.running = false;
    
    AudioQueueStop(state.queue, true);
    AudioFileClose(audioFile);
    
    for (int i = 0; i < VACRecorderNumberBuffers; i++) {
        AudioQueueFreeBuffer(state.queue, state.buffers[i]);
    }
    
    AudioQueueDispose(state.queue, true);
    AudioFileClose(state.audioFile);
}

+ (NSData *)wavHeader {
    
    int headerSize = 44;

    unsigned long totalAudioLen = 0; //use dummy for stream
    unsigned long totalDataLen = 0; //use dummy for stream
    unsigned long longSampleRate = kSampleRate;
    unsigned int channels = kNumberOfChanels;
    unsigned long byteRate = 2 * longSampleRate * channels;
    Byte *header = (Byte*)malloc(44);
    header[0] = 'R';  // RIFF/WAVE header
    header[1] = 'I';
    header[2] = 'F';
    header[3] = 'F';
    header[4] = (Byte) (totalDataLen & 0xff);
    header[5] = (Byte) ((totalDataLen >> 8) & 0xff);
    header[6] = (Byte) ((totalDataLen >> 16) & 0xff);
    header[7] = (Byte) ((totalDataLen >> 24) & 0xff);
    header[8] = 'W';
    header[9] = 'A';
    header[10] = 'V';
    header[11] = 'E';
    header[12] = 'f';  // 'fmt ' chunk
    header[13] = 'm';
    header[14] = 't';
    header[15] = ' ';
    header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
    header[17] = 0;
    header[18] = 0;
    header[19] = 0;
    header[20] = 1;  // format = 1
    header[21] = 0;
    header[22] = (Byte) channels;
    header[23] = 0;
    header[24] = (Byte) (longSampleRate & 0xff);
    header[25] = (Byte) ((longSampleRate >> 8) & 0xff);
    header[26] = (Byte) ((longSampleRate >> 16) & 0xff);
    header[27] = (Byte) ((longSampleRate >> 24) & 0xff);
    header[28] = (Byte) (byteRate & 0xff);
    header[29] = (Byte) ((byteRate >> 8) & 0xff);
    header[30] = (Byte) ((byteRate >> 16) & 0xff);
    header[31] = (Byte) ((byteRate >> 24) & 0xff);
    header[32] = (Byte) (kNumberOfChanels * 16 / 8);  // block align
    header[33] = 0;
    header[34] = 16;  // bits per sample
    header[35] = 0;
    header[36] = 'd';
    header[37] = 'a';
    header[38] = 't';
    header[39] = 'a';
    header[40] = (Byte) (totalAudioLen & 0xff);
    header[41] = (Byte) ((totalAudioLen >> 8) & 0xff);
    header[42] = (Byte) ((totalAudioLen >> 16) & 0xff);
    header[43] = (Byte) ((totalAudioLen >> 24) & 0xff);

    return [NSData dataWithBytes:header length:headerSize];
}

@end
