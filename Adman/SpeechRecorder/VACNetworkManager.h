/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>
#import <SocketRocket/SRWebSocket.h>

typedef void (^VACConnectionInit)(void);

@class VACNetworkManager;

typedef NS_ENUM(NSInteger, ServerVoiceMessageType) {
    VoiceMessageAudioStart,
    VoiceMessageTranscript,
    VoiceMessageResult
};

@protocol VACNetworkManagerDelegate <NSObject>

- (void)networkManagerConnectionDidOpened:(nonnull VACNetworkManager *)manager;
- (void)networkManager:(nonnull VACNetworkManager *)manager didResponseRecived:(nullable NSDictionary *)response;
- (void)networkManager:(nonnull VACNetworkManager *)manager didFailWithError:(nullable NSError *)error;
- (void)networkManager:(nonnull VACNetworkManager *)manager didSessionEnded:(nullable NSDictionary *)response;
- (void)networkManager:(nonnull VACNetworkManager *)manager didCommandRecived:(nullable NSString *)command;

@end

@interface VACNetworkManager : NSObject<SRWebSocketDelegate>

@property (weak, nonatomic, nullable) id<VACNetworkManagerDelegate> delegate;

+ (void)setWebsocketUrl:(nonnull NSString *)url;
+ (nonnull NSString *)getWebSocketUrl;

- (void)openConnection:(nullable VACConnectionInit)handler withOptions:(nonnull NSDictionary *)options;
- (void)closeConnection;

- (bool)isOpen;
- (void)send:(nonnull NSData *)data;
- (void)sendData:(nonnull NSData *) data;
- (nonnull NSMutableData *)toMessage:(nonnull NSString *)msg withSamples:(nullable NSData *)data;
@end
