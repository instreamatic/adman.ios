/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "VACNetworkManager.h"
#import "VACRecorder.h"

static NSString *AdmanRecognitionUrl = nil;

@interface VACNetworkManager ()

@property bool headerSended;
@property NSDictionary *connectionOptions;
@property SRWebSocket *connection;

@property VACConnectionInit initHandler;

@end

@implementation VACNetworkManager

+ (void)setWebsocketUrl:(NSString *)url {
    AdmanRecognitionUrl = url;
}

+ (NSString *)getWebSocketUrl {
    return AdmanRecognitionUrl;
}

- (void)openConnection:(VACConnectionInit)handler withOptions:(NSDictionary *)options {
    NSLog(@"voice server url: %@", AdmanRecognitionUrl);

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:AdmanRecognitionUrl]];
    if (@available(iOS 12.0, *))
        [request setNetworkServiceType:NSURLNetworkServiceTypeBackground];

    _connection = [[SRWebSocket alloc] initWithURLRequest:request];
    _initHandler = handler;
    _headerSended = false;
    _connectionOptions = [NSDictionary dictionaryWithDictionary:options];

    _connection.delegate = self;
    [_connection open];
}

- (NSMutableData *)toMessage:(nonnull NSString *)msg withSamples:(nullable NSData *)data {
    NSMutableData *request = [NSMutableData data];
    NSUInteger mSize = [[msg dataUsingEncoding:NSUTF8StringEncoding] length];

    Byte bSize[] = { (mSize >> 8) & 0xff, mSize & 0xff };
    [request appendBytes:bSize length:2];
    [request appendData:[msg dataUsingEncoding:NSUTF8StringEncoding]];
    if (data)
        [request appendData:data];
    return request;
}

- (void)closeConnection {
    if (_connection == nil)
        return;

    [_connection close];
    _initHandler = nil;
    _connection = nil;
    _headerSended = false;

}

- (bool)isOpen {
    return _connection != nil && _connection.readyState == SR_OPEN;
}

- (void)send:(NSData *)data
{
    if (_connection.readyState == SR_OPEN)
        [_connection sendData:data error:nil];
}

- (void)sendData:(NSData *)data {
    if (_connection.readyState == SR_OPEN && _headerSended)
        [_connection sendData:[self toMessage:@"content-type: audio/wav\r\npath: audio.data\r\n" withSamples:data] error:nil];
}

// MARK: webSocket delegate

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    if (_connection.readyState != SR_OPEN)
        return;

    if (_initHandler != nil) {
        _initHandler();
        _initHandler = nil;
    }

    NSString *request_data = [NSString stringWithFormat:@"{\"partner_id\": 1, \"site_id\": %i, \"ad_id\": \"%@\", \"response_delay\": %@, \"device_info\": \"%@\", \"idfa\": \"%@\", \"vad\": %i}",
                              ((NSNumber *) _connectionOptions[@"site_id"]).intValue,
                              _connectionOptions[@"ad_id"],
                              _connectionOptions[@"response_delay"],
                              _connectionOptions[@"device_info"],
                              _connectionOptions[@"idfa"],
                              ((NSNumber *) _connectionOptions[@"vad"]).intValue];
    [_connection sendData:[self toMessage:@"content-type: application/json\r\npath: request\r\n\r\n" withSamples:[request_data dataUsingEncoding:NSUTF8StringEncoding]] error:nil];
    NSLog(@"Sending request message, body: {%@}", request_data);
    // NSLog(@"Adman.wss: headers sended");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {

    NSArray *result = [message componentsSeparatedByString:@"\r\n\r\n"];
    NSMutableDictionary *headers = [NSMutableDictionary dictionary];

    for (NSString *header in [result[0] componentsSeparatedByString:@"\r\n"]) {
        NSArray *list = [header componentsSeparatedByString:@":"];
        [headers setValue:[list[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:[list[0] lowercaseString]];
    }

    // start message
    if ([headers[@"path"] isEqualToString:@"audio.start"] && _connection.readyState == SR_OPEN) {
        NSLog(@"Send wav header to server");
        [_connection sendData:[self toMessage:@"content-type: audio/wav\r\npath: audio.data\r\n" withSamples:[VACRecorder wavHeader]] error:nil];
        _headerSended = true;
        [self.delegate networkManagerConnectionDidOpened:self];
        return;
    }

    //
    if ([headers[@"path"] isEqualToString:@"audio.stop"]) {
        [_delegate networkManager:self didCommandRecived:headers[@"path"]];
        return;
    }

    if ([headers[@"content-type"] isEqualToString:@"application/json"] || [headers[@"content-type"] isEqualToString:@"text/json"]) {
        NSDictionary *obj = [NSJSONSerialization JSONObjectWithData:[result[1] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
        // NSLog(@"Websocket %@ resp: %@", webSocket, message);

        // VoiceTranscript message, ignore silence
        // obj[@"transcript"] can be nil in iOS < 12
        if ([headers[@"path"] isEqualToString:@"voice.transcript"] && obj[@"transcript"] != nil) {
            // obj[@"transcript"] is NSNul in iOS 12 >
            if (![obj[@"transcript"] isEqual:[NSNull null]] && [obj[@"transcript"] length] > 0)
                [_delegate networkManager:self didResponseRecived:obj];
        }

        // Voice result message
        if ([headers[@"path"] isEqualToString:@"voice.result"]) {
            // NSLog(@"Websocket, end result: %@", obj);
            [_delegate networkManager:self didSessionEnded:obj];
            [self closeConnection];
        }

        // error message
        if ([headers[@"path"] isEqualToString:@"error"]) {
            [_delegate networkManager:self didFailWithError:[NSError errorWithDomain:NSNetServicesErrorDomain
                                                                                code:NSURLErrorCannotDecodeRawData userInfo:obj]];
            [self closeConnection];
        }
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [_delegate networkManager:self didFailWithError:error];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(nullable NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"Connection closed with reason %@, code: %ld", reason, (long)code);
    if (_connection != nil) {
        [_delegate networkManager:self didFailWithError:[NSError errorWithDomain:NSNetServicesErrorDomain
                                                                            code:NSURLErrorTimedOut userInfo:nil]];
        [self closeConnection];
    }
}

@end
