/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>

//! Project version number for VoiceAdControl.
FOUNDATION_EXPORT double VoiceAdControlVersionNumber;

//! Project version string for VoiceAdControl.
FOUNDATION_EXPORT const unsigned char VoiceAdControlVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VoiceAdControl/PublicHeader.h>

#import "VACRecognizer.h"
