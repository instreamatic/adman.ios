/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "VACNetworkManager.h"

typedef NS_ENUM(NSUInteger, VACRecognizerState) {
    VACRecognizerStateNone,
    VACRecognizerStateReady,
    VACRecognizerStateListening
};

@class VACRecognizer;

@protocol VACRecognizerDelegate <NSObject>

- (void)recognizer:(VACRecognizer *)recogizer didChangeState:(VACRecognizerState)state;
- (void)recognizer:(VACRecognizer *)recogizer didRecognizeCommand:(NSDictionary *)command;
- (void)recognizer:(VACRecognizer *)recogizer didSessionEnded:(NSDictionary *)result;
- (void)recognizer:(VACRecognizer *)recogizer didFailWithError:(NSError *)error;
- (void)recognizer:(VACRecognizer *)recogizer didConnectionOpened:(VACRecognizerState)state;

@end


@interface VACRecognizer : NSObject

@property (nonatomic, readonly) VACRecognizerState state;
@property (nonatomic, weak) id <VACRecognizerDelegate> delegate;

- (void)start:(VACConnectionInit)onStart withOptions:(NSDictionary *)options;
- (void)stop;

- (void)startListening;
- (void)stopListening;
- (void)endSpeechDetection;
- (void)closeConnection;

@end
