/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "VACRecognizer.h"
#import "VACRecorder.h"
#import "VACNetworkManager.h"

@interface VACRecognizer () <VACRecorderDelegate, VACNetworkManagerDelegate>
{
    NSInteger   samplescounter;
    NSString    *docsDir;
    NSArray     *dirPaths;
}

@property (nonatomic) NSMutableArray *samplesPartialBuffer;
@property (nonatomic) VACRecorder *recorder;
@property (nonatomic) VACNetworkManager *networkManager;
@property (nonatomic, readwrite) VACRecognizerState state;

@end

@implementation VACRecognizer

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _state = VACRecognizerStateNone;
        _recorder = [VACRecorder new];
        _recorder.delegate = self;
        _samplesPartialBuffer = [NSMutableArray arrayWithCapacity:1000];
        _networkManager = [VACNetworkManager new];
        _networkManager.delegate = self;

        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
    }

    return self;
}

- (void)setState:(VACRecognizerState)state {
    if (_state != state) {
        _state = state;
        [self.delegate recognizer:self didChangeState:state];
    }
}

- (void)start:(VACConnectionInit)onStart withOptions:(NSDictionary *)options {
    samplescounter = 0;

    self.state = VACRecognizerStateReady;
    NSLog(@"libAdman: network recognizer started");
    [_networkManager openConnection:^() {
        if (onStart)
            onStart();
        self.state = VACRecognizerStateListening;
    } withOptions:options];
}

- (void)stop {
    if (_state == VACRecognizerStateNone)
        return;

    self.state = VACRecognizerStateNone;
    [_recorder stopRecording];
    [self endSpeechDetection];
    [_samplesPartialBuffer removeAllObjects];
    NSLog(@"libAdman: network recognizer stopped");
}

- (void)closeConnection {
    [_networkManager closeConnection];
}

- (void)openSocket:(VACConnectionInit)socketDidOpen {
    
}

- (void)startListening {
    NSLog(@"libAdman: voice recording started");

    [_samplesPartialBuffer removeAllObjects];
    if (!_recorder.isRunning)
        [_recorder startRecording];
    self.state = VACRecognizerStateListening;
}

- (void)stopListening {
    NSLog(@"libAdman: voice recording stopped");

    if (_recorder.isRunning)
        [_recorder stopRecording];
    self.state = VACRecognizerStateNone;
}

- (void)endSpeechDetection {
    [_networkManager send:[_networkManager toMessage:@"content-type: audio/wav\r\npath: audio.end\r\n" withSamples:nil]];
}

#pragma mark - VACRecorderDelegate

- (void)processSampleWithCapacity:(UInt32)audioDataBytesCapacity
                        audioData:(void *)audioData
{
    NSData *sampleData = [NSData dataWithBytes:audioData length:audioDataBytesCapacity];
    // NSLog(sampleData.description);

    if (![_networkManager isOpen] || [_samplesPartialBuffer count] > 0)
        [_samplesPartialBuffer addObject:sampleData];
    else
        [self.networkManager sendData:sampleData];

    // NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld.raw", samplescounter++]]];
    // [sampleData writeToFile:databasePath atomically:YES];
}

#pragma mark - VACNetworkManagerDelegate

- (void)networkManager:(VACNetworkManager *)manager didCommandRecived:(NSString *)command {
    // NSLog(@"networkManagerDidCommandRecived %@", command);
    if ([command isEqualToString:@"audio.stop"]) {
        [self stopListening];
    }
}

- (void)networkManagerConnectionDidOpened:(VACNetworkManager *)manager {
    if ([_samplesPartialBuffer count] > 0)
        while ([_samplesPartialBuffer count] > 0) {
            id sample = [_samplesPartialBuffer objectAtIndex:0];
            [_samplesPartialBuffer removeObjectAtIndex:0];
            [self.networkManager sendData:sample];
        }

    NSLog(@"Conection opened: %@", [VACNetworkManager getWebSocketUrl]);
    [_delegate recognizer:self didConnectionOpened:_state];
}

-(void)networkManager:(VACNetworkManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"libAdman.webSocket: didFailWithError %@", error);
    // ignore Nuance silence recognition error
    if (error.code == 1015) {
        [_delegate recognizer:self didRecognizeCommand:nil];
    }

    [self stop];
    [_delegate recognizer:self didFailWithError:error];
}

- (void)networkManager:(VACNetworkManager *)manager didResponseRecived:(NSDictionary *)response {
    [_delegate recognizer:self didRecognizeCommand:response];
}

- (void)networkManager:(VACNetworkManager *)manager didSessionEnded:(NSDictionary *)response {
    [_delegate recognizer:self didSessionEnded:response];
    [_samplesPartialBuffer removeAllObjects];
}

@end
