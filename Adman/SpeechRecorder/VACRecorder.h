/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>
#import <AudioToolbox/AudioToolbox.h>

@protocol VACRecorderDelegate <NSObject>

- (void)processSampleWithCapacity:(UInt32)audioDataBytesCapacity
                        audioData:(void *)audioData;

@end

@interface VACRecorder : NSObject

@property (nonatomic, weak) id <VACRecorderDelegate> delegate;

@property (readonly, getter=isRunning) BOOL running;

- (void)startRecording;
- (void)stopRecording;

+ (NSData *)wavHeader;

@end
