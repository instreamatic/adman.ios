/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>
#import "AdmanWebsocketDtmfListener.h"

static NSString *AdmanDtmfWebsocketDomain = @"wss://dtmf.instreamatic.com:8081/dtmfws/sub";

@implementation AdmanWebsocketDtmfListener

- (void)start:(NSString *)station_key {
    NSString *domain = [NSString stringWithFormat: @"%@/%@", AdmanDtmfWebsocketDomain, station_key];
    NSLog(@"Open connection over websocket %@", domain);
    NSMutableURLRequest *r = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:domain]];
    _connection = [[SRWebSocket alloc] initWithURLRequest:r];
    _connection.delegate = self;
    [_connection open];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        if (self.connection && self.connection.readyState != SR_OPEN) {
            NSLog(@"Failed to open websocket");
            [self.connection close];
            self.connection = NULL;
            [self start:station_key];
        }
    });

    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(pingConnection) userInfo:nil repeats:YES];
}

- (void)pingConnection {
    if (!_connection)
        return;
    if (_connection.readyState == SR_OPEN)
        [_connection sendPing:nil error:nil];
}

- (void)stop {
    [_connection close];
    _connection = nil;
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"libAdman: dtmf listener opened");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSError *jsonError;
    NSLog(@"libAdman: dtmf recived: %@", message);
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    if ([json[@"label"] isEqualToString:@"IN"])
        [self.delegate dtmfIn];
    else if ([json[@"label"] isEqualToString:@"OUT"])
        [self.delegate dtmfOut];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"libAdman: Dtmf listener recive error, %@", error.localizedDescription);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"libAdman: dtmf listener closed");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    // NSLog(@"libAdman: dtmf listener, pong recived");
}

@end
