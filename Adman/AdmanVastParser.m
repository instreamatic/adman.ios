/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#import "AdmanVastParser.h"
#import "AdmanVastParserDelegate.h"
#import "AdmanConstants.h"

#import "NSString+HTML.h"

@interface AdmanVastParser ()

@property NSArray *adsList;
@property (strong) NSURLSessionTask *operation;

- (void)parseWrapper:(AdmanBannerWrapper *)ad onComplete:(AdmanOnFulfill())fulfill;

@end

@implementation AdmanVastParser

- (void)parse:(NSString *)url params:(NSDictionary *)params {

    [self startLoadingAdsFromUrl:url withParams:params
                      onComplete:^(id adslist) {
        [self.delegate creativeDidParsed:adslist];
    }
                          onFail:^(NSError *err) {
        [self.delegate creativeDidFailed:err];
    }];
}

- (void)startLoadingAdsFromUrl:url withParams:(NSDictionary *)params
                    onComplete:(AdmanOnFulfill())fulfill onFail:(AdmanOnFail())fail {
    self.adsList = nil;
    self.adsLoaded = 0;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^void() {
        self.operation = [AdmanVastParser request:url params:params onComplete:^(NSData *responseObject, NSURLResponse* response, NSError* error) {
            NSLog(@"libAdmam.xmlparser: Data recived from ad server");
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode != 200) {
                fail(error);
                return;
            }

            NSXMLParser *XMLParser = [[NSXMLParser alloc] initWithData:responseObject];
            [XMLParser setShouldProcessNamespaces:YES];
            AdmanVastParserDelegate *parser = [[AdmanVastParserDelegate alloc] init];
            XMLParser.delegate = parser;
            [XMLParser parse];

            if ([parser.adsList count] > 0) {
                NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"sequence" ascending:TRUE];
                NSArray *sortDescriptors = [NSArray arrayWithObject:sortDesc];
                self.adsList = [parser.adsList sortedArrayUsingDescriptors:sortDescriptors];
                for (AdmanBannerWrapper *item in self.adsList) {
                    if (item.redirectUrl != nil)
                        [self parseWrapper:item onComplete:fulfill];
                    else
                        [self adLoaded:item onComplete:fulfill];
                }
            } else {
                dispatch_sync(dispatch_get_main_queue(), ^void() { fulfill(parser.adsList); });
            }
        }];
        [self.operation resume];
    });
}

- (void)adLoaded:(AdmanBannerWrapper *)ad onComplete:(AdmanOnFulfill())fulfill {
    _adsLoaded += 1;

    if (_adsLoaded == [_adsList count]) {
        fulfill(self.adsList);
        self.adsList = nil;
    }
}

- (void)cancel {
    [_operation cancel];
}

- (void)parseWrapper:(AdmanBannerWrapper *)ad onComplete:(AdmanOnFulfill())fulfill {
    ad.redirectUrl = [self.delegate creativeDidParseRedirect:ad.redirectUrl];
    NSLog(@"Start parsing redirect to %@", ad.redirectUrl);
    _operation = [AdmanVastParser request:ad.redirectUrl params:nil onComplete:^(NSData *responseObject, NSURLResponse* response, NSError* err) {
        if (err != nil) {
            [ad setUrl:nil];
            [ad setSourceUrl:nil];
            [ad setRedirectUrl:nil];
            NSLog(@"Failed to parse redirect");
            [self adLoaded:ad onComplete:fulfill];
            return;
        }
        NSXMLParser *XMLParser = [[NSXMLParser alloc] initWithData:responseObject];
        [XMLParser setShouldProcessNamespaces:YES];
        AdmanVastParserDelegate *delegate = [[AdmanVastParserDelegate alloc] init];
        XMLParser.delegate = delegate;

        [ad setRedirectUrl:nil];
        [delegate setWrappedAd:ad];
        [XMLParser parse];
        if (ad.redirectUrl)
            [self parseWrapper:ad onComplete:fulfill];
        else
            [self adLoaded:ad onComplete:fulfill];
        NSLog(@"Redirect parsed succefully");
    }];
}

+ (NSURLSessionTask *)request:(NSString *)url params:(NSDictionary *)params onComplete:(AdmanSessionComplete())onComplete {
    NSURLComponents *urlComponent = [NSURLComponents componentsWithString:url];
    NSMutableArray *queryItems = [NSMutableArray arrayWithCapacity:[params count]];
    for (NSString* key in params) {
        [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:[params objectForKey:key]]];
    }
    urlComponent.queryItems = queryItems;

    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 7.0;

    return [[NSURLSession sessionWithConfiguration:sessionConfig] dataTaskWithURL:urlComponent.URL completionHandler:onComplete];
}

@end
