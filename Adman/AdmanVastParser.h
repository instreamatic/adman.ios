/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "AdmanBanner.h"
#import "AdmanConstants.h"

@protocol AdmanVastParserDelegate <NSObject>
@required
- (void)creativeDidParsed:(nonnull NSArray<AdmanBanner *> *)ads;
- (nonnull NSString *)creativeDidParseRedirect:(nonnull NSString *)redirectURL;
- (void)creativeDidFailed:(nonnull NSError *)error;
@end

@interface AdmanVastParser : NSObject

@property (nonnull) id delegate;
@property NSInteger adsLoaded;

- (void)parse:(nonnull NSString *)url params:(nullable NSDictionary *)params;
- (void)startLoadingAdsFromUrl:(nonnull NSString *)url withParams:(nullable NSDictionary *)params
                    onComplete:(nonnull AdmanOnFulfill())fulfill onFail:(nonnull AdmanOnFail())fail;
- (void)cancel;
+ (nonnull NSURLSessionTask *)request:(nonnull NSString *)url params:(nullable NSDictionary *)params onComplete:(nonnull AdmanSessionComplete())onComplete;

@end
