/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AdmanPlayer.h"

static AdmanPreloadMode preloadMode = AdmanPreloadModeProgressive;

@implementation AdmanPlayerItem

- (id)initWithString:(NSString *)url adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType {
    return [self initWithAsset:[AVAsset assetWithURL:[NSURL URLWithString:url]] adID:adID itemType:itemType];
}

- (id)initWithAsset:(AVAsset *)asset adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType {
    self = [super initWithAsset:asset];
    if (self) {
        _adID = adID;
        _itemType = itemType;
        _isPlayed = NO;
        _isLoaded = NO;
    }
    return self;
}

@end

@implementation AdmanPlayer

- (id)init {
    self = [super init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioInterruptionSel:)
                                                 name:AVAudioSessionInterruptionNotification object:nil];
    return self;
}

+ (void)preloadAsset:(NSString *)url onComplete:(AdmanOnFulfill())fulfill onFail:(AdmanOnFail())reject {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^void() {
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:url] options:nil];
        NSArray *keys = @[@"playable", @"tracks",@"duration"];
        [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^() {
            // make sure everything downloaded properly
            for (NSString *thisKey in keys) {
                NSError *error = nil;
                AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
                if (keyStatus == AVKeyValueStatusFailed)
                    dispatch_sync(dispatch_get_main_queue(), ^void() {
                        reject(error);
                    });
                else
                    dispatch_sync(dispatch_get_main_queue(), ^void() { fulfill(asset); });
            }
        }];
    });
}

+ (void)setPreloadMode:(AdmanPreloadMode)mode {
    preloadMode = mode;
}

+ (AdmanPreloadMode)getPreloadMode {
    return preloadMode;
}

- (void)playbackFinished:(NSNotification *)notification {
    if (notification.object != self.currentItem)
        return;

    [self removeStateListeners];

    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self.currentItem name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self.currentItem name:AVPlayerItemPlaybackStalledNotification object:nil];

        if ([self observationInfo])
            [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *exception) {
        //NSLog(@"Failed to remove observer from AddmanPlayer: nothing to do");
    }

    [self itemPlayed];

}

- (void)playbackStailed:(NSNotification*)notification {
    NSLog(@"Playback stailed");
}

- (void)removeAllItems {
    [self removeStateListeners];
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self.currentItem name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self.currentItem name:AVPlayerItemPlaybackStalledNotification object:nil];

        if ([self observationInfo])
            [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *exception) {

    }

    [self replaceCurrentItemWithPlayerItem:nil];
}

- (void)addAsset:(AVAsset *)asset adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType {
    AdmanPlayerItem *playerItem = [[AdmanPlayerItem alloc] initWithAsset:asset adID:adID itemType:itemType];

    [playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    [playerItem addObserver:self forKeyPath:@"playbackBufferFull" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    [playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStailed:)
                                                 name:AVPlayerItemPlaybackStalledNotification object:nil];

    [self replaceCurrentItemWithPlayerItem:playerItem];
    if (itemType != AdmanPlayerItemMicOff && itemType != AdmanPlayerItemMicOn)
        [self.delegate log:[NSString stringWithFormat:@"Buffering %@", [(AVURLAsset *)playerItem.asset URL].absoluteString]];
}

- (void)addUrl:(NSString *)url adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType {
    [self addAsset:[AVAsset assetWithURL:[NSURL URLWithString:url]] adID:(NSInteger)adID itemType:(NSInteger)itemType];
}

- (void)play {
    if ([self currentItem] == nil)
        return;

    NSString *logString = [NSString stringWithFormat:@"Playing asset %@", [(AVURLAsset *)[self currentItem].asset URL].absoluteString];
    [self.delegate log:logString];

    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication.sharedApplication beginReceivingRemoteControlEvents];
    });
    _isPlaying = TRUE;
    [super play];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    if (self.currentItem == nil)
        return;

    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerItemStatus status = AVPlayerItemStatusUnknown;
        NSNumber *statusNumber = [change valueForKey:NSKeyValueChangeNewKey];
        if ([statusNumber isKindOfClass:[NSNumber class]])
            status = statusNumber.integerValue;

        AdmanPlayerItem *item = (AdmanPlayerItem *) self.currentItem;
        switch (status) {
            case AVPlayerItemStatusReadyToPlay:
            case AVPlayerItemStatusUnknown:
                if (item.itemType == AdmanPlayerItemMicOff || item.itemType == AdmanPlayerItemMicOn)
                    [self itemLoaded];
                else
                    [super play];
                break;
            case AVPlayerItemStatusFailed:
                _isPlaying = FALSE;
                [self removeStateListeners];
                [self.delegate itemPlaybackDidFailed:item.adID itemType:item.itemType err:self.currentItem.error];
                NSLog(@"admanPlayer: failed to load audio");
                break;
        }
    } else if ([keyPath isEqualToString:@"playbackBufferFull"] || [keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        AdmanPlayerItem *item = (AdmanPlayerItem *) self.currentItem;

        if (item.itemType == AdmanPlayerItemMicOff || item.itemType == AdmanPlayerItemMicOn)
            return;

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super pause];
        });
        [self itemLoaded];
    }

}

- (void)audioInterruptionSel:(NSNotification *)notification {
    if (self.currentItem == NULL)
        return;

    if (!self.isPlaying)
        return;

    NSNumber *t = [notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey];
    switch ([t integerValue]) {
        case AVAudioSessionInterruptionTypeBegan:
            [super pause];
            break;

        case AVAudioSessionInterruptionTypeEnded:
            if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionOptionKey] intValue] == AVAudioSessionInterruptionOptionShouldResume)
                [super play];
            break;
    }
}

- (void)removeStateListeners {
    _isPlaying = FALSE;
    @try {
        if (self.currentItem && [self.currentItem observationInfo])
            [self.currentItem removeObserver:self forKeyPath:@"status"];
    } @catch (NSException *exception) {
        
    }

    @try {
        [self.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    } @catch (NSException *exception) {
        
    }
    @try {
        [self.currentItem removeObserver:self forKeyPath:@"playbackBufferFull"];
    } @catch (NSException *exception) {
        
    }
}

- (void)itemLoaded {
    AdmanPlayerItem *item = (AdmanPlayerItem *) self.currentItem;
    if (!item.isLoaded) {
        item.isLoaded = YES;
        [self removeStateListeners];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.delegate itemDidLoaded:item.adID itemType:item.itemType];
        });
    }
}

- (void)itemPlayed {
    AdmanPlayerItem *item = (AdmanPlayerItem *) self.currentItem;

    if (!item.isPlayed) {
        item.isPlayed = YES;
        _isPlaying = FALSE;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.delegate itemPlaybackDidFinished:item.adID itemType:item.itemType];
        });
    }
}

@end
