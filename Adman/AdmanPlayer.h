/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@import AVFoundation;

#import "AdmanBanner.h"
#import "AdmanConstants.h"

typedef NS_ENUM(NSUInteger, AdmanPlayerItemType) {
    AdmanPlayerItemAd,
    AdmanPlayerItemUnknown,
    AdmanPlayerItemReaction,
    AdmanPlayerItemMicOn,
    AdmanPlayerItemMicOff,
    AdmanPlayerItemInitial
};

@interface AdmanPlayerItem : AVPlayerItem

@property (readonly) NSInteger adID;
@property AdmanPlayerItemType itemType;

@property bool isLoaded;
@property bool isPlayed;

- (nonnull id)initWithString:(nonnull NSString *)url adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType;
- (nonnull id)initWithAsset:(nonnull AVAsset *)asset adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType;

@end

@protocol AdmanPlayerDelegate<NSObject>
@required
- (void)itemDidLoaded:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType;
- (void)itemPlaybackDidFinished:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType;
- (void)itemPlaybackDidFailed:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType err:(nullable NSError *)err;
- (void)log:(nullable NSString *)data;

@end

@interface AdmanPlayer : AVPlayer<AVAudioPlayerDelegate>

@property (nonnull) id delegate;
@property (nullable) NSObject *userData;
@property (readonly) bool isPlaying;

/**
 Async assets preloading from url (only mp3 links)
 */
+ (void)preloadAsset:(nonnull NSString *)url onComplete:(nonnull AdmanOnFulfill())fulfill onFail:(nonnull AdmanOnFail())reject;

+ (void)setPreloadMode:(AdmanPreloadMode)mode;
+ (AdmanPreloadMode)getPreloadMode;

- (id _Null_unspecified)init;

- (void)addAsset:(nonnull AVAsset *)asset adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType;
- (void)addUrl:(nonnull NSString *)url adID:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType;

- (void)playbackFinished:(nullable NSNotification *)notification;
- (void)playbackStailed:(nullable NSNotification *)notification;
- (void)removeAllItems;
@end
