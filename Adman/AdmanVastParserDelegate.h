/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <AVFoundation/AVFoundation.h>
#import "AdmanBanner.h"

@interface AdmanBannerWrapper : AdmanBanner

typedef NS_ENUM (NSUInteger, AdmanVastParserProp) {
    AdmanVastParserPropResponse,
    AdmanVastParserPropKeyword
};

@property (nullable) NSString *crossServerAdID;
@property NSInteger responseDelay;
@property (nullable) NSString *responseURL;
@property (nonatomic, nullable) NSString *responseLang;

@property (nullable) NSString *redirectUrl;
@property (nonatomic) NSInteger sequence;

@end

@interface AdmanVastParserDelegate : NSObject<NSXMLParserDelegate>

@property (nonnull) NSMutableArray<AdmanBannerWrapper *> *adsList;
@property (nullable) AdmanBannerWrapper *wrappedAd;

+ (nonnull NSString *)getDefaultImageSize;
+ (void)setDefaultImageSize:(nonnull NSString *)size;
+ (void)setDefaultAudioFormat:(nonnull NSString *)format withBitrate:(nonnull NSNumber *)bitrate;

@end
