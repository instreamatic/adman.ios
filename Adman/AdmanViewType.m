/**
* Copyright 2019 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import <Foundation/Foundation.h>
#import "AdmanViewType.h"

//AdmanViewBase
@implementation AdmanViewBase

- (UIView*)getView {
    return nil;
}

@end


//AdmanViewBaseViewController
@interface AdmanViewBaseViewController()

@property (strong, nullable) UIViewController *view;

@end

@implementation AdmanViewBaseViewController

- (id)init:(UIViewController *)view {
    self = [super init];
    if (self) {
        _view = view;
    }
    return self;
}

- (UIViewController*)getView {
    return (UIViewController*)_view;
}

@end

//AdmanViewBaseView
@interface AdmanViewBaseView()

@property (weak, nullable) UIView *view;

@end

@implementation AdmanViewBaseView

- (id)init:(UIView *)view {
    self = [super init];
    if (self) {
        _view = view;
    }
    return self;
}

- (UIView*)getView{
    return (UIView*)_view;
}

@end

//AdmanViewBaseButton
@interface AdmanViewBaseButton()

@property (weak, nullable) UIButton *view;

@end

@implementation AdmanViewBaseButton

- (id)init:(UIButton *)view {
    self = [super init];
    if (self) {
        _view = view;
    }
    return self;
}

- (UIButton*)getView {
    return _view;
}

@end


//AdmanViewBaseLabel
@interface AdmanViewBaseLabel()

@property (weak, nullable) UILabel *view;

@end

@implementation AdmanViewBaseLabel

- (id)init:(UILabel *)view {
    self = [super init];
    if (self) {
        _view = view;
    }
    return self;
}

- (UILabel*)getView {
    return _view;
}

@end

//AdmanViewBaseImageView
@interface AdmanViewBaseImageView()

@property (weak, nullable) UIImageView *view;

@end

@implementation AdmanViewBaseImageView

- (id)init:(UIImageView *)view {
    self = [super init];
    if (self) {
        _view = view;
    }
    return self;
}

- (UIImageView*)getView {
    return _view;
}

@end

//AdmanViewBind
@implementation AdmanViewBind

- (id)init:(AdmanViewElement)type withView:(AdmanViewBase *)view {
    self = [super init];
    if (self) {
        _type = type;
        _view = view;
    }
    return self;
}

@end

//AdmanViewBundle
/*
@interface AdmanViewBundle()

@property (strong) NSMutableDictionary*  byId;

@end

@implementation AdmanViewBundle

- (_Null_unspecified id)init:(NSMutableArray<AdmanViewBind *> * _Nonnull)bindings{
    self = [super init];
    if (self) {
        _byId = [NSMutableDictionary dictionary];
        for (AdmanViewBind *item in bindings) {
            [_byId setObject:item forKey:item.type];
        }
    }
    return self;
}

- (bool) contains:(AdmanViewType) type{
    return ([[_byId allKeys] containsObject:type]);
}

- (nullable AdmanViewBase*) get:(AdmanViewType) type{
    return [_byId objectForKey:  type];
}

@end

*/
