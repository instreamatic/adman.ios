/**
 * Copyright 2021 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>

#import "AdmanConstants.h"
#import "AdmanVastParserDelegate.h"
#import "NSString+HTML.h"

static NSString *defaultImageSize = @"640x640";
static NSString *defaultAudioFormat = @"mp3";
static NSInteger defaultBitrate = 128;

@implementation AdmanMediaFile

- (id)initWithSource:(NSString *)source type:(NSString *)type andBitrate:(NSInteger)bitrate {
    self = [super init];
    if (self) {
        _url = source;
        _type = type;
        _bitrate = bitrate;
    }
    return self;
}

@end

@implementation AdmanBannerWrapper

@end

@interface AdmanVastParserDelegate ()

@property (strong) NSMutableArray<NSDictionary *> *attributes;
@property (strong) NSString *elementName;
@property (strong) NSMutableString *outString;
@property (strong) AdmanInteraction *response;
@property (strong) NSString *staticResourceSize;

@property (nonatomic) BOOL rootTracking;
@property (strong) NSMutableDictionary<NSString *, NSMutableArray<NSString *> *> *companionTracking;

- (int)parseDuration:(NSString *)t;

@end

@implementation AdmanVastParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    self.adsList = [NSMutableArray array];
    self.attributes = [NSMutableArray arrayWithCapacity:5];
    self.rootTracking = YES;

    if (_wrappedAd)
        [_adsList addObject:_wrappedAd];
    if (PARSER_DEBUG_ENABLED)
        NSLog(@"VAST parser started");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    _elementName = qName;
    if (PARSER_DEBUG_ENABLED)
        NSLog(@"Processing element: %@", elementName);
    if (attributeDict != NULL)
        [_attributes addObject:attributeDict];
    else
        [_attributes addObject:@{}];

    SEL handler = NSSelectorFromString([NSString stringWithFormat:@"xml%@Start:", elementName]);
    if ([self respondsToSelector:handler])
        ((void (*)(id, SEL, NSDictionary *))[self methodForSelector:handler])(self, handler, attributeDict);

    _outString = [NSMutableString string];
}

- (void)xmlCompanionStart:(NSDictionary *)attributeDict {
    _rootTracking = NO;
    _companionTracking = [NSMutableDictionary dictionaryWithCapacity:4];
    _staticResourceSize = [NSString stringWithFormat:@"%@x%@", [attributeDict objectForKey:@"height"], [attributeDict objectForKey:@"width"]];
}

- (void)xmlTrackingStart:(NSDictionary *)attributeDict {
    NSString *event = attributeDict[@"event"];

    if (!_rootTracking && [_companionTracking objectForKey:event] == nil)
        [_companionTracking setObject:[NSMutableArray arrayWithCapacity:2] forKey:event];
}

- (void)xmlResponseStart:(NSDictionary *)attributeDict {
    AdmanInteraction *action = [[AdmanInteraction alloc] initWithAction:attributeDict[@"action"] andType:attributeDict[@"type"]];
    action.priority = [[attributeDict objectForKey:@"priority"] integerValue];

    NSMutableArray *response = (NSMutableArray *) [_adsList.lastObject.responses objectForKey:attributeDict[@"action"]];
    if (response != nil)
        [response addObject:action];
    else
        [_adsList.lastObject.responses setValue:[NSMutableArray arrayWithObject:action] forKey:attributeDict[@"action"]];
    _response = [_adsList.lastObject.responses objectForKey:attributeDict[@"action"]].lastObject;
}

- (void)xmlResponsesStart:(NSDictionary *)attributeDict {
    _adsList.lastObject.responses = [NSMutableDictionary dictionaryWithCapacity:6];
}

- (void)xmlValueStart:(NSDictionary *)attributeDict {
    if (attributeDict == NULL)
        return;

    if ([attributeDict[@"type"] isEqualToString:@"tracking_events"])
        return;
    if ([attributeDict[@"type"] isEqualToString:@"media"])
        return;

    [_response.values addObject:[[AdmanResponseValue alloc] initWithType:attributeDict[@"type"] andData:nil]];
}

- (void)xmlAdStart:(NSDictionary *)attributeDict {
    if (_wrappedAd == nil)
        [_adsList addObject:[[AdmanBannerWrapper alloc] init]];

    if ([attributeDict objectForKey:@"id"] != nil)
        _adsList.lastObject.adId = attributeDict[@"id"];
    if (attributeDict[@"sequence"] != nil)
        _adsList.lastObject.sequence = [attributeDict[@"sequence"] intValue];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (_elementName == nil)
        return;
    
    [_outString appendFormat:@"%@", string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    _elementName = nil;
    SEL handler = NSSelectorFromString([NSString stringWithFormat:@"xml%@End:", qName]);
    if ([self respondsToSelector:handler])
        ((void (*)(id, SEL, NSString *))[self methodForSelector:handler])(self, handler, qName);
    if ([_attributes count] > 0)
        [_attributes removeLastObject];
}

- (void)xmlAdSystemEnd:(NSString *)qName {
    _adsList.lastObject.adSystem = _outString;
}

- (void)xmlAdEnd:(NSString *)qName {
    if (_adsList.lastObject.responses != nil) {
        NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:@"priority" ascending:NO];
        [((NSMutableDictionary *)_adsList.lastObject.responses) setObject:[_adsList.lastObject.responses[@"unknown"] sortedArrayUsingDescriptors:@[desc]] forKey:@"unknown"];
    }

    for (AdmanMediaFile *mediaitem in _adsList.lastObject.media) {
        if ([mediaitem.type rangeOfString:defaultAudioFormat].location != NSNotFound && mediaitem.bitrate == defaultBitrate) {
            [_adsList.lastObject setSourceUrl:mediaitem.url];
            [_adsList.lastObject setBitrate:mediaitem.bitrate];
        }
    }
    if (_adsList.lastObject.sourceUrl == nil) {
        AdmanMediaFile *mediaitem = [self getDefaultMediafile:_adsList.lastObject.media];
        [_adsList.lastObject setSourceUrl:mediaitem.url];
        [_adsList.lastObject setBitrate:mediaitem.bitrate];
    }
    
    if ([_adsList.lastObject.companionAds objectForKey:defaultImageSize] != nil) {
        _adsList.lastObject.urlToNavigateOnClick = [_adsList.lastObject.companionAds objectForKey:defaultImageSize].urlToNavigateOnClick;
    }

    if (_adsList.lastObject.responseTime == 0)
        _adsList.lastObject.responseTime = -1;
    if (_adsList.lastObject.responseDelay == 0)
        _adsList.lastObject.responseDelay = -1;
}

- (void)xmlClickTrackingEnd:(NSString *)qName {
    [_adsList.lastObject.statData save:_outString forEvent:AdmanStatEventClickTracking];
}

- (void)xmlCompanionClickThroughEnd:(NSString *)qName {
    [_adsList.lastObject.companionAds[_staticResourceSize] setUrlToNavigateOnClick:_outString];
}

- (void)xmlCompanionClickTrackingEnd:(NSString *)qName {
    if (_companionTracking[@"clickTracking"] == nil)
        [_companionTracking setObject:[NSMutableArray arrayWithCapacity:2] forKey:@"clickTracking"];
    [_companionTracking[@"clickTracking"] addObject:_outString];
}

- (void)xmlClickThroughEnd:(NSString *)qName {
    [_adsList.lastObject setUrlToNavigateOnClick:_outString];
}

- (void)xmlImpressionEnd:(NSString *)qName {
    [_adsList.lastObject.statData save:_outString forEvent:AdmanStatEventImpression];
}

- (void)xmlErrorEnd:(NSString *)qName {
    [_adsList.lastObject.statData save:_outString forEvent:AdmanStatEventError];
}

- (void)xmlTrackingEnd:(NSString *)qName {
    NSString *event = _attributes.lastObject[@"event"];

    if (_rootTracking)
        [_adsList.lastObject.statData save:_outString forEvent:event];
    else
        [_companionTracking[event] addObject:_outString];
}

- (void)xmlDurationEnd:(NSString *)qName {
    [_adsList.lastObject setDuration:[self parseDuration:_outString]];
}

- (void)xmlExtensionEnd:(NSString *)qName {
    NSString *extensionType = [_attributes.lastObject objectForKey:@"type"];
    if (extensionType == nil)
        return;

    if ([extensionType isEqualToString:@"ResponseTime"])
        _adsList.lastObject.responseTime = [_outString intValue];
    else if ([extensionType isEqualToString:@"ResponseMicOnSound"])
        [_adsList.lastObject.assets setObject:_outString forKey:AdmanBannerAssetMicOnUrl];
    else if ([extensionType isEqualToString:@"ResponseMicOffSound"])
        [_adsList.lastObject.assets setObject:_outString forKey:AdmanBannerAssetMicOffUrl];
    else if ([extensionType isEqualToString:@"ResponseDelay"])
        _adsList.lastObject.responseDelay = [_outString intValue];
    else if ([extensionType isEqualToString:@"ResponseLanguage"])
        _adsList.lastObject.responseLang = _outString;
    else if ([extensionType isEqualToString:@"IntroAudio"])
        [_adsList.lastObject setIntroUrl:_outString];
    else if ([extensionType isEqualToString:@"ResponseUrl"])
        [_adsList.lastObject setResponseURL:_outString];
    else if ([extensionType isEqualToString:@"linkTxt"])
        [_adsList.lastObject setAdText:[_outString kv_encodeHTMLCharacterEntities]];
    else if ([extensionType isEqualToString:@"AdId"])
        [_adsList.lastObject setCrossServerAdID:_outString];
    else if ([extensionType isEqualToString:@"Expires"])
        [_adsList.lastObject setExpirationTime:[_outString intValue]];
    else if ([extensionType isEqualToString:@"controls"])
        _adsList.lastObject.showControls = [_outString intValue];
    else if ([extensionType isEqualToString:@"isClickable"])
        _adsList.lastObject.isClickable = [_outString intValue];
}

- (void)xmlCompanionEnd:(NSString *)qName {
    _staticResourceSize = nil;
    _rootTracking = YES;
    _companionTracking = nil;
}

- (void)xmlExpiresEnd:(NSString *)qName {
    [_adsList.lastObject setExpirationTime:[_outString intValue]];
}

- (void)xmlStaticResourceEnd:(NSString *)qName {
    [_adsList.lastObject.companionAds setValue:[[AdmanCreative alloc] init] forKey:_staticResourceSize];
    _adsList.lastObject.companionAds[_staticResourceSize].tracking = _companionTracking;

    _adsList.lastObject.companionAds[_staticResourceSize].staticResource = _outString;
    if ([_staticResourceSize isEqualToString:defaultImageSize])
        [_adsList.lastObject setUrl:_outString];
}

- (void)xmlMediaFileEnd:(NSString *)qName {
    AdmanMediaFile *mediaitem = [[AdmanMediaFile alloc] initWithSource:_outString
                                                        type:_attributes.lastObject[@"type"]
                                                  andBitrate:[self parseBitrate:_attributes.lastObject[@"bitrate"]]];
    if (_response != nil)
        [_response.values addObject:[[AdmanResponseValue alloc] initWithType:@"media" andData:_outString]];
    else
        [((NSMutableArray *)_adsList.lastObject.media) addObject:mediaitem];
}

- (void)xmlVASTAdTagURIEnd:(NSString *)qName {
    _adsList.lastObject.redirectUrl = _outString;
}

- (void)xmlResponseEnd:(NSString *)qName {
    _response = nil;
}

- (void)xmlValueEnd:(NSString *)qName {
    if (_response.values.lastObject.data == nil)
        _response.values.lastObject.data = _outString;
}

- (void)xmlKeywordEnd:(NSString *)qName {
    [_response.keywords addObject:[_outString lowercaseString]];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    _attributes = nil;
    _outString = nil;
    _elementName = nil;
    _response = nil;
    _staticResourceSize = nil;
    if (PARSER_DEBUG_ENABLED)
        NSLog(@"VAST parser stoped");
}

- (NSInteger)parseBitrate:(NSString *)bitrate {
    if (bitrate.length > 3)
        return [[bitrate substringWithRange:NSMakeRange(0, 3)] integerValue];
    else
        return [bitrate integerValue];
}

- (int)parseDuration:(NSString *)t {
    NSArray* digits = [t componentsSeparatedByString: @":"];
    return  [digits[0] intValue] * 3600 + [digits[1] intValue] * 60 + [digits[2] intValue];
}

- (AdmanMediaFile *)getDefaultMediafile:(NSArray<AdmanMediaFile *> *)mediafiles {
    for (AdmanMediaFile *item in mediafiles)
        if ([item.type rangeOfString:@"mp3"].location != NSNotFound || [item.type rangeOfString:@"mpeg"].location != NSNotFound)
            return item;
    return [mediafiles firstObject];
}

+ (NSString *)getDefaultImageSize {
    return defaultImageSize;
}

+ (void)setDefaultImageSize:(NSString *)size {
    defaultImageSize = size;
}

+ (void)setDefaultAudioFormat:(NSString *)format withBitrate:(NSNumber *)bitrate {
    defaultAudioFormat = format;
    defaultBitrate = ([bitrate integerValue] > 0) ? [bitrate integerValue] : 128;
}

@end
