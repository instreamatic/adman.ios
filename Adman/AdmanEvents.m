/**
* Copyright 2019 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import <Foundation/Foundation.h>
#import "AdmanEvents.h"

@implementation NotificationAdmanBase
- (_Null_unspecified id)init:(enum AdmanMessageEventType)event {
    self = [super init];
    if (self) {
        _event = event;
    }
    return self;
}

+ (void)sendEvent:(AdmanMessageEventType)event {
    NotificationAdmanBase *message = [[NotificationAdmanBase alloc] init:event];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:message forKey:NOTIFICATION_ADMAN_MESSAGE_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ADMAN_MESSAGE object:nil userInfo:dict];
}

+ (NSString*)getEventString:(AdmanMessageEventType)event {
    switch (event) {
        case AdmanMessageEventFetchingInfo:
            return [NSString stringWithFormat:@"AdmanMessageEventFetchingInfo (%ld)", (long) event];
        case AdmanMessageEventPreloading:
            return [NSString stringWithFormat:@"AdmanMessageEventPreloading (%ld)", (long) event];
        case AdmanMessageEventNone:
            return [NSString stringWithFormat:@"AdmanMessageEventNone (%ld)", (long) event];
        case AdmanMessageEventError:
            return [NSString stringWithFormat:@"AdmanMessageEventError (%ld)", (long) event];
        case AdmanMessageEventReady:
            return [NSString stringWithFormat:@"AdmanMessageEventReady (%ld)", (long) event];
        case AdmanMessageEventStarted:
            return [NSString stringWithFormat:@"AdmanMessageEventStarted (%ld)", (long) event];
        case AdmanMessageEventStopped:
            return [NSString stringWithFormat:@"AdmanMessageEventStopped (%ld)", (long) event];
        case AdmanMessageEventCompleted:
            return [NSString stringWithFormat:@"AdmanMessageEventCompleted (%ld)", (long) event];
        case AdmanMessageEventSkipped:
            return [NSString stringWithFormat:@"AdmanMessageEventSkipped (%ld)", (long) event];
        default:
            return @"Unknown";
    }
}
@end

@implementation NotificationAdmanPlayer

- (id)initWith:(AdmanPlayerEventType)event andUserData:(id)userData {
    self = [super init];
    if (self) {
        _event = event;
        _userData = userData;
    }
    return self;
}

+ (void)sendEvent:(AdmanPlayerEventType)event {
    [NotificationAdmanPlayer sendEvent:event userData:nil];
}

+ (void)sendEvent:(AdmanPlayerEventType)event userData:(id)userData {
    NotificationAdmanPlayer *message = [[NotificationAdmanPlayer alloc] initWith:event andUserData:userData];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:message forKey:NOTIFICATION_ADMAN_PLAYER_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ADMAN_PLAYER object:nil userInfo:dict];
}

+ (NSString*)getEventString:(AdmanPlayerEventType)event {
    switch (event) {
        case AdmanPlayerEventPrepare:
            return [NSString stringWithFormat:@"AdmanPlayerEventPrepare (%ld)", (long) event];
        case AdmanPlayerEventReady:
            return [NSString stringWithFormat:@"AdmanPlayerEventReady (%ld)", (long) event];
        case AdmanPlayerEventFailed:
            return [NSString stringWithFormat:@"AdmanPlayerEventFailed (%ld)", (long) event];
        case AdmanPlayerEventPlaying:
            return [NSString stringWithFormat:@"AdmanPlayerEventPlaying (%ld)", (long) event];
        case AdmanPlayerEventPlay:
            return [NSString stringWithFormat:@"AdmanPlayerEventPlay (%ld)", (long) event];
        case AdmanPlayerEventPause:
            return [NSString stringWithFormat:@"AdmanPlayerEventPause (%ld)", (long) event];
        case AdmanPlayerEventProgress:
            return [NSString stringWithFormat:@"AdmanPlayerEventProgress (%ld)", (long) event];
        case AdmanPlayerEventComplete:
            return [NSString stringWithFormat:@"AdmanPlayerEventComplete (%ld)", (long) event];
        case AdmanPlayerEventCloseable:
            return [NSString stringWithFormat:@"AdmanPlayerEventCloseable (%ld)", (long) event];
        case AdmanPlayerEventSkippable:
            return [NSString stringWithFormat:@"AdmanPlayerEventSkippable (%ld)", (long) event];
        default:
            return @"Unknown";
    }
}

@end

@implementation NotificationAdmanVoice

- (_Null_unspecified id)init:(enum AdmanVoiceEventType)event {
    self = [super init];
    if (self) {
        _event = event;
    }
    return self;
}

+ (void)sendEvent:(AdmanVoiceEventType)event {
    NotificationAdmanVoice *message = [[NotificationAdmanVoice alloc] init:event];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:message forKey:NOTIFICATION_ADMAN_VOICE_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ADMAN_VOICE object:nil userInfo:dict];
}

+ (NSString*)getEventString:(AdmanVoiceEventType)event {
    switch (event) {
        case AdmanVoiceEventResponsePlaying:
            return [NSString stringWithFormat:@"AdmanVoiceEventResponsePlaying (%ld)", (long) event];
        case AdmanVoiceEventInteractionStarted:
            return [NSString stringWithFormat:@"AdmanVoiceEventInteractionStarted (%ld)", (long) event];
        case AdmanVoiceEventInteractionEnded:
            return [NSString stringWithFormat:@"AdmanVoiceEventInteractionEnded (%ld)", (long) event];
        case AdmanVoiceEventError:
            return [NSString stringWithFormat:@"AdmanVoiceEventError (%ld)", (long) event];
        case AdmanVoiceEventRecognizerStarted:
            return [NSString stringWithFormat:@"AdmanVoiceEventRecognizerStarted (%ld)", (long) event];
        case AdmanVoiceEventRecognizerStopped:
            return [NSString stringWithFormat:@"AdmanVoiceEventRecognizerStopped (%ld)", (long) event];
        default:
            return @"Unknown";
    }
}

@end

@implementation NotificationAdmanControl

- (_Null_unspecified id)initWith:(enum AdmanControlEventType)event andUserData:(id)userData {
    self = [super init];
    if (self) {
        _event = event;
        _userData = userData;
    }
    return self;
}

+ (void)sendEvent:(AdmanControlEventType)event {
    [NotificationAdmanControl sendEvent:event userData:nil];
}

+ (void)sendEvent:(AdmanControlEventType)event userData:(id)userData {
    NotificationAdmanControl *message = [[NotificationAdmanControl alloc] initWith:event andUserData:userData];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:message forKey:NOTIFICATION_ADMAN_CONTROL_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ADMAN_CONTROL object:nil userInfo:dict];
}

+ (NSString*)getEventString:(AdmanControlEventType)event {
    switch (event) {
        case AdmanControlEventPrepare:
            return [NSString stringWithFormat:@"AdmanControlEventPrepare (%ld)", (long) event];
        case AdmanControlEventStart:
            return [NSString stringWithFormat:@"AdmanControlEventStart (%ld)", (long) event];
        case AdmanControlEventPause:
            return [NSString stringWithFormat:@"AdmanControlEventPause (%ld)", (long) event];
        case AdmanControlEventResume:
            return [NSString stringWithFormat:@"AdmanControlEventResume (%ld)", (long) event];
        case AdmanControlEventSkip:
            return [NSString stringWithFormat:@"AdmanControlEventSkip (%ld)", (long) event];
        case AdmanControlEventClick:
            return [NSString stringWithFormat:@"AdmanControlEventClick (%ld)", (long) event];
        case AdmanControlEventLink:
            return [NSString stringWithFormat:@"AdmanControlEventLink (%ld)", (long) event];
        case AdmanControlEventSwipeBack:
            return [NSString stringWithFormat:@"AdmanControlEventSwipeBack (%ld)", (long) event];
        case AdmanControlEventPositive:
            return [NSString stringWithFormat:@"AdmanControlEventPositive (%ld)", (long) event];
        case AdmanControlEventNegative:
            return [NSString stringWithFormat:@"AdmanControlEventNegative (%ld)", (long) event];
        default:
            return @"Unknown";
    }
}

@end
