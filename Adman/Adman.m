/**
 * Copyright 2021 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define TAG @"Adman"

#define AdmanCurrentAd (AdmanBannerWrapper *)[self.adsList objectAtIndex:self.activeAd]
#define AdmanGetAd(Index) (AdmanBanner *)[self.adsList objectAtIndex:Index]

#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <EventKit/EventKit.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#import "Adman.h"
#import "AdmanBanner.h"
#import "AdmanConstants.h"
#import "AdmanProperties.h"
#import "AdmanEvents.h"

#import "AdmanVastParser.h"
#import "AdmanVastParserDelegate.h"
#import "AdmanWebsocketDtmfListener.h"
#import "AdmanPlayer.h"
#import "AdmanUIBase.h"
#import "NetworkStatus.h"

#import "NSString+HTML.h"
#import "VACRecognizer.h"

static NSString* const InstreamaticServer = @"instreamatic.com";

@interface Adman ()<VACRecognizerDelegate, AdmanDtmfDelegate, AdmanPlayerDelegate, AdmanVastParserDelegate>

@property AdmanStats *statistic;
@property AdmanSettings *properties;
@property bool intentTapped;

@property NSTimer *vastLiveDurationTimer;
/**
 Player which is used for audio ads playback
 */
@property AdmanPlayer *player;
/**
 Timer to refresh playback time. 1 tick = 1 second
 */
@property (nonatomic, strong) NSTimer *playbackProgressTimer;
/**
 Timer for opening the socket connection to voice recognition server and for enabling the microphone before the main track stops
 */
@property (nonatomic, strong) NSTimer *voiceResponseDelayTimer;
/**
 Timer for voice recognition. After 5 seconds the ‘audio.end’ is to be sent to server
 */
@property (nonatomic, strong) NSTimer *interactionTimeoutTimer;
/**
 Timer that used to send the ‘audio.end’ after the pre-defined time period
 */
@property (nonatomic, strong) NSTimer *endSpeechDetectionTimer;

@property (nonatomic, strong) NetworkStatus *networkStatus;
@property (strong) AdmanVastParser *vastLoader;
@property (strong) AdmanWebsocketDtmfListener *dtmfListener;

@property VACRecognizer *recognizer;

@property NSMutableDictionary *cache;
/**
 The amount of ‘unknown’ target actions. (Received from VAST)
 */
@property NSInteger unknownPhraseIteration;

@property bool allowPreload;

@property bool silentPreload;

/**
 Ad request. As for now, on play event `requestSection:success:failure` should be called for `sPreroll`. As a responce you will get the ad information - AdmanBanner object
 @param section requsted ad type: pre-roll, mid-roll, post-roll.
*/
- (void)requestSection:(AdmanSectionType)section;
/**
 Build and return full URL for Instreamatic voice recognition server
 */
+ (NSString *)getVoiceServer:(AdmanRegion)region;
+ (NSString *)getVoiceServer:(AdmanRegion)region location:(AdmanRecognitionBackendLocation)location;

- (bool)hasNextAd;
- (void)endVoiceSession:(NSDictionary *)result;
- (void)errorRecived:(NSError *)err;
/**
 Check if application is in background
 */
@end

@implementation Adman

static Adman *sharedManager = nil;
bool firstQuarterReported;
bool secondQuarterReported;
bool thirdQuarterReported;

#pragma mark Initialization variants

+ (instancetype)sharedManagerWithSiteId:(NSInteger)siteId {
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedManager = [[Adman alloc] initWithSiteId:siteId];
    });

    return sharedManager;
}

+ (instancetype)sharedManagerWithSiteId:(NSInteger)siteId testMode:(BOOL)testMode {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[Adman alloc] initWithSiteId:siteId testMode:testMode];
    });
    
    return sharedManager;
}

+ (instancetype)sharedManagerWithSiteId:(NSInteger)siteId playerId:(NSInteger)playerId testMode:(BOOL)testMode {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[Adman alloc] initWithSiteId:siteId testMode:testMode withZoneId:0 region:AdmanRegionEU playerId:playerId];
    });

    return sharedManager;
}

+ (instancetype)sharedManager {
    if (sharedManager == nil)
        [NSException raise:NSInvalidArgumentException
                    format:@"Use sharedManagerWithSiteId:(NSInteger)siteId testMode:(BOOL) testMode first!"];
    return sharedManager;
}

+ (instancetype)sharedManagerWithSiteId:(NSInteger)siteId withZoneId:(NSInteger)zoneId testMode:(BOOL)testMode {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[Adman alloc] initWithSiteId:siteId testMode:testMode withZoneId:zoneId region:AdmanRegionEU playerId:0];
    });
    
    return sharedManager;
}

+ (instancetype)sharedManagerWithSiteId:(NSInteger)siteId region:(AdmanRegion)region playerId:(NSUInteger)playerId testMode:(BOOL)testMode {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[Adman alloc] initWithSiteId:siteId testMode:testMode withZoneId:0 region:region playerId:playerId];
    });
    
    return sharedManager;
}

+ (instancetype)sharedManagerWithSiteId:(NSInteger)siteId region:(AdmanRegion)region testMode:(BOOL)testMode {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[Adman alloc] initWithSiteId:siteId testMode:testMode withZoneId:0 region:region playerId:0];
    });
    
    return sharedManager;
}

- (id)initWithSiteId:(NSInteger)siteId {
    return [self initWithSiteId:siteId testMode:false withZoneId:0 region:AdmanRegionEU playerId:0];
}

- (id)initWithSiteId:(NSInteger)siteId testMode:(BOOL)testMode {
    return [self initWithSiteId:siteId testMode:testMode withZoneId:0 region:AdmanRegionEU playerId:0];
}

- (id)initWithURL:(NSString *)url {
    self = [self initWithSiteId:0];
    _properties.requestURL = url;
    return self;
}

- (id)initWithSiteId:(NSInteger)siteId testMode:(BOOL)testMode withZoneId:(NSInteger)zoneId region:(AdmanRegion)region playerId:(NSInteger)playerId {
    self = [super init];
    if (self) {
        _playerId = playerId;
        _properties = [[AdmanSettings alloc] init];
        _properties.region = (!region) ? AdmanRegionEU : region;
        _properties.siteId = siteId;
        _properties.zoneId = zoneId;

        _recognizer = [[VACRecognizer alloc] init];
        _recognizer.delegate = self;

        if (testMode)
            _properties.debugMode = [NSNumber numberWithInt:AdmanDebugAudio];

        _statistic = [[AdmanStats alloc] init];
        [_statistic setRegion:_properties.region];
        [_statistic saveFor:AdmanStatEventLoad point:@"load" admanId:3171
                      siteId:_properties.siteId playerId:0 campaignId:0 bannerId:0 deviceId:_properties.idfa];
        [_statistic saveFor:AdmanStatEventRequest point:@"req" admanId:3171
                      siteId:_properties.siteId playerId:0 campaignId:0 bannerId:0 deviceId:_properties.idfa];
        [_statistic saveFor:AdmanStatEventFetched point:@"fetched" admanId:3171
                      siteId:_properties.siteId playerId:0 campaignId:0 bannerId:0 deviceId:_properties.idfa];

        if (_properties.region != AdmanRegionVoice)
            [_statistic reportEvent:AdmanStatEventLoad];

        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(triggerActionAdmanControl:) name:NOTIFICATION_ADMAN_CONTROL object:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(triggerActionAdmanPlayer:) name:NOTIFICATION_ADMAN_PLAYER object:nil];

        _player = [[AdmanPlayer alloc] init];
        _player.delegate = self;
        _allowPreload = TRUE;
        [self setState:AdmanStateInitial];

        __weak Adman* weakSelf = self;
        _networkStatus = [NetworkStatus reachabilityForInternetConnection];
        _networkStatus.reachableBlock = ^(NetworkStatus *status) {
            [weakSelf networkStateDidChanged:[status currentReachabilityStatus] != NotReachable];
        };
        _networkStatus.unreachableBlock = ^(NetworkStatus *status) {
            [weakSelf networkStateDidChanged:[status currentReachabilityStatus] != NotReachable];
        };
        [_networkStatus startNotifier];
    }
    return self;
}

#pragma mark Set functions

- (void)setCustomRegionServer:(NSString *)adServer {
    _properties.region = AdmanRegionCustom;
    _properties.customAdServer = adServer;
}

- (void)setDefaultAudioFormat:(NSString *)format withBitrate:(NSNumber *)bitrate {
    [AdmanVastParserDelegate setDefaultAudioFormat:format withBitrate:bitrate];
}

- (void)setDefaultBannerSize:(NSString *)size {
    [AdmanVastParserDelegate setDefaultImageSize:size];
}

- (void)setMute:(BOOL)mute {
    _mute = mute;
    if (_player) {
        if (mute) {
            [_player setVolume:0.0];
        } else {
            [_player setVolume:self.volume];
        }
    }
}

+ (void)setPreloadMode:(AdmanPreloadMode)mode {
    [AdmanPlayer setPreloadMode:mode];
}

- (void)setRecognitionBackend:(AdmanRecognitionBackend)backend {
    _properties.backend = backend;
}

- (void)setResponseDelay:(float)delay{
    _properties.voiceResponseDelay = delay;
}

- (void)setServerVoiceActivityDetection:(bool)state {
    _properties.serverVoiceActivityDetection = state;
}

- (void)setState:(AdmanState)newState {
    if (newState == _state)
        return;
   
    if (newState != AdmanStateError)
        _fallbackState = newState;
    _state = newState;

    [[self delegate] admanStateDidChange:self];
}

- (void)setVolume:(float)volume {
    _volume = volume;
    if (_mute && volume > 0.0)
        _mute = false;
    if (_player)
        [_player setVolume:volume];
}

- (void)overrideIP:(NSString *)ip {
    _properties.clientIP = ip;
}

#pragma mark Get functions

- (AdmanRegion)currentRegion {
    return _properties.region;
}

- (AdmanBanner *)getActiveAd {
    return (AdmanCurrentAd);
}

- (long)elapsedTime {
    if (_player.currentItem == nil)
        return 0;

    long i = lroundf(CMTimeGetSeconds(_player.currentItem.currentTime));
    if (i < 0)
        return 0;
    if (i > 65)
        return 0;
    return i;
}

- (long)estimatedTime {
    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
    if (ad == nil)
        return 0;

    if (_player.currentItem == nil)
        return ad.duration;

    long i = lroundf(CMTimeGetSeconds(_player.currentItem.duration) - CMTimeGetSeconds(_player.currentItem.currentTime));
    if (i < 0)
        return ad.duration;
    if (i > 65)
        return ad.duration;
    return i;
}

- (NSString*)elapsedTimeString {
    return [self formatTime:self.elapsedTime];
}

- (NSString*)estimatedTimeString {
    return [self formatTime:self.estimatedTime];
}

- (NSString*)formatTime:(long) seconds {
    int mins = (seconds % 3600) / 60;
    int secs = seconds % 60;
    return [NSString stringWithFormat:@"%02d:%02d", mins, secs];
}

- (NSString *)getDefaultBannerSize {
    return [AdmanVastParserDelegate getDefaultImageSize];
}

+ (NSString *)getRecognitionBackend:(AdmanRecognitionBackend)backend {
    switch (backend) {
        case AdmanRecognitionBackendNuanceDev:
            return @"/nuance";
        case AdmanRecognitionBackendNuanceRelease:
            return @"/nuance/prod";
        case AdmanRecognitionBackendHoundify:
            return @"/houndify";
        case AdmanRecognitionBackendYandex:
            return @"/yandex";
        case AdmanRecognitionBackendMicrosoft:
            return @"/microsoft";
        case AdmanRecognitionBackendAuto:
        default:
            return @"";
    }
}

- (float)getResponseDelay{
    return _properties.voiceResponseDelay;
}

- (NSString *)getServer:(AdmanRegion)region {
    switch (region) {
    case AdmanRegionGlobal:
        return [NSString stringWithFormat:@"x3.%@", InstreamaticServer];
    case AdmanRegionVoice:
        return [NSString stringWithFormat:@"voice.%@", InstreamaticServer];
    case AdmanRegionDemo:
        return [NSString stringWithFormat:@"d1.%@", InstreamaticServer];
    case AdmanRegionDemoEu:
        return [NSString stringWithFormat:@"test.%@", InstreamaticServer];
    case AdmanRegionIndia:
        return [NSString stringWithFormat:@"x1-india.%@", InstreamaticServer];
    case AdmanRegionUS:
        return @"d2975e0yphvxch.cloudfront.net";
    case AdmanRegionCustom:
        return _properties.customAdServer;
    default:
        return [NSString stringWithFormat:@"x.%@", InstreamaticServer];
    }
}

- (NSString *)getSpeechRecognizerUrl:(NSString *)voiceServer {
    if ([voiceServer rangeOfString:@"adman/"].location != NSNotFound)
        return voiceServer;

    AdmanBannerWrapper *ad = nil;
    NSString *url = [NSString stringWithFormat:@"%@/adman%@/v2", voiceServer,
                     [Adman getRecognitionBackend:_properties.backend]];
    if (ad.responseURL)
        url = ad.responseURL;

    if (_adsList.count > 0) {
        NSString *crossServerAdID = @"";
        ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
        if (ad.crossServerAdID != nil)
            crossServerAdID = [NSString stringWithFormat:@"&ad_id=%@", ad.crossServerAdID];
        url = [NSString stringWithFormat:@"%@?language=%@%@", url, ad.responseLang, crossServerAdID];
    }
    return url;
}

+ (NSString *)getVersion {
    return AdmanVersion;
}

+ (NSString *)getVoiceServer:(AdmanRegion)region {
    switch (region) {
        // USA
        case AdmanRegionGlobal:
        case AdmanRegionVoice:
            return @"wss://v3.instreamatic.com";
        // Test voice server
        case AdmanRegionDemo:
            return @"wss://voice.instreamatic.com";
        // EU
        default:
            return @"wss://v2.instreamatic.com";
    }
}

+ (NSString *)getVoiceServer:(AdmanRegion)region location:(AdmanRecognitionBackendLocation)location {
    switch (location) {
        case AdmanRecognitionBackendLocationEU:
            return [Adman getVoiceServer:AdmanRegionEU];
        case AdmanRecognitionBackendLocationUSA:
            return [Adman getVoiceServer:AdmanRegionGlobal];
        case AdmanRecognitionBackendLocationDemo:
            return [Adman getVoiceServer:AdmanRegionDemo];
        default:
            return [Adman getVoiceServer:region];
    }
}

- (NSDictionary *)getWebsocketHeaderOptions {
    return @{@"idfa": _properties.idfa,
             @"site_id": [NSNumber numberWithInteger:_properties.siteId],
             @"vad": [NSNumber numberWithBool:_properties.serverVoiceActivityDetection],
             @"ad_id": (AdmanCurrentAd).adId,
             @"response_delay": [NSString stringWithFormat:@"%.4g", [self getResponseDelay]],
             @"device_info": [NSString stringWithFormat:@"Adman voice sdk %@; %@ %ld", AdmanVersion, [[UIDevice currentDevice] model],
                              (long)[[NSProcessInfo processInfo] operatingSystemVersion].majorVersion]};
}

- (void)isInBackground:(bool)synced then:(AdmanOnFulfill())cb {
    if (synced) {
        if ([NSThread isMainThread]) {
            bool state = [UIApplication sharedApplication].applicationState == UIApplicationStateBackground;
            cb([NSNumber numberWithBool:state]);
        } else
            dispatch_sync(dispatch_get_main_queue(), ^void() {
                bool state = [UIApplication sharedApplication].applicationState == UIApplicationStateBackground;
                cb([NSNumber numberWithBool:state]);
            });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^void() {
            bool state = [UIApplication sharedApplication].applicationState == UIApplicationStateBackground;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^void() {
                cb([NSNumber numberWithBool:state]);
            });
        });
    }
}

- (bool)isMicrophoneEnabled {
    switch ([AVAudioSession sharedInstance].recordPermission) {
        case AVAudioSessionRecordPermissionDenied:
        case AVAudioSessionRecordPermissionUndetermined:
            return false;
        default:
            return true;
    }
}

- (bool)isValidToPlayback {
    [_vastLiveDurationTimer invalidate];
    _vastLiveDurationTimer = NULL;

    long int expires = AD_EXPIRATION_TIME_DEFAULT;
    if ([self.adsList count] > 0 && (AdmanCurrentAd).expirationTime > 0)
        expires = (AdmanCurrentAd).expirationTime;
    if (_properties.lastFetchTime == nil)
        return false;
    if ([[NSDate date] timeIntervalSinceDate:_properties.lastFetchTime] > expires) {
        self.silentPreload = TRUE;
        [self prepare];
        NSLog(@"adman: VAST expired, revalidation started");
        return false;
    }
    return true;
}

- (bool)hasNextAd {
    if (_adsList == nil)
        return false;
    return ([_adsList count] - 1) > (self.activeAd);
}

- (NSInteger)siteId {
    return _properties.siteId;
}

#pragma mark Preloading

- (void)cancelLoading {
    if (_vastLoader)
        [_vastLoader cancel];
    _vastLoader = nil;

    [self resetRecognizerState];
    [self setState:AdmanStateAdNone];
    [NotificationAdmanBase sendEvent:AdmanMessageEventNone];
}

- (void)preloadIntro {
    // Not used anymore
}

- (void)preloadAd:(NSUInteger)index onComplete:(AdmanOnFulfill())fulfill onFail:(AdmanOnFail())reject {
    [self log:[NSString stringWithFormat:@"Fetching %li ad from queue. Banner: %@",
               (long) index, (AdmanGetAd(index)).url]];
    if ((AdmanGetAd(index)).url != nil) {
        NSURL   *imageURL   = [NSURL URLWithString:(AdmanGetAd(index)).url];
        (AdmanGetAd(index)).adCache  = [NSData dataWithContentsOfURL:imageURL];
        UIImage *img = [UIImage imageWithData:(AdmanGetAd(index)).adCache];
        // preload image for fullscreen ad
        [self preloadImage:img];
        (AdmanGetAd(index)).bannerImage = img;
        [self log:@"Banner preloaded succefully"];
    }
    
    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanGetAd(index));
    if ([ad.assets objectForKey:AdmanBannerAssetMicOnUrl] != nil) {
        __block bool micOnLoaded = false, micOffLoaded = false;
        [AdmanPlayer preloadAsset:ad.assets[AdmanBannerAssetMicOnUrl] onComplete:^void(id asset) {
            micOnLoaded = true;
            [ad.assets setObject:asset forKey:AdmanBannerAssetMicOnCache];
            if (micOffLoaded && micOnLoaded)
                fulfill(ad);
        } onFail:^void(NSError *err) {
            micOnLoaded = true;
            if (micOffLoaded && micOnLoaded)
                fulfill((AdmanGetAd(index)));
        }];
        [AdmanPlayer preloadAsset:ad.assets[AdmanBannerAssetMicOffUrl] onComplete:^void(id asset) {
            micOffLoaded = true;
            [ad.assets setObject:asset forKey:AdmanBannerAssetMicOffCache];
            if (micOffLoaded && micOnLoaded)
                fulfill(ad);
        } onFail:^void(NSError *err) {
            micOffLoaded = true;
            if (micOffLoaded && micOnLoaded)
                fulfill((AdmanGetAd(index)));
        }];
    } else {
        fulfill(ad);
    }
}

- (NSDictionary *)buildAdmanRequest:(AdmanSettings *)params cache:(bool)cache {
    NSString *path;
    NSInteger supportedVASTVersion = VAST_VERSION_SUPPORT;
    if (_properties.region == AdmanRegionUS)
        supportedVASTVersion = 2;

    if (_properties.region != AdmanRegionVoice && _properties.region != AdmanRegionDemo && _properties.region != AdmanRegionDemoEu)
        path = [NSString stringWithFormat:@"https://%@/v%li/vast", [self getServer:_properties.region], (long) supportedVASTVersion];
    else
        path = [NSString stringWithFormat:@"https://%@/x/v%li/vast", [self getServer:_properties.region], (long) supportedVASTVersion];

    if (_properties.siteId)
        path = [path stringByAppendingString:[NSString stringWithFormat:@"/%ld", (long)_properties.siteId]];
    if (_playerId)
        path = [path stringByAppendingString:[NSString stringWithFormat:@"/%ld", (long)_playerId]];
    path = [path stringByAppendingString:@".xml"];
    if (cache)
        [_cache setValue:path forKey:@"requestURL"];

    NSMutableDictionary *requestParams = [NSMutableDictionary dictionary];
    [requestParams setObject:_properties.idfa forKey:@"idfa"];
    [requestParams setObject:(_properties.isTrackingEnabled ? @"1" : @"0") forKey:@"advertising_tracking_enabled"];
    [requestParams setObject:AdmanVersion forKey:@"version"];

    NSString *slot;
    switch (_properties.slot) {
        case AdmanPreroll:
            slot = @"preroll";
            break;
        case AdmanMidroll:
            slot = @"midroll";
            break;
        case AdmanPostroll:
            slot = @"postroll";
            break;
        default:
            break;
    }
    if (slot)
        [requestParams setObject:slot forKey:@"slot"];

    if (params.type == AdmanTypeVoice || params.type == AdmanTypeAny || params.type == AdmanTypeVoiceMultipleAds) {
        [requestParams setObject:[NSNumber numberWithInteger:[self isMicrophoneEnabled]].stringValue forKey:@"microphone"];
        if ([EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent] == EKAuthorizationStatusAuthorized)
            [requestParams setObject:@"1" forKey:@"calendar"];
    }

    if (_properties.debugMode && _properties.debugMode.integerValue)
        [requestParams setObject:_properties.debugMode forKey:@"preview"];
    if (_properties.clientIP)
        [requestParams setObject:_properties.clientIP forKey:@"ip"];

    if (_properties.campaignId)
        [requestParams setObject:[NSString stringWithFormat:@"%ld", (long)_properties.campaignId] forKey:@"campaign_id"];
    if (_properties.creativeId)
        [requestParams setObject:[NSString stringWithFormat:@"%ld", (long)_properties.creativeId] forKey:@"creative_id"];
    if (_properties.zoneId)
        [requestParams setObject:[NSString stringWithFormat:@"%ld", (long)_properties.zoneId] forKey:@"zone_id"];
    if (_userId)
        [requestParams setObject:_userId forKey:@"user_id"];
    if (_properties.format)
        [requestParams setObject:[NSString stringWithFormat:@"%ld", (long)_properties.format] forKey:@"format"];
    if (_properties.maxAdsCount)
        [requestParams setObject:[NSString stringWithFormat:@"%ld", (long)_properties.maxAdsCount] forKey:@"ads_count"];
    if (_properties.maxAdBlockDuration)
        [requestParams setObject:[NSString stringWithFormat:@"%ld", (long)_properties.maxAdBlockDuration] forKey:@"duration"];

    if (_properties.useGeolocationTargeting) {
        [requestParams setObject:[NSNumber numberWithFloat:_properties.latitude].stringValue forKey:@"lat"];
        [requestParams setObject:[NSNumber numberWithFloat:_properties.longitude].stringValue forKey:@"long"];
    }

    for (NSString *key in _properties.siteVariables)
        [requestParams setObject:[_properties.siteVariables valueForKey:key] forKey:key];

    [requestParams setObject:[NSString stringWithFormat:@"%@", [[NSLocale preferredLanguages] firstObject]] forKey:@"lang"];

    if (_properties.type) {
        [self isInBackground:YES then:^(id state) {
            bool appIsInBG = [state intValue] == 1;

            switch (self.properties.type) {
                case AdmanTypeAny:
                    if (@available(iOS 12.0, *)) {
                        if (appIsInBG)
                            [requestParams setObject:@"digital" forKey:@"type"];
                        else
                            [requestParams setObject:@"any" forKey:@"type"];
                    } else
                        [requestParams setObject:@"any" forKey:@"type"];
                    break;
                case AdmanTypeAudioOnly:
                    [requestParams setObject:@"radio" forKey:@"type"];
                    break;
                case AdmanTypeAudioPlus:
                    [requestParams setObject:@"digital" forKey:@"type"];
                    break;
                case AdmanTypeVoice:
                    if (@available(iOS 12.0, *)) {
                        if (appIsInBG)
                            [requestParams setObject:@"digital" forKey:@"type"];
                        else
                            [requestParams setObject:@"voice" forKey:@"type"];
                    } else
                        [requestParams setObject:@"voice" forKey:@"type"];
                    break;
                default:
                    break;
            }
        }];
    }

    // reset voice activity status
    self.properties.clientVoiceActivityEnded = NO;
    if (cache)
        [_cache setValue:requestParams forKey:@"requestParams"];

    // check network type
    NSArray *networks_2g = @[CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x];
    NSArray *networks_4g = @[CTRadioAccessTechnologyLTE];
    if ([_networkStatus currentReachabilityStatus] == ReachableViaWiFi)
        [requestParams setObject:@"wifi" forKey:@"network_type"];
    else {
        CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
        NSString *carrierType = nil;
        NSDictionary *carriers = info.serviceCurrentRadioAccessTechnology;
        NSString *carrierName = carriers.allKeys.firstObject;
        carrierType = carriers[carrierName];

        if ([networks_2g containsObject:carrierType])
            [requestParams setObject:@"2g" forKey:@"network_type"];
        else if ([networks_4g containsObject:carrierType])
            [requestParams setObject:@"4g" forKey:@"network_type"];
        else
            [requestParams setObject:@"3g" forKey:@"network_type"];
    }

    AVAudioSessionRouteDescription *currentRoute = [[AVAudioSession sharedInstance] currentRoute];
    AVAudioSessionPortDescription *output = currentRoute.outputs.firstObject;
    if ([output.portType isEqualToString:AVAudioSessionPortBluetoothA2DP])
        [requestParams setObject:@"bluetootha2dp" forKey:@"audio_output"];
    else if ([output.portType isEqualToString:AVAudioSessionPortBluetoothLE])
        [requestParams setObject:@"bluetootha2dp" forKey:@"audio_output"];
    else if ([output.portType isEqualToString:AVAudioSessionPortHeadphones])
        [requestParams setObject:@"headphones" forKey:@"audio_output"];
    else if ([output.portType isEqualToString:AVAudioSessionPortBuiltInReceiver])
        [requestParams setObject:@"speakerphone" forKey:@"audio_output"];
    else
        [requestParams setObject:@"speakerphone" forKey:@"audio_output"];

    return @{@"requestURL": path, @"requestParams": requestParams};
}

- (void)requestSection:(AdmanSectionType)section {
    _error = nil;
    _cache = [NSMutableDictionary dictionaryWithCapacity:2];
    if (!_allowPreload) {
        [self setState:AdmanStateAdNone];
        NSLog(@"Ad requests disabled for this Adman instance");
        return;
    }

    [self buildAdmanRequest:_properties cache:YES];

    if (_properties.region != AdmanRegionVoice)
        [_statistic reportEvent:AdmanStatEventRequest];

    NSLog(@"Adman version: %@", AdmanVersion);

    _vastLoader = [[AdmanVastParser alloc] init];
    _vastLoader.delegate = self;
    if (_properties.requestURL != NULL) {
        [self log:[NSString stringWithFormat:@"Start fetching url: %@", _properties.requestURL]];
        [_vastLoader parse:_properties.requestURL params:@{}];
    } else {
        [self log:[NSString stringWithFormat:@"Start fetching url: %@ with params: %@", [_cache valueForKey:@"requestURL"], [_cache valueForKey:@"requestParams"]]];
        [_vastLoader parse:[_cache valueForKey:@"requestURL"] params:[_cache valueForKey:@"requestParams"]];
    }
}

- (void)requestCommonBanner:(AdmanBannerType)type success:(void (^)(AdmanBanner *banner))success failure:(void (^)(NSError *error))failure {
    NSInteger slot = (type == t320x480) ? 3146 :
                     (type == t320x50)  ? 3175 : 3146;
    NSString *path = [NSString stringWithFormat:@"https://ad.mail.ru/vp/%ld?_SITEID=%ld&idfa=%@&advertising_tracking_enabled=%@", (long)slot, (long)_properties.siteId, _properties.idfa, _properties.isTrackingEnabled ? @"1" : @"0"];

    NSLog(@"AdmanStateFetchingInfo from: %@", path);
    NSLog(@"Fetching %@", path);

    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 7.0;

    NSURLSessionTask *task = [[NSURLSession sessionWithConfiguration:sessionConfig] dataTaskWithURL:[NSURL URLWithString:path] completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
        if (err != nil) {
            NSLog(@"error: %@", [err localizedDescription]);
            failure(err);
            return;
        }
        
        if ([data length] == 0) {
            [self setState:AdmanStateAdNone];
            return;
        }

        NSError *serializationError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&serializationError];
        if (serializationError != nil) {
            failure(serializationError);
            return;
        }
        NSArray *banners = [json valueForKeyPath:@"sections.preroll"];
        id a = [banners firstObject];

        AdmanBanner *ab = [[AdmanBanner alloc] init];
        [ab setAdId:[a valueForKey:@"bannerID"]];

        int width = [[a valueForKey:@"width"] intValue];
        int height = [[a valueForKey:@"height"] intValue];
        NSString *companionAdSize = [NSString stringWithFormat:@"%ix%i", width, height];
        [ab.companionAds setValue:[[AdmanCreative alloc] init] forKey:companionAdSize];
        ab.companionAds[companionAdSize].staticResource = [a valueForKey:@"src"];

        [ab setUrl:[a valueForKey:@"src"]];
        [ab setUrlToNavigateOnClick:[a valueForKey:@"urlToNavigateOnClick"]];

        NSArray<NSDictionary <NSString*, id>*> *statData = [a valueForKey:@"statistics"];
        if (statData != nil && [statData count] > 0)
            [ab.statData save:[statData.firstObject valueForKey:@"url"] forEvent:AdmanStatEventClickTracking];

        statData = [a valueForKey:@"statistics"];
        if (statData != nil && [statData count] > 1)
            [ab.statData save:[[statData objectAtIndex:1] valueForKey:@"url"] forEvent:AdmanStatEventCreativeView];

        success(ab);
    }];
    [task resume];

    // [netManager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
}

- (void)prepare {
    if (!self.silentPreload) {
        [self setState:AdmanStateFetchingInfo];
        [NotificationAdmanBase sendEvent:AdmanMessageEventFetchingInfo];
    }

    self.intentTapped = FALSE;
    _properties.lastFetchTime = [NSDate date];

    NSString *type;
    switch (_properties.type) {
        case AdmanTypeAny:
        case AdmanTypeVoiceMultipleAds:
            type = @"any";
            break;
        case AdmanTypeVoice:
            type = @"voice";
            break;
        default:
            type = @"audio";
            break;
    }
    
    NSString *slot;
    switch (_properties.slot) {
        case AdmanPreroll:
            slot = @"preroll";
            break;
        case AdmanMidroll:
            slot = @"midroll";
            break;
        case AdmanPostroll:
            slot = @"postroll";
            break;
        default:
            slot = @"instreamatic";
            break;
    }

    [self isMicrophoneEnabled];
    NSString *params = [NSString stringWithFormat:@"site_id=%ld&player_id=%ld&type=%@&slot=%@&idfa=%@&microphone=%li",
                        (long)_properties.siteId, (long)_playerId, type, slot,
                        _properties.idfa, (long) [self isMicrophoneEnabled]];
    NSString *can_show = [NSString stringWithFormat:@"https://%@.instreamatic.com/live/can_show.gif?%@", [_statistic getServer], params];
    [_statistic save:can_show forEvent:AdmanStatEventCanShow];

    [self requestSection:sPreroll];
}

- (void)prepareWithFormat:(AdmanFormat)format {
    [self prepare:format type:AdmanTypeAny maxDuration:0 adsCount:0];
}
- (void)prepareWithType:(AdmanType)type {
    [self prepare:AdmanFormatAny type:type maxDuration:0 adsCount:0];
}
- (void)prepare:(AdmanFormat)format type:(AdmanType)type {
    [self prepare:format type:type maxDuration:0 adsCount:0];
}

- (void)prepareWithDelay {
    NSInteger interval = (18 + arc4random_uniform(32));
    NSLog(@"Prepare delayed for %li", (long) interval);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(interval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self prepare];
    });
}

- (void)prepare:(AdmanFormat)format type:(AdmanType)type maxDuration:(NSInteger)maxDuration adsCount:(NSInteger)adsCount {
    _unknownPhraseIteration = 0;
    _transcriptedPhrase = @"";

    _properties.format = format;
    _properties.type = type;

    // min value 1
    if (adsCount)
        _properties.maxAdsCount = adsCount;
    else
        _properties.maxAdsCount = 1;

    // set no limit for max ads duration if parameter lower than 10
    if (maxDuration < 10)
        _properties.maxAdBlockDuration = 0;
    else
        _properties.maxAdBlockDuration = maxDuration;

    [self prepare];
}

- (void)prepareWithAdLimits:(NSInteger)maxDuration adsCount:(NSInteger)adsCount {
    [self prepare:AdmanFormatAny type:AdmanTypeAny maxDuration:maxDuration adsCount:adsCount];
}

- (void)startDefaultPlayback {
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventStart];
    NSString *size = [AdmanVastParserDelegate getDefaultImageSize];
    if ((AdmanCurrentAd).companionAds[size].tracking && (AdmanCurrentAd).companionAds[size].tracking[@"creativeView"])
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView forCompanion:(AdmanCurrentAd).companionAds[size]];
    else
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView];
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventImpression];

    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);

    if (((AdmanPlayerItem *) _player.currentItem).itemType == AdmanPlayerItemInitial)
        [self itemDidLoaded:_activeAd itemType:AdmanPlayerItemAd];
    else
        [_player addUrl:ad.sourceUrl adID:_activeAd itemType:AdmanPlayerItemAd];
}

#pragma mark Playback control

- (void)finishAdPlayback {
    firstQuarterReported = false;
    secondQuarterReported = false;
    thirdQuarterReported = false;

    if (_state == AdmanStateVoiceInteractionStarted)
        return;

    if ([(AdmanCurrentAd).responses count] > 0) {
        [_recognizer stop];
        [self stopPlaybackProgressTimer];
    }
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventComplete];

    if ((AdmanCurrentAd).bannerImage)
        [self viewClosed];

    if ([self hasNextAd]) {
        [self playNextAd];
    } else {
        [NotificationAdmanPlayer sendEvent:AdmanPlayerEventComplete];
        [self.player removeAllItems];
        [self setState:AdmanStatePlaybackCompleted];
        if (_properties.dtmfAdsDetection)
            [self prepareWithDelay];
    }
}

- (void)startPlayback {
    /**
     Only for voice ads in background mode in WiFi networks
     Sience iOS 12 release we cant turn microphone on when app in background

     If application was in background and preloaded creative require voice activity
     We need to change default ad type to audio only, preload it and play
     */
    [self isInBackground:NO then:^void(id state) {
        if ([(AdmanCurrentAd) isVoiceAd] && [state intValue] == 1 && [self.networkStatus currentReachabilityStatus] == ReachableViaWiFi) {
            [[self.cache valueForKey:@"requestParams"] setValue:@"digital" forKey:@"type"];
            NSLog(@"Trying to play voice ad in background, start preloading digital ad");
            [[[AdmanVastParser alloc] init] startLoadingAdsFromUrl:[self.cache valueForKey:@"requestURL"]
                                                        withParams:[self.cache valueForKey:@"requestParams"]
                                                        onComplete:^void(id adslist){
                NSLog(@"Digital ad preloaded");
                [[self.cache valueForKey:@"requestParams"] setValue:@"any" forKey:@"type"];
                if ([adslist count] == 0) {
                    NSLog(@"No ads for type digital at this time");
                    [self startDefaultPlayback];
                    return;
                }

                self->_activeAd = 0;
                self->_adsList = adslist;
                self.intentTapped = NO;
                [(AdmanCurrentAd).statData reportEvent:AdmanStatEventStart];
                [(AdmanCurrentAd).statData reportEvent:AdmanStatEventImpression];

                NSString *size = [AdmanVastParserDelegate getDefaultImageSize];
                if ((AdmanCurrentAd).companionAds[size].tracking && (AdmanCurrentAd).companionAds[size].tracking[@"creativeView"])
                    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView
                                              forCompanion:(AdmanCurrentAd).companionAds[size]];
                else
                    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView];

                NSLog(@"Started playback for new digital ad in background");
                [self.player addUrl:((NSArray<AdmanBanner *> *) adslist).firstObject.sourceUrl adID:self.activeAd itemType:AdmanPlayerItemAd];
                return;
            }
                                                            onFail:^void(NSError *err) {
                NSLog(@"Failed to preload digital ad: %@", [err localizedDescription]);
                [[self.cache valueForKey:@"requestParams"] setValue:@"any" forKey:@"type"];
                [self startDefaultPlayback];
            }];
        } else {
            [self startDefaultPlayback];
        }

        if ([self hasNextAd])
            [self preloadAd:self.activeAd + 1 onComplete:^void(id ad) {
                return;
            } onFail:nil];
    }];
}

- (void)startPlayer {
    [_player play];
}

- (void)play {
    if (_state == AdmanStatePlaying)
        return;

    if (_vastLiveDurationTimer != NULL) {
        [_vastLiveDurationTimer invalidate];
        _vastLiveDurationTimer = NULL;
        _silentPreload = FALSE;
    }

    AVAudioSession* session = [AVAudioSession sharedInstance];
    if (_properties.isAudioSessionSetupEnabled && session.category != AVAudioSessionCategoryPlayAndRecord) {
        if (@available(iOS 10.1, *))
            [session setCategory:AVAudioSessionCategoryPlayAndRecord
                                             withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetoothA2DP | AVAudioSessionCategoryOptionAllowAirPlay
                                                   error:nil];
        else
            [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
        [session setActive:YES error: nil];
    }

    if (_state == AdmanStateReadyForPlayback || _state == AdmanStateAdChangedForPlayback) {
        // TODO play intro audio
        [self startPlayback];
        [self updateProgress];
    } else if (_state == AdmanStateSpeechRecognizerStarted || _state == AdmanStateVoiceInteractionStarted) {
        [self startListening];
    } else if (_state == AdmanStateVoiceResponsePlaying) {
        [_player play];
    } else {
        NSLog(@"Error: Invalid internal state (%lu) on playback attempt", (unsigned long)_state);
    }
}

- (void)audioPlayerDidFinishPlaying {
    [self log:@"Ad playback completed"];

    [_playbackProgressTimer invalidate];
    _playbackProgressTimer = nil;

    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
    if (![ad isVoiceAd]) {
        [self finishAdPlayback];
        return;
    } else
        [self enableVoiceSpeechDetection:ad];
}

- (void)playNextAd {
    _activeAd += 1;
    _unknownPhraseIteration = 0;
    _properties.voiceResponseDelay = ((AdmanBannerWrapper *)(AdmanCurrentAd)).responseDelay;
    [self setState:AdmanStateAdChangedForPlayback];
    [NotificationAdmanPlayer sendEvent:AdmanPlayerEventReady];
    [self play];

    [_recognizer stop];
    [_recognizer closeConnection];

    [self log:[NSString stringWithFormat:@"libAdman: start playing next ad(%@) from sequence", (AdmanCurrentAd).adId]];
}

- (void)pause {
    if (_state == AdmanStatePlaying || _state == AdmanStateVoiceInteractionStarted || _state == AdmanStateVoiceResponsePlaying) {
        [_player pause];
        [self stopPlaybackProgressTimer];
        //[_uiManager showPlayButton];
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventPause];
        [self setState:AdmanStatePaused];
        [NotificationAdmanPlayer sendEvent:AdmanPlayerEventPause];
    }
}

- (void)resume {
    if (_state == AdmanStatePaused) {
        [_player play];
        [self updateProgress];
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventResume];
        [self setState:AdmanStatePlaying];
        [NotificationAdmanPlayer sendEvent:AdmanPlayerEventPlaying userData:(AdmanCurrentAd)];
        //[_uiManager showPauseButton];
    }
}

- (void)stopPlaybackProgressTimer {
    if (_playbackProgressTimer != nil)
        [_playbackProgressTimer invalidate];
    _playbackProgressTimer = nil;
}

- (void)rewind {
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventRewind];
    [_player pause];
    [_player seekToTime:CMTimeMakeWithSeconds(0, NSEC_PER_SEC)];
    [_player play];

    [self setState:AdmanStatePlaying];
    [NotificationAdmanPlayer sendEvent:AdmanPlayerEventPlaying userData:(AdmanCurrentAd)];
    [self stopPlaybackProgressTimer];
    [self updateProgress];
}

- (void)stopPlayer {
    if ([(AdmanCurrentAd).responses count] > 0) {
        [_recognizer stop];
        [_interactionTimeoutTimer invalidate];
    }

    if (_state == AdmanStateVoiceInteractionStarted || _state == AdmanStatePlaying)
        [_player pause];
    [self stopPlaybackProgressTimer];
    //[_uiManager showPlayButton];
}

- (void)stop {
    if (_state == AdmanStatePlaybackCompleted || _state == AdmanStateError || _state == AdmanStateStopped)
        return;

    [self resetRecognizerState];

    [_player pause];
    [_player replaceCurrentItemWithPlayerItem:nil];

    [self setState:AdmanStateStopped];
    [NotificationAdmanBase sendEvent:AdmanMessageEventStopped];
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventComplete];
}

- (void)skip {
    [_player pause];
    [_player replaceCurrentItemWithPlayerItem:nil];
    [self resetRecognizerState];

    if ([self hasNextAd]) {
        [self playNextAd];
    } else {
        if (_state != AdmanStateError) {
            [NotificationAdmanPlayer sendEvent:AdmanPlayerEventComplete];
            [self.player removeAllItems];
            [self setState:AdmanStatePlaybackCompleted];
            if (_properties.dtmfAdsDetection)
                [self prepareWithDelay];
        }
    }
}

#pragma mark Recognition

- (void)enableVoiceSpeechDetection:(AdmanBannerWrapper *)ad {
    [VACNetworkManager setWebsocketUrl:[self getSpeechRecognizerUrl:[Adman getVoiceServer:_properties.region location:_properties.recognitionBackendLocation]]];
    [_recognizer start:^() {
    } withOptions:[self getWebsocketHeaderOptions]];
    [self startListening];
}

- (void)endSpeechDetectSel {
    [_recognizer endSpeechDetection];
    [_recognizer stopListening];
    if (_state != AdmanStateSpeechRecognizerStopped) {
        [self setState:AdmanStateSpeechRecognizerStopped];
        [NotificationAdmanVoice sendEvent:AdmanVoiceEventRecognizerStopped];
    }

    if ([(AdmanCurrentAd).assets objectForKey:AdmanBannerAssetMicOffCache] != nil)
        [self.player addAsset:[(AdmanCurrentAd).assets objectForKey:AdmanBannerAssetMicOffCache] adID:_activeAd itemType:AdmanPlayerItemMicOff];
}

- (void)processRecognizerResult:(NSDictionary *)result {
    _properties.clientVoiceActivityEnded = NO;
    [_recognizer stopListening];
    if (_state != AdmanStateSpeechRecognizerStopped) {
        [self setState:AdmanStateSpeechRecognizerStopped];
        [NotificationAdmanVoice sendEvent:AdmanVoiceEventRecognizerStopped];
    }

    NSString *actionString = [result objectForKey:@"action"];

    if ([self.delegate respondsToSelector:NSSelectorFromString(@"phraseRecognized:")])
        [self.delegate phraseRecognized:result];

    if ([actionString isEqualToString:@"unknown"] || [actionString isEqualToString:@"silence"]) {
        [self playUnknownAction:actionString];
        return;
    }

    if ([actionString isEqualToString:@"skip"]) {
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventSkip];
        [self skip];
        return;
    }

    if ([actionString isEqualToString:@"swearing"])
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventResponseSwearing];

    if ([actionString rangeOfString:@"stop"].location != NSNotFound) {
        [self stop];
        return;
    }

    AdmanInteraction *action = nil;
    for (NSString *actionName in (AdmanCurrentAd).responses) {
        if ([actionString isEqualToString:[(AdmanCurrentAd).responses[actionName] firstObject].type]) {
            action = [(AdmanCurrentAd).responses[actionName] firstObject];
            break;
        }
    }

    if (action != nil)
        [self reactionForInteraction:action from:[result objectForKey:@"source"]];
    else
        [self playUnknownAction:@"unknown"];
}

- (void)reactionForInteraction:(AdmanInteraction *)response from:(NSString *)source {
    NSLog(@"Performing reaction for %@", response.action);
    NSMutableDictionary *valList = [NSMutableDictionary dictionaryWithCapacity:response.values.count];
    for (AdmanResponseValue *val in response.values)
        valList[val.type] = val;

    _player.userData = nil;

    AdmanResponseValue *audio = valList[@"audio"];
    if (audio == nil)
        audio = valList[@"media"];
    AdmanResponseValue *link = valList[@"link"];
    AdmanResponseValue *phone = valList[@"phone"];
    AdmanResponseValue *custom_intent = valList[@"custom_intent"];

    if (source == nil)
        [(AdmanCurrentAd).statData reportEvent:[NSString stringWithFormat:@"response_%@", response.action]];

    if (audio != nil) {
        [self stopPlayer];
        [self setState:AdmanStateVoiceResponsePlaying];
        [NotificationAdmanVoice sendEvent:AdmanVoiceEventResponsePlaying];
        _player.userData = valList;
        [_player addUrl:(NSString *) audio.data adID:_activeAd itemType:AdmanPlayerItemReaction];
    } else if (link != nil) {
        [self openUrlSafe:(NSString *) link.data type:AdmanLinkTypeUrl];
        [self skip];
    } else if (phone != nil) {
        [self openUrlSafe:[NSString stringWithFormat:@"tel:%@", phone.data] type:AdmanLinkTypeMobile];
        [self skip];
    } else if (custom_intent != nil && [_delegate respondsToSelector:NSSelectorFromString(@"customVoiceIntentHandler")]) {
        [self.delegate customVoiceIntentHandler];
    }
}

- (void)recognizedIntentLoaded {
    [self setState:AdmanStateVoiceResponsePlaying];
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventResponsePlaying];
    [self log:@"Playing voice intent"];
    [_player play];
    [self updateProgress];
    [[NSRunLoop mainRunLoop] addTimer:_playbackProgressTimer forMode:NSRunLoopCommonModes];
}

- (void)recognizedIntentPlayed {
    [self log:@"Voice intent succefully played"];
    NSDictionary * valList = nil;
    if ([_player.userData isKindOfClass:[NSString class]])
        valList = @{};
    else
        valList = (NSDictionary *) _player.userData;

    AdmanResponseValue *link = valList[@"link"];
    AdmanResponseValue *phone = valList[@"phone"];
    AdmanResponseValue *custom_intent = valList[@"custom_intent"];

    [_playbackProgressTimer invalidate];
    _playbackProgressTimer = nil;

    if (link != nil && link.data != nil)
        [self openUrlSafe:(NSString *) link.data type:AdmanLinkTypeUrl];
    if (phone != nil)
        [self openUrlSafe:[NSString stringWithFormat:@"tel:%@", phone.data] type:AdmanLinkTypeMobile];
    if (custom_intent != nil && [self.delegate respondsToSelector:NSSelectorFromString(@"customVoiceIntentHandler")])
        [self.delegate customVoiceIntentHandler];
    _player.userData = nil;
    [self skip];
}

- (void)unknownActionDidPlayed {
    [self log:@"Unknown intent played"];
    [_playbackProgressTimer invalidate];
    _playbackProgressTimer = nil;
    self.unknownPhraseIteration += 1;
    [self setState:AdmanStateVoiceInteractionEnded];
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventInteractionEnded];

    NSString *actionType = (NSString *) _player.userData;
    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
    id voiceAction = (AdmanCurrentAd).responses[actionType];
    NSLog(@"Current voice response iteration: %ld:%ld", (long)self.unknownPhraseIteration, (long)[ad.responses[actionType] count]);

    if ([voiceAction count] > self.unknownPhraseIteration && [actionType isEqualToString:@"unknown"]) {
        _player.userData = nil;
        [self.recognizer start:nil withOptions:[self getWebsocketHeaderOptions]];
        [self startListening];
    } else {
        [self finishAdPlayback];
    }
}

- (void)playUnknownAction:(NSString *)actionType {
    NSLog(@"Playing interaction for %@", actionType);
    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
    bool isSilence = [actionType isEqualToString:@"silence"];

    [self stopPlayer];

    if (!isSilence && _unknownPhraseIteration >= [ad.responses[actionType] count]) {
        [self finishAdPlayback];
        return;
    }

    AdmanInteraction *action;
    if (!isSilence)
        action = ad.responses[actionType][_unknownPhraseIteration];
    else
        action = ad.responses[actionType].firstObject;
    [(AdmanCurrentAd).statData reportEvent:[NSString stringWithFormat:@"response_%@", actionType]];

    _player.userData = actionType;
    //[_uiManager showPauseButton];
    [_player addUrl:(NSString *) ((AdmanResponseValue *) [action.values firstObject]).data adID:_activeAd itemType:AdmanPlayerItemUnknown];
}

- (void)resetRecognizerState {
    [_recognizer stop];
    [_recognizer closeConnection];

    if (_interactionTimeoutTimer) {
        [_interactionTimeoutTimer invalidate];
        _interactionTimeoutTimer = nil;
    }
    if (_voiceResponseDelayTimer) {
        [_voiceResponseDelayTimer invalidate];
        _voiceResponseDelayTimer = nil;
    }
    if (_playbackProgressTimer) {
        [_playbackProgressTimer invalidate];
        _playbackProgressTimer = nil;
    }
    if (_endSpeechDetectionTimer) {
        [_endSpeechDetectionTimer invalidate];
        _endSpeechDetectionTimer = nil;
    }

    [self setState:AdmanStateVoiceInteractionEnded];
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventInteractionEnded];
}

- (void)processIntent:(NSDictionary *)dict {
    [self resetRecognizerState];

    if (_state == AdmanStatePlaying || _state == AdmanStateVoiceResponsePlaying) {
        [_player pause];
        [_player replaceCurrentItemWithPlayerItem:nil];
    }

    if (_state != AdmanStateSpeechRecognizerStopped) {
        [self setState:AdmanStateSpeechRecognizerStopped];
        [NotificationAdmanVoice sendEvent:AdmanVoiceEventRecognizerStopped];
    }

    [self processRecognizerResult:dict];
}

- (void)voiceInteractionDidNotFinishedAfterTimeout {
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventError];
    [self log:@"504, Recognition server timeout"];
    [self errorRecived:[NSError errorWithDomain:InstreamaticDomain
                                           code:42
                                       userInfo:@{NSLocalizedDescriptionKey: @"504, Recognition server timeout"}] param: 1];
}

- (void)startListeningCb {
    if (_state == AdmanStateError || _state == AdmanStatePlaybackCompleted || _state == AdmanStateReadyForPlayback)
        return;

    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
    [self setState:AdmanStateSpeechRecognizerStarted];
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventRecognizerStarted];
    [_recognizer startListening];

    if (_interactionTimeoutTimer != nil) {
        [_interactionTimeoutTimer invalidate];
        _interactionTimeoutTimer = nil;
    }

    long responseTime = ad.responseTime;
    if (_properties.customAdResponseTime > 0)
        responseTime = _properties.customAdResponseTime;
                    long delay = [self getResponseDelay];

    _interactionTimeoutTimer = [NSTimer timerWithTimeInterval:responseTime + delay + _properties.serverTimeout
                                                         target:self
                                                       selector:@selector(voiceInteractionDidNotFinishedAfterTimeout)
                                                       userInfo:nil repeats:NO];

    _endSpeechDetectionTimer = [NSTimer scheduledTimerWithTimeInterval:responseTime + delay - 1 target:self
                                                                  selector:@selector(endSpeechDetectSel) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_endSpeechDetectionTimer forMode:NSRunLoopCommonModes];
    //[[NSRunLoop mainRunLoop] addTimer:_interactionTimeoutTimer forMode:NSRunLoopCommonModes];

    [self setState:AdmanStateVoiceInteractionStarted];
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventInteractionStarted];
}

- (void)startListening {
    if (_recognizer.state == VACRecognizerStateListening)
        return;

    AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);
    if ([ad.assets objectForKey:AdmanBannerAssetMicOnCache] != nil) {
        [_player pause];
        [_player replaceCurrentItemWithPlayerItem:nil];
        [_player addAsset:[ad.assets objectForKey:AdmanBannerAssetMicOnCache] adID:_activeAd itemType:AdmanPlayerItemMicOn];
    } else {
        [self startListeningCb];
    }
}

- (void)responseMicOffSoundPlayed {
    _properties.clientVoiceActivityEnded = YES;
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    if ([_properties respondsToSelector:aSelector])
        return _properties;

    return [super forwardingTargetForSelector:aSelector];
}

- (void)reportAdEvent:(nonnull NSString *)eventName {
    if ([eventName isEqualToString:AdmanStatEventCanShow])
        [self.statistic reportEvent:eventName];
    else
        [(AdmanCurrentAd).statData reportEvent:eventName];
}

#pragma mark - Interface Control

- (void)preloadImage:(UIImage *)image {
    CGImageRef ref = image.CGImage;
    size_t width = CGImageGetWidth(ref);
    size_t height = CGImageGetHeight(ref);
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, width, height, 8, width * 4, space, kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    CGColorSpaceRelease(space);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), ref);
    CGContextRelease(context);
}

- (void)showBannerForSpot:(UIView *)spot withType:(AdmanBannerType)type {
    NSLog(@"showBannerForSpot:withType: is called");

    [self requestCommonBanner:type success:^(AdmanBanner *banner) {
        if (banner.url == nil) {
            NSLog(@"Warning! There is no banners");
            return;
        }

        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,spot.frame.size.width,spot.frame.size.height)];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:banner.url]]];
        [iv setImage:image];
        iv.userInteractionEnabled = true;

        UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerTouched:)];
        tapper.delegate = self;
        [iv addGestureRecognizer:tapper];

        [spot addSubview:iv];
        [spot bringSubviewToFront:iv];
        NSString *size = [AdmanVastParserDelegate getDefaultImageSize];
        if ((AdmanCurrentAd).companionAds[size].tracking && (AdmanCurrentAd).companionAds[size].tracking[@"creativeView"])
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView forCompanion:(AdmanCurrentAd).companionAds[size]];
        else
            [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView];
    } failure:^(NSError *error) {
        NSLog(@"Error on loading banner: %@", [error localizedDescription]);
    }];
}

- (void)showBannerInPlaceholder:(UIView *)placeholder withButton:(UIButton *)button {
    if ((AdmanCurrentAd).url != nil) {
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, placeholder.frame.size.width, placeholder.frame.size.height)];
        iv.contentMode = UIViewContentModeScaleAspectFit;

        [placeholder addSubview:iv];
        [placeholder bringSubviewToFront:iv];
    } else {
        NSLog(@"Warning! Banner info has not been fetched yet! Wait until state=AdmanStateReadyForPlayback");
    }
}

#pragma mark Errors handling

/*&
 When network is gone and restore after AVPlayerItem buffer end reached
 need properly return to old state
 */
- (void)networkStateDidChanged:(bool)available {
    if (!available && [AdmanPlayer getPreloadMode] == AdmanPreloadModeProgressive) {
        [self setState:AdmanStateError];
        [NotificationAdmanBase sendEvent:AdmanMessageEventError];
    }

    if (available && [AdmanPlayer getPreloadMode] == AdmanPreloadModeProgressive
        && self.state == AdmanStateError) {
        if (self.fallbackState == AdmanStatePlaybackCompleted)
            _state = self.fallbackState;
        else
            [self setState:self.fallbackState];

        if (self.fallbackState == AdmanStatePlaying) {
            if (CMTimeCompare(self.player.currentItem.duration, self.player.currentItem.currentTime) == 0) {
                if (((AdmanPlayerItem *) self.player.currentItem).isPlayed)
                    [self.player playbackFinished:nil];
                else
                    [self skip];
            } else {
                [self.player play];
            }
        }
    }
}

- (void)errorRecived:(NSError *)err param:(int) param {
    if ([_delegate respondsToSelector:NSSelectorFromString(@"errorReceived:")] && err != nil)
        [self.delegate errorReceived:err];

    _error = err;
    NSLog(@"Error recived: %@", err.localizedDescription);
    [self resetRecognizerState];

    [_player pause];
    [_player replaceCurrentItemWithPlayerItem:nil];

    [self setState:AdmanStateError];
    [self logBadRequest];
    [NotificationAdmanBase sendEvent:AdmanMessageEventError];
    if (param == 0)
        [self skip];
    else if (param == 1)
        [self processIntent:@{ @"transcript": @"No",@"action": @"negative", @"playMicOff": @"No" }];;
}

- (void)errorRecived:(NSError *)err {
    [self errorRecived:err param:0];
}

- (void)openUrlSafe:(NSString *)URL type:(NSInteger)linkType {
    @try {
        NSLog(@"%@: Url should be %@ opened", TAG, URL);
        [self isInBackground:YES then:^(id state) {
            if ([state intValue] == 1 && linkType != AdmanLinkTypeMobile) {
                [NotificationAdmanControl sendEvent:AdmanControlEventLink userData:[NSURL URLWithString:URL]];
            } else {
                NSCharacterSet *allowedCharacters = [NSCharacterSet URLQueryAllowedCharacterSet];
                NSString *encodedURL = [URL stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];

                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:encodedURL] options:@{} completionHandler:nil];
            }
        }];
    } @catch (NSException *exception) {
        NSLog(@"Failed to open url: %@, %@", URL, [exception description]);
    } @finally {

    }
}

#pragma mark - VACRecognizer delegate

- (void)recognizer:(VACRecognizer *)recogizer didChangeState:(VACRecognizerState)state {

}

- (void)recognizer:(VACRecognizer *)recogizer didRecognizeCommand:(NSDictionary *)result {

}

- (void)recognizer:(VACRecognizer *)recogizer didFailWithError:(NSError *)error {
    [_interactionTimeoutTimer invalidate];
    [self log:[NSString stringWithFormat:@"Recognition error: %@", [error localizedDescription]]];
    [NotificationAdmanVoice sendEvent:AdmanVoiceEventError];
    [self errorRecived:error param:1];
}

- (void)onSessionEndedAfterPlayback {
    [self processRecognizerResult:(NSDictionary *) _player.userData];
}

- (void)endVoiceSession:(NSDictionary *)result {
    if (self.properties.clientVoiceActivityEnded)
        [self processRecognizerResult:result];
    else
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self endVoiceSession:result];
        });
}

- (void)recognizer:(VACRecognizer *)recogizer didSessionEnded:(NSDictionary *)result {
    if (self.state == AdmanStatePlaybackCompleted || self.state == AdmanStateError || self.state == AdmanStateStopped)
        return;

    if (self.intentTapped)
        return;

    [self.interactionTimeoutTimer invalidate];
    self.interactionTimeoutTimer = nil;

    if (result == nil) {
        [self log:@"Transcripted phrase: empty; reaction: empty"];
        [self skip];
        return;
    }
    [self log:[NSString stringWithFormat:@"adman.websocket: phrase - %@, reaction - %@", result[@"transcript"], result[@"action"]]];

    if ([(AdmanCurrentAd).assets objectForKey:AdmanBannerAssetMicOffCache] == nil)
        self.properties.clientVoiceActivityEnded = YES;

    [self endVoiceSession:result];
}

- (void)recognizer:(VACRecognizer *)recogizer didConnectionOpened:(VACRecognizerState)state {

}

#pragma mark DTMF

- (void)enableDtmfAdsDetectionForStation:(nonnull NSString *)name preload:(BOOL)preload vc:(nullable UIViewController *)vc {
    // [_uiManager setBannerParentVC:vc];

    [_properties setDtmfAdsDetection:YES];
    [_properties setDtmfStationKey:name];

    _dtmfListener = [[AdmanWebsocketDtmfListener alloc] init];
    _dtmfListener.delegate = self;
    [_dtmfListener start:_properties.dtmfStationKey];

    if (preload)
        [self prepareWithDelay];
}

- (void)disableDtmfAdsDetection {
    [_properties setDtmfAdsDetection:NO];
    [_dtmfListener stop];
    _dtmfListener = nil;
}

- (void)dtmfIn {
    if ([_delegate respondsToSelector:@selector(dtmfIn)])
        [self.delegate dtmfIn];

    if (_state == AdmanStateReadyForPlayback)
        [self play];
}

- (void)dtmfOut {
    if ([_delegate respondsToSelector:@selector(dtmfOut)])
        [self.delegate dtmfOut];
}

#pragma mark - UI events

- (void)bannerTouched:(NSString *)urlToNavigate {
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventClickTracking forCompanion:[(AdmanCurrentAd).companionAds valueForKey:[AdmanVastParserDelegate getDefaultImageSize]]];
    if ([_delegate respondsToSelector:@selector(bannerTouched:)])
        [self.delegate bannerTouched:urlToNavigate];

    if (![(AdmanCurrentAd) isVoiceAd])
        [self viewClosed];
    
    if ([(AdmanCurrentAd) isVoiceAd] && _state == AdmanStateVoiceInteractionStarted) {
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventResponseOpenBanner];
        [self processIntent:@{ @"transcript": @"Yes", @"action": @"positive", @"playMicOff": @"No", @"source": @"ui" }];
    } else
        [self openUrlSafe:urlToNavigate type:AdmanLinkTypeDirect];
}

- (void)viewClosed {
    // if playback was stopped and view closed
    if (_state == AdmanStatePaused)
        [self resume];

    if ([_delegate respondsToSelector:@selector(viewClosed)])
        [self.delegate viewClosed];
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventClose];
}

- (void)positiveIntent {
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventResponseOpenButton];
    [self processIntent:@{ @"transcript": @"Yes",@"action": @"positive", @"playMicOff": @"No", @"source": @"ui" }];
}

- (void)negativeIntent {
    [(AdmanCurrentAd).statData reportEvent:AdmanStatEventResponseNegativeButton];
    [self processIntent:@{ @"transcript": @"No",@"action": @"negative", @"playMicOff": @"No", @"source": @"ui" }];
}
/**
 Event for fullscreen banner
 */
- (void)updateProgress {
    if (_playbackProgressTimer != nil)
        [self stopPlaybackProgressTimer];
    _playbackProgressTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                              target:self
                                            selector:@selector(updateProgressSelector:)
                                            userInfo:nil
                                             repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_playbackProgressTimer forMode:NSRunLoopCommonModes];
}

- (void)updateProgressSelector:(NSTimer *) timer {
    [self setProgress:(CMTimeGetSeconds(_player.currentItem.currentTime)/CMTimeGetSeconds(_player.currentItem.duration))];

    int current = (int)(_progress*100);
    if (current > 25 && !firstQuarterReported) {
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventFirstQuartile];
        firstQuarterReported = true;
    } else if (current > 50 && !secondQuarterReported) {
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventMidpoint];
        secondQuarterReported = true;
        [self log:@"Playback midpoint"];

    } else if (current > 75 && !thirdQuarterReported) {
        [(AdmanCurrentAd).statData reportEvent:AdmanStatEventThirdQuartile];
        [NotificationAdmanBase sendEvent:AdmanMessageEventAlmostCompleted];
        thirdQuarterReported = true;
    }

    [NotificationAdmanPlayer sendEvent:AdmanPlayerEventProgress
                              userData:[NSNumber numberWithLong:self.estimatedTime]];
}

#pragma mark - AdmanPlayerDelegate

- (void)itemDidLoaded:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType {

    switch (itemType) {
        case AdmanPlayerItemAd:
            (AdmanCurrentAd).duration = CMTimeGetSeconds(_player.currentItem.duration);
            [_player play];
            [NotificationAdmanPlayer sendEvent:AdmanPlayerEventPlaying userData:(AdmanCurrentAd)];
            [self setState:AdmanStatePlaying];
            [self log:@"Playback started"];
            break;
        case AdmanPlayerItemReaction:
        case AdmanPlayerItemUnknown:
            [self recognizedIntentLoaded];
            break;
        case AdmanPlayerItemMicOn:
        case AdmanPlayerItemMicOff:
            [self startPlayer];
            break;
        case AdmanPlayerItemInitial:
            (AdmanCurrentAd).duration = CMTimeGetSeconds(_player.currentItem.duration);
            [self log:@"Creative preloaded and ready for playback"];
            if (!self.silentPreload) {
                [self setState:AdmanStateReadyForPlayback];
                [NotificationAdmanPlayer sendEvent:AdmanPlayerEventReady];
            }
            self.silentPreload = FALSE;
            [self enableVastExpiration];
            break;
    }
}

- (void)itemPlaybackDidFailed:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType err:(nullable NSError *)err {
    switch (itemType) {
        case AdmanPlayerItemAd:
        case AdmanPlayerItemInitial:
            [self errorRecived:err];
        case AdmanPlayerItemReaction:
        case AdmanPlayerItemUnknown:
            [self skip];
            [self log:err.localizedDescription];
            break;
        case AdmanPlayerItemMicOn:
            [self startListeningCb];
            break;
        case AdmanPlayerItemMicOff:
            [self responseMicOffSoundPlayed];
            break;
    }
}

- (void)itemPlaybackDidFinished:(NSInteger)adID itemType:(AdmanPlayerItemType)itemType {
    switch (itemType) {
        case AdmanPlayerItemAd:
        case AdmanPlayerItemInitial:
            [self audioPlayerDidFinishPlaying];
            break;
        case AdmanPlayerItemReaction:
            [self recognizedIntentPlayed];
            break;
        case AdmanPlayerItemUnknown:
            [self unknownActionDidPlayed];
            break;
        case AdmanPlayerItemMicOn:
            [self startListeningCb];
            break;
        case AdmanPlayerItemMicOff:
            [self responseMicOffSoundPlayed];
            break;
    }
}

- (void)log:(NSString *)logString {
    if ([_delegate respondsToSelector:NSSelectorFromString(@"log:")])
        [self.delegate log:logString];
    NSLog(@"%@", logString);
}

- (void)logBadRequestCb {
    self.allowPreload = TRUE;
    NSLog(@"Preloading enabled");
}

- (void)logBadRequest {
    [self.statistic.badRequests addObject:[NSDate date]];

    if ([self.statistic.badRequests count] >= 2 && self.allowPreload) {
        NSTimeInterval seconds = [self.statistic.badRequests.lastObject timeIntervalSinceDate:self.statistic.badRequests.firstObject];
        NSTimeInterval timeout = 60 * 1.2;
        if (seconds < timeout) {
            [self.statistic.badRequests removeAllObjects];

            self.allowPreload = FALSE;
            NSLog(@"Too many failed requests in short time (2 in ~70 sec), preloading disabled for current Adman instance");
            NSTimer *allowPreload = [NSTimer timerWithTimeInterval:timeout target:self selector:@selector(logBadRequestCb) userInfo:nil repeats:NO];
            [[NSRunLoop mainRunLoop] addTimer:allowPreload forMode:NSRunLoopCommonModes];
        } else {
            [self.statistic.badRequests removeObjectAtIndex:0];
        }
    }
}

- (void)creativeDidFailed:(nonnull NSError *)error {
    NSLog(@"fetching error (source instreamatic): %@", [error localizedDescription]);
    if ([self.delegate respondsToSelector:NSSelectorFromString(@"errorReceived:")])
        [self.delegate errorReceived:error];
    if (_vastLiveDurationTimer != NULL) {
        [self log:[NSString stringWithFormat:@"error recived on revalidating VAST: %@", error.localizedDescription]];
        [_vastLiveDurationTimer invalidate];
        _vastLiveDurationTimer = NULL;
        return;
    } else {
        [self log:[NSString stringWithFormat:@"error recived on preloading VAST: %@", error.localizedDescription]];
        [self errorRecived:error];
    }
}

- (NSString *)creativeDidParseRedirect:(NSString *)redirectURL {
    return redirectURL;
}

- (void)enableVastExpiration {
    if (_vastLiveDurationTimer != NULL) {
        [_vastLiveDurationTimer invalidate];
        _vastLiveDurationTimer = NULL;
    }
    _vastLiveDurationTimer = [NSTimer scheduledTimerWithTimeInterval:(AdmanCurrentAd).expirationTime target:self selector:@selector(isValidToPlayback) userInfo:nil repeats:FALSE];
    [[NSRunLoop mainRunLoop] addTimer:_vastLiveDurationTimer forMode:NSRunLoopCommonModes];
    NSLog(@"adman: vast lifetime - %li", (long) (AdmanCurrentAd).expirationTime);
}

- (void)creativeDidParsed:(nonnull NSArray<AdmanBanner *> *)ads {
    NSLog(@"VAST parsed succefully");

    if ([ads count] == 0) {
        NSLog(@"No ads to playback");
        [self logBadRequest];
        if (!self.silentPreload) {
            [self setState:AdmanStateAdNone];
            [NotificationAdmanBase sendEvent:AdmanMessageEventNone];
        }
        self.silentPreload = FALSE;
        return;
    }

    if (![self isMicrophoneEnabled] && [ads.firstObject isVoiceAd]) {
        NSString *message = @"Microphone disabled, failed to prepare voice ad";
        _error = [NSError errorWithDomain:InstreamaticDomain code:42 userInfo:@{NSLocalizedDescriptionKey: message}];
        [self setState:AdmanStateError];
        [NotificationAdmanBase sendEvent:AdmanMessageEventError];
        NSLog(@"%@", message);
        return;
    }

    [self isInBackground:NO then:^void(id state) {
        if ([state intValue] == 1 && self.properties.requestURL != nil) {
            NSString *message = @"App recieved in background via custom URL, failed to preare ad";
            self->_error = [NSError errorWithDomain:InstreamaticDomain code:42 userInfo:@{NSLocalizedDescriptionKey: message}];
            [self setState:AdmanStateTypeMismatch];
            [NotificationAdmanBase sendEvent:AdmanMessageEventError];
            NSLog(@"%@", message);
        } else {
            self->_activeAd = 0;
            self->_adsList = ads;
            self.intentTapped = NO;
            AdmanBannerWrapper *ad = ((AdmanBannerWrapper *)(AdmanCurrentAd));
            self.properties.voiceResponseDelay = ad.responseDelay;

            if (self.properties.region != AdmanRegionVoice)
                [self.statistic reportEvent:AdmanStatEventFetched];

            [self log:[NSString stringWithFormat:@"Data from ad server parsed succefully, preloading ad: %@", (AdmanCurrentAd).adId]];
            if (!self.silentPreload) {
                [self setState:AdmanStatePreloading];
                [NotificationAdmanBase sendEvent:AdmanMessageEventPreloading];
            }

            [self preloadAd:self.activeAd onComplete:^void(id ad) {
                if ([AdmanPlayer getPreloadMode] == AdmanPreloadModeProgressive)
                    [self.player addUrl:((AdmanBanner *) ad).sourceUrl adID:self.activeAd itemType:AdmanPlayerItemInitial];
                else
                    [AdmanPlayer preloadAsset:((AdmanBanner *) ad).sourceUrl onComplete:^void(AVAsset *asset) {
                        // TODO
                        if ([((AdmanBanner *) ad).sourceUrl isEqualToString:[((AVURLAsset *)self.player.currentItem.asset).URL absoluteString]]) {
                            return;
                        }

                        [self.player addAsset:asset adID:self.activeAd itemType:AdmanPlayerItemInitial];
                    } onFail:^(NSError *err) {
                        [self log:[NSString stringWithFormat:@"Failed to preload audio ad, %@", err.localizedDescription]];
                        [self errorRecived:err];
                    }];
            }
                     onFail:^void(NSError *err) {
                [self log:[NSString stringWithFormat:@"Failed to load creative, %@", err.localizedDescription]];
                [self errorRecived:err];
            }];
        }
    }];}

#pragma mark - NOTIFICATION_ADMAN_CONTROL

- (void)triggerActionAdmanControl:(NSNotification *)notification {
    if (_state == AdmanStateReadyForPlayback || _state == AdmanStateError || _state == AdmanStateAdNone || _state == AdmanStatePlaybackCompleted || _state == AdmanStateInitial)
        return;

    NSDictionary *dict = notification.userInfo;
    NotificationAdmanControl *message = [dict valueForKey:NOTIFICATION_ADMAN_CONTROL_KEY];
    if (message == nil)
        return;

    NSLog(@"%@ triggerActionAdmanControl: state: %@", TAG, [NotificationAdmanControl getEventString: message.event]);
    switch (message.event) {
        case AdmanControlEventPrepare:
            break;
        case AdmanControlEventStart:
            [self resume];
            break;
        case AdmanControlEventPause:
            [self pause];
            break;
        case AdmanControlEventResume:
            [self rewind];
            break;
        case AdmanControlEventSkip:
            [self viewClosed];
            break;
        case AdmanControlEventClick:
            [self bannerTouched:(AdmanCurrentAd).urlToNavigateOnClick];
            break;
        case AdmanControlEventSwipeBack:
            [self stop];
            break;
        case AdmanControlEventPositive:
            self.intentTapped = YES;
            [self positiveIntent];
            break;
        case AdmanControlEventNegative:
            self.intentTapped = YES;
            [self negativeIntent];
            break;
        default:
            break;
    }
}

#pragma mark - NOTIFICATION_ADMAN_PLAYER

- (void)triggerActionAdmanPlayer:(NSNotification *)notification {
    if (_state == AdmanStateReadyForPlayback || _state == AdmanStateError || _state == AdmanStateAdNone || _state == AdmanStatePlaybackCompleted || _state == AdmanStateInitial)
        return;

    NSDictionary *dict = notification.userInfo;
    NotificationAdmanPlayer *message = [dict valueForKey:NOTIFICATION_ADMAN_PLAYER_KEY];
    if (message != nil) {
        switch (message.event) {
            case AdmanPlayerEventReady:
                [NotificationAdmanBase sendEvent:AdmanMessageEventReady];
                break;
            case AdmanPlayerEventComplete:
                [NotificationAdmanBase sendEvent:AdmanMessageEventCompleted];
                break;
            default:
                break;
        }
    }
    else {
        [self log:@"Warning. Message of triggerActionAdmanPlayer is nil"];
    }
}

@end
