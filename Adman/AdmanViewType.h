/**
* Copyright 2019 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AdmanViewType_h
#define AdmanViewType_h

@import UIKit;

typedef NS_ENUM (NSUInteger, AdmanViewElement) {
    AdmanViewContainer = 1,
    AdmanViewBanner,
    AdmanViewClose,
    AdmanViewPlay,
    AdmanViewPause,
    AdmanViewRewind,
    AdmanViewRemainingTime,

    AdmanViewVoiceContainer,
    AdmanViewPositive,
    AdmanViewNegative,
    AdmanViewMicStatus,
    AdmanViewVoiceRequestProgress
};

@interface AdmanViewBase : NSObject
- (nullable UIView*)getView;
@end

@interface AdmanViewBaseViewController : AdmanViewBase

- (nullable UIViewController*)getView;
- (_Null_unspecified id)init:(UIViewController * _Nonnull)view;

@end

@interface AdmanViewBaseView : AdmanViewBase

- (nullable UIView*)getView;
- (_Null_unspecified id)init:(UIView * _Nonnull)view;

@end

@interface AdmanViewBaseButton : AdmanViewBase

- (nullable UIButton*)getView;
- (_Null_unspecified id)init:(UIButton * _Nonnull)view;

@end

@interface AdmanViewBaseLabel : AdmanViewBase

- (nullable UILabel*)getView;

- (_Null_unspecified id)init:(UILabel * _Nonnull)view;

@end

@interface AdmanViewBaseImageView : AdmanViewBase

- (nullable UIImageView*)getView;

- (_Null_unspecified id)init:(UIImageView * _Nonnull)view;

@end

@interface AdmanViewBind : NSObject

@property (strong) AdmanViewBase* _Nullable view;
@property AdmanViewElement type;

- (_Null_unspecified id)init:(AdmanViewElement)type withView:(AdmanViewBase* _Nullable)view;

@end

@protocol IAdmanViewBundle<NSObject>
- (bool)contains:(AdmanViewElement)type;
- (nullable AdmanViewBase*)get:(AdmanViewElement)type;
@end

/*
@interface AdmanViewBundle: NSObject<IAdmanViewBundle>
- (_Null_unspecified id)init:(NSMutableArray<AdmanViewBind *> * _Nonnull)bindings;
@end
*/

#endif /* AdmanViewType_h */
