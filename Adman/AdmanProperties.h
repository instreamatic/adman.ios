/**
 * Copyright 2021 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <CoreLocation/CoreLocation.h>

#import "AdmanCommon.h"

@interface AdmanSettings : NSObject<CLLocationManagerDelegate>
/**
 ID for advertisement
 */
@property (strong, atomic, readonly, nonnull) NSString *idfa;
/**
 flag which is used to indicate if ad tracking was enabled on the device
 */
@property (readonly) BOOL isTrackingEnabled;
@property (nonatomic) bool useGeolocationTargeting;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
/**
 Allow libAdman to setup sharedInstance of AVAudioSession for correct playback
 default: true
 */
@property (nonatomic) bool isAudioSessionSetupEnabled;
@property (nonatomic, nonnull) NSNumber *debugMode;

@property (nonatomic) BOOL dtmfAdsDetection;
/**
 Short radiostation name
 */
@property (nonatomic, nullable) NSString *dtmfStationKey;

@property (nonatomic) NSInteger siteId;
@property (nonatomic) NSInteger zoneId;
@property (nonatomic) NSInteger campaignId;
@property (nonatomic) NSInteger creativeId;

@property (nonatomic) AdmanType type;
@property (nonatomic) AdmanRegion region;
@property (nonatomic) AdmanFormat format;

@property (nonatomic) NSInteger maxAdBlockDuration;
@property (nonatomic) NSInteger maxAdsCount;
@property (nonatomic) AdmanAdSlot slot;
/**
 override ip sended to ad server
 */
@property (nullable) NSString *clientIP;
/**
 speech-to-text voice engine
 */
@property (nonatomic) AdmanRecognitionBackend backend;
@property (nonatomic) AdmanRecognitionBackendLocation recognitionBackendLocation;
@property (nonatomic) bool serverVoiceActivityDetection;
@property (nonatomic) bool clientVoiceActivityEnded;
/**
 default voice response delay value from VAST
 */
@property (nonatomic) float voiceResponseDelay;
@property (nonatomic) NSInteger serverTimeout;
/**
 user defined response delay value
 */
@property (nonatomic) float customVoiceResponseDelay;
@property (nonatomic) float customAdResponseTime;
@property (nonatomic, nonnull) NSMutableDictionary<NSString *, NSString *>*siteVariables;

@property (nonatomic, nullable) NSString *customAdServer;
@property (nonatomic, nullable) NSString *requestURL;

@property (nonatomic, nullable) NSDate *lastFetchTime;

- (void)enableGeolocationTargeting;
- (void)setServerTimeout:(NSInteger)delay;
- (void)setSiteVariable:(nonnull NSString *)key value:(nonnull NSString *)value;
@end
