/**
 * Copyright 2018 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <AdSupport/ASIdentifierManager.h>
#import <Foundation/Foundation.h>

#import "AdmanProperties.h"

@interface AdmanSettings()

@property CLLocationManager *clManager;

@end

@implementation AdmanSettings

- (id)init {
    self = [super init];
    if (self) {
        ASIdentifierManager *adIdentManager = [ASIdentifierManager sharedManager];
        _idfa = [[adIdentManager advertisingIdentifier] UUIDString];
        _isTrackingEnabled = [adIdentManager isAdvertisingTrackingEnabled];
        _isAudioSessionSetupEnabled = YES;

        _format = AdmanFormatAny;
        _type = AdmanTypeAny;
        _slot = AdmanAdSlotDefault;
        _backend = AdmanRecognitionBackendAuto;
        _recognitionBackendLocation = AdmanRecognitionBackendLocationAuto;
        _serverVoiceActivityDetection = false;
        _useGeolocationTargeting = NO;
        _voiceResponseDelay = -1.0;
        _customVoiceResponseDelay = 0;
        _customAdResponseTime = 0;
        _serverTimeout = 4;
        _siteVariables = [NSMutableDictionary dictionary];

        _longitude = -1;
        _latitude = 0;
    }
    return self;
}

- (void)setPreview:(AdmanDebugMode)mode {
    _debugMode = [NSNumber numberWithInteger:mode];
}

- (float)getResponseDelay {
    float delay = _voiceResponseDelay;
    if (_customVoiceResponseDelay != 0)
        delay = _customVoiceResponseDelay;
    return delay;
}

- (void)setResponseDelay:(float)delay {
    _customVoiceResponseDelay = delay * 1.0;
}

- (void)disableAudioSessionSetup {
    _isAudioSessionSetupEnabled = NO;
}

- (void)setResponseTime:(float)duration {
    _customAdResponseTime = duration;
}

- (void)setServerTimeout:(NSInteger)delay {
    _serverTimeout = delay;
}

- (float)getResponseTime {
    return _customAdResponseTime;
}

- (void)enableGeolocationTargeting {
    if (_useGeolocationTargeting || !_useGeolocationTargeting)
        return;

    _clManager = [[CLLocationManager alloc] init];
    _clManager.delegate = self;

    switch (CLLocationManager.authorizationStatus) {
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            break;
        case kCLAuthorizationStatusNotDetermined:
            [_clManager requestAlwaysAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            _useGeolocationTargeting = YES;
            [_clManager startUpdatingLocation];
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        _useGeolocationTargeting = YES;
        [manager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    _longitude = locations.lastObject.coordinate.longitude;
    _latitude = locations.lastObject.coordinate.latitude;
    [manager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (_longitude == -1 && _latitude == 0)
        _useGeolocationTargeting = false;
}

- (void)setSiteVariable:(nonnull NSString *)key value:(nonnull NSString *)value {
    [_siteVariables setObject:value forKey:key];
}

@end
