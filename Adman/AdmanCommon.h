/**
 * Copyright 2021 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

typedef NS_ENUM (NSUInteger, AdmanDebugMode) {
    /** test creative with image and audio */
    AdmanDebugBanner = 11,
    /** test creative with audio */
    AdmanDebugAudio = 12,
    /** test voice creative */
    AdmanDebugVoiceAd = 41,
    /** debug mode disabled */
    AdmanDebugNone = 0
};

typedef NS_ENUM (NSUInteger, AdmanType) {
    AdmanTypeAny = 1,
    AdmanTypeAudioOnly = 7,
    AdmanTypeAudioPlus = 6,
    AdmanTypeVoice = 12,
    AdmanTypeVoiceMultipleAds
};

typedef NS_ENUM (NSUInteger, AdmanRegion) {
    AdmanRegionEU = 0,
    AdmanRegionUS,
    AdmanRegionGlobal,
    AdmanRegionDemo,
    AdmanRegionDemoEu,
    AdmanRegionVoice,
    AdmanRegionIndia,
    AdmanRegionCustom
};

typedef NS_ENUM (NSUInteger, AdmanFormat) {
    AdmanFormatAny = 1,
    AdmanFormatSwiftSponsor = 2,
    AdmanFormatGameVibe = 3
};

typedef NS_ENUM(NSUInteger, AdmanAdSlot) {
    AdmanPreroll,
    AdmanMidroll,
    AdmanPostroll,
    AdmanAdSlotDefault
};

typedef NS_ENUM (NSUInteger, AdmanRecognitionBackend) {
    AdmanRecognitionBackendAuto = 0,
    AdmanRecognitionBackendYandex,
    AdmanRecognitionBackendMicrosoft,
    AdmanRecognitionBackendNuanceDev,
    AdmanRecognitionBackendHoundify,
    AdmanRecognitionBackendNuanceRelease
};

#define AdmanStatEventLoad @"load"
#define AdmanStatEventRequest @"request"
#define AdmanStatEventFetched @"fetched"

#define AdmanStatEventClickTracking @"clickTracking"
#define AdmanStatEventCreativeView @"creativeView"
#define AdmanStatEventImpression @"impression"
#define AdmanStatEventCanShow @"can_show"
#define AdmanStatEventStart @"start"
#define AdmanStatEventFirstQuartile @"firstQuartile"
#define AdmanStatEventMidpoint @"midpoint"
#define AdmanStatEventThirdQuartile @"thirdQuartile"
#define AdmanStatEventComplete @"complete"
#define AdmanStatEventPause @"pause"
#define AdmanStatEventRewind @"rewind"
#define AdmanStatEventResume @"resume"
#define AdmanStatEventMute @"mute"
#define AdmanStatEventUnmute @"unmute"
#define AdmanStatEventClose @"close"
#define AdmanStatEventError @"error"

#define AdmanStatEventSkip @"skip"
#define AdmanStatEventResponseNegative @"respose_negative"
#define AdmanStatEventResponseSilence @"respose_silence"
#define AdmanStatEventResponseUnknown @"respose_unknown"
#define AdmanStatEventResponseOpen @"respose_open"
#define AdmanStatEventResponseSkip @"respose_skip"
#define AdmanStatEventResponseSwearing @"respose_swearing"
#define AdmanStatEventResponseError @"respose_error"
#define AdmanStatEventResponseOpenBanner @"response_open_banner"
#define AdmanStatEventResponseOpenButton @"response_open_button"
#define AdmanStatEventResponseNegativeButton @"response_negative_button"

/**
 geo location for voice recognition server
 Auto - default value, replaced by VAST responseURL section if present.
 EU
 USA
 Demo - USA demo server
 */
typedef NS_ENUM(NSUInteger, AdmanRecognitionBackendLocation) {
    AdmanRecognitionBackendLocationAuto,
    AdmanRecognitionBackendLocationEU,
    AdmanRecognitionBackendLocationUSA,
    AdmanRecognitionBackendLocationDemo
};

@interface AdmanCreative : NSObject
/**
@property statistics<NSString*, NSArray*<NSString*>> stat data from Creative tracking
*/
@property (nonnull, strong) NSMutableDictionary<NSString *, NSMutableArray<NSString *> *> *tracking;

@property (nonnull) NSString *staticResource;
/**
 @property urlToNavigateOnClick link to open in internal browser
 */
@property (nullable) NSString *urlToNavigateOnClick;

@end

@interface AdmanStatRequest : NSObject

@property bool isReported;
@property (nullable) void(^ onComplete)(void);
@property (nullable) void(^ onFail)(NSError * _Nullable);

- (nonnull instancetype)initFromArray:(nonnull NSArray *)URIs;
- (void)addURL:(nonnull NSString *)URL;
- (void)report;

@end

@interface AdmanStats : NSObject

@property (nonnull) NSMutableArray<NSDate *> *badRequests;

- (nullable AdmanStatRequest *)valueForKey:(nonnull NSString *)key;

- (void)save:(nonnull NSString *)url forEvent:(nonnull NSString *)event;
- (void)saveFor:(nonnull NSString *)event point:(nonnull NSString *)point admanId:(NSInteger)admanId siteId:(NSInteger)siteId
       playerId:(NSInteger)playerId campaignId:(nullable NSString *)campaignId
       bannerId:(NSInteger)bannerId deviceId:(nullable NSString*)deviceId;

- (void)reportEvent:(nonnull NSString *)eventName;
- (void)reportEvent:(nonnull NSString *)event forCompanion:(nullable AdmanCreative *)companion;

- (void)setRegion:(AdmanRegion)region;
- (nonnull NSString *)getServer;

@end
