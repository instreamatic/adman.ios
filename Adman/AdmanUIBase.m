/**
* Copyright 2019 Instreamatic

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.

* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#import <SafariServices/SafariServices.h>

#import "AdmanUIBase.h"
#import "AdmanConstants.h"
#import "AdmanEvents.h"
#import "AdmanVastParserDelegate.h"

#define TAG @"AdmanUI"

@interface AdmanUIBase ()<SFSafariViewControllerDelegate>

@property (nonnull) AdmanBannerWrapper *ad;

@end

@implementation AdmanUIBase

- (id)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(admanPlayerNotification:) name:NOTIFICATION_ADMAN_PLAYER object:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(admanControlNotification:) name:NOTIFICATION_ADMAN_CONTROL object:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(admanVoiceNotification:) name:NOTIFICATION_ADMAN_VOICE object:nil];
    }
    return self;
}

+ (void)addSelector:(SEL)sel forView:(nonnull UIView *)view withTarget:(nonnull id)delegate {
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:delegate action:sel];
    tapper.numberOfTapsRequired = 1;
    tapper.numberOfTouchesRequired = 1;
    tapper.delegate = delegate;
    [view addGestureRecognizer:tapper];
    [view setUserInteractionEnabled:YES];
}

+ (UIImage *)loadImageFrom:(NSString *)base64 {
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:base64]]];
}

+ (void)setVisible:(bool)visibility view:(NSArray<UIView*>*)listView {
    for (UIView *viewItem in listView) {
        if (viewItem == nil) continue;
            [viewItem setHidden:!visibility];
    }
}

- (void)initBaseView {
    if (_rootVC == nil)
        return;

    // transparent background color
    self.view.backgroundColor = [UIColor blackColor];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    [self.view setFrame:CGRectMake(bounds.origin.x, bounds.origin.y, CGRectGetWidth(bounds), CGRectGetHeight(bounds))];

    [_rootVC.view addSubview:self.view];
    [_rootVC addChildViewController:self];
    [self didMoveToParentViewController:_rootVC];

    if (_rootVC.navigationController) {
        UIScreenEdgePanGestureRecognizer *gesture = (UIScreenEdgePanGestureRecognizer*)[_rootVC.navigationController.view.gestureRecognizers firstObject];
        if (gesture)
            [gesture addTarget:self action:@selector(onSwipeBack:)];
    }
    [self initBaseComponents];

    // Timer label
    if (_remainingTime != nil) {
        [_remainingTime setTextColor:[UIColor whiteColor]];
        [_remainingTime setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_remainingTime];
        [_remainingTime setFont:[UIFont fontWithName:@"Arial" size:25]];
        if (_ad)
            [self setTime:_ad.duration];
    }

    // play button
    if (_playView != nil) {
        [AdmanUIBase addSelector:@selector(playStateChanged:) forView:_playView withTarget:self];
        [self.view addSubview:_playView];
    }

    // rewind button
    if (_rewindView != nil) {
        [AdmanUIBase addSelector:@selector(playbackRewindEvent:) forView:_rewindView withTarget:self];
        [self.view addSubview:_rewindView];
    }

    if (_ad != nil && [_ad isVoiceAd])
        [self initVoiceView];

    // Banner image
    [self.view addSubview:_bannerView];
    if (_ad.url)
        [self update:self.ad];

    // Close button
    if (_closeView != nil) {
        [AdmanUIBase addSelector:@selector(onClose:) forView:_closeView withTarget:self];
        [self.view addSubview:_closeView];
        [_closeView setHidden:YES];
    }

    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChangedEvent:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:[UIDevice currentDevice]];

    if (_microphoneStatus != nil) {
        _microphoneStatus.backgroundColor = [UIColor redColor];
        [self.view addSubview:_microphoneStatus];
        [_microphoneStatus setHidden:YES];
    }
}

- (void)initBaseComponents {
    if (_remainingTime == nil)
        _remainingTime = [[UILabel alloc] init];

    if (_playView == nil)
        _playView = [[UIImageView alloc] initWithImage:[AdmanUIBase loadImageFrom:PlayButtonString]];

    if (_rewindView == nil)
        _rewindView = [[UIImageView alloc] initWithImage:[AdmanUIBase loadImageFrom:BackButtonString]];

    if (_bannerView == nil)
        _bannerView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 640, 640)];

    if (_closeView == nil)
        _closeView = [[UIImageView alloc] initWithImage:[AdmanUIBase loadImageFrom:CloseButtonString]];

    if (_microphoneStatus == nil)
        _microphoneStatus = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _rootVC.view.frame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
}

- (void)initVoiceView {
    UIImage *img = nil;
    NSMutableArray *animation = [NSMutableArray arrayWithCapacity:41];
    for (int i = 1; i <= 40; i++) {
        if (i < 10)
            img = [UIImage imageNamed:[NSString stringWithFormat:@"loading_1000%i.gif", i]];
        else
            img = [UIImage imageNamed:[NSString stringWithFormat:@"loading_100%i.gif", i]];
        if (img != nil)
            [animation addObject:img];
    }

    [self initVoiceComponents];

    if (_voiceRequestAnimation != nil) {
        [_voiceRequestAnimation setAnimationImages:animation];
        _voiceRequestAnimation.animationDuration = 1.0;
        _voiceRequestAnimation.animationRepeatCount = 0;
        [_voiceRequestAnimation startAnimating];
        [self.view addSubview:_voiceRequestAnimation];
        [_voiceRequestAnimation setHidden:YES];
    }

    if (_intentLabel != nil) {
        NSString *locale = (_ad.responseLang != nil) ? _ad.responseLang : [[NSLocale preferredLanguages] firstObject];
        if ([locale rangeOfString:@"ru"].location != NSNotFound) {
            [_intentLabel setText:@"Хотите узнать больше?"];
            [_intentLabel setFont:[UIFont fontWithName:@"Arial" size:17]];
        } else if ([locale rangeOfString:@"ja"].location != NSNotFound) {
            [_intentLabel setText:@"興味がありますか"];
            [_intentLabel setFont:[UIFont fontWithName:@"Arial" size:23]];
        } else {
            [_intentLabel setText:@"Interested?"];
            [_intentLabel setFont:[UIFont fontWithName:@"Arial" size:27]];
        }
        [_intentLabel setTextColor:[UIColor whiteColor]];
        [_intentLabel setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_intentLabel];
        [_intentLabel setHidden:YES];
    }

    // negative intent
    if (_negativeView != nil) {
        [AdmanUIBase addSelector:@selector(negativeIntent:) forView:_negativeView withTarget:self];
        [self.view addSubview:_negativeView];
        [_negativeView setHidden:YES];
    }

    // positive intent
    if (_positiveView != nil) {
        [AdmanUIBase addSelector:@selector(positiveIntent:) forView:_positiveView withTarget:self];
        [self.view addSubview:_positiveView];
        [_positiveView setHidden:YES];
    }
}

- (void)initVoiceComponents {
    UIImage *img = [UIImage imageNamed:@"loading_10001.gif"];

    if (_voiceRequestAnimation == nil)
        _voiceRequestAnimation = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, img.size.width, img.size.height)];

    if (_intentLabel == nil)
        _intentLabel = [[UILabel alloc] init];

    NSString *locale = (_ad.responseLang != nil) ? _ad.responseLang : [[NSLocale preferredLanguages] firstObject];
    NSString *buttonLocal = NegativeButtonString;
    if ([locale rangeOfString:@"ru"].location != NSNotFound)
        buttonLocal = NegativeButtonStringRu;
    else if ([locale rangeOfString:@"ja"].location != NSNotFound)
        buttonLocal = NegativeButtonStringJa;

    if (_negativeView == nil)
        _negativeView = [[UIImageView alloc] initWithImage:[AdmanUIBase loadImageFrom:buttonLocal]];

    buttonLocal = PositiveButtonString;
    if ([locale rangeOfString:@"ru"].location != NSNotFound)
        buttonLocal = PositiveButtonStringRu;
    else if ([locale rangeOfString:@"ja"].location != NSNotFound)
        buttonLocal = PositiveButtonStringJa;

    if (_positiveView == nil)
        _positiveView = [[UIImageView alloc] initWithImage:[AdmanUIBase loadImageFrom:buttonLocal]];
}

- (void)update:(AdmanBannerWrapper *)ad {
    _ad = ad;

    if (_ad != nil && [ad isVoiceAd] && _intentLabel == nil)
        [self initVoiceView];

    if (ad != nil && ad.bannerImage != nil) {
        [_bannerView setImage:ad.bannerImage];
        [AdmanUIBase addSelector:@selector(bannerTapped:) forView:_bannerView withTarget:self];
        [_bannerView setHidden:NO];
        [self rotateByOrientation];
    } else {
        [_bannerView setImage:nil];
        [_bannerView setHidden:YES];
    }

    [_closeView setHidden:YES];
}

#pragma mark common methods

- (void)show:(UIViewController *)rootVC {
    _rootVC = rootVC;

    [self initBaseView];
    [_rootVC.view addSubview:self.view];
    [self rotateByOrientation];
}

- (void)hide {
    if (![NSThread isMainThread])
        dispatch_async(dispatch_get_main_queue(), ^() {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        });
    else {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
    _intentLabel = nil;
    _ad = nil;
}

- (void)setTime:(NSInteger)time {
    if (_remainingTime == nil)
        return;
    
    long v = (time < 1) ? 0 : time;
    if (time > 9)
        [_remainingTime setText:[NSString stringWithFormat:@"00:%li", (long) v]];
    else
        [_remainingTime setText:[NSString stringWithFormat:@"00:0%li", (long) v]];
}

- (void)showCloseButton {
    if ([_ad isVoiceAd])
        return;
    
    if (self.closeView)
        [self.closeView setHidden:NO];
}

- (void)showPauseButton:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (tapGestureRecognizer == nil)
        [_playView removeGestureRecognizer:_playView.gestureRecognizers.firstObject];
    else
        [_playView removeGestureRecognizer:tapGestureRecognizer];

    [self.playView setImage:[AdmanUIBase loadImageFrom:PauseButtonString]];
    [AdmanUIBase addSelector:@selector(pauseStateChanged:) forView:_playView withTarget:self];
}

- (void)showPlayButton:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (tapGestureRecognizer == nil)
        [_playView removeGestureRecognizer:_playView.gestureRecognizers.firstObject];
    else
        [_playView removeGestureRecognizer:tapGestureRecognizer];

    [self.playView setImage:[AdmanUIBase loadImageFrom:PlayButtonString]];
    [_playView removeGestureRecognizer:tapGestureRecognizer];
    [AdmanUIBase addSelector:@selector(playStateChanged:) forView:_playView withTarget:self];
}

- (void)orientationChangedEvent:(NSNotification *)notify {
    [self rotateByOrientation];
}

- (void)rotateByOrientation {
    UIInterfaceOrientation toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) || [self.ad isVoiceAd])
        [self rotateVertical];
    else if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
        [self rotateHorizontal];
}

- (void)rotateVertical {
    CGRect bounds = [[UIScreen mainScreen] bounds];

     int screenWidth = CGRectGetWidth(bounds);
     int screenHeight = CGRectGetHeight(bounds);
     int spacerSize = screenWidth * UIControlsSpacerRatio;
     CGPoint screenCenter = CGPointMake(screenWidth / 2, screenHeight / 2);

     // banner
     if (_ad.url) {
         int side = screenWidth - spacerSize;
         if ([_ad isVoiceAd])
             side = screenWidth;
         CGPoint centerPoint = CGPointMake(side / 2, side / 2);
         CGRect bannerPoint = CGRectMake(screenCenter.x - centerPoint.x, screenCenter.y - centerPoint.y, side, side);
         if ([_ad isVoiceAd])
             bannerPoint.origin.y -= spacerSize * 2;
         [_bannerView setFrame:bannerPoint];
     }

     CGRect mainControlsRect;
     // play/pause buttons position
     mainControlsRect = CGRectMake(screenCenter.x - _playView.image.size.width / 2,
                             screenHeight - _playView.image.size.height * 2,
                             _playView.image.size.width,
                             _playView.image.size.height);
     [_playView setFrame:mainControlsRect];
     
     // back button
     CGRect rewind = CGRectMake(mainControlsRect.origin.x, mainControlsRect.origin.y,
                                _rewindView.image.size.width, _rewindView.image.size.height);
     rewind.origin.x -= (_rewindView.image.size.width + _playView.image.size.width * 1.2);
     [_rewindView setFrame:rewind];

     if (![_ad isVoiceAd]) {
         // close
         CGRect close = CGRectMake(screenWidth - _closeView.image.size.width * 1.6,
                                   [UIApplication sharedApplication].statusBarFrame.size.height * 1.1,
                                   _closeView.image.size.width,
                                   _closeView.image.size.height);
         [_closeView setFrame:close];
     } else {
         int buttonWidth = screenWidth / 2 - spacerSize * 3;
         int x = screenWidth / 2;
         // x, y, sizeX, sizeY
         mainControlsRect = CGRectMake(x - spacerSize - buttonWidth, screenHeight - spacerSize - buttonWidth / 2.19,
                                 buttonWidth, (int) buttonWidth / 2.19);
         [_negativeView setFrame:mainControlsRect];
         [_closeView setFrame:CGRectMake(screenWidth - spacerSize - 16, _bannerView.frame.origin.y + spacerSize, 16, 16)];

         NSString *locale = (_ad.responseLang != nil) ? _ad.responseLang : [[NSLocale preferredLanguages] firstObject];
         if ([locale rangeOfString:@"ru"].location != NSNotFound || [locale rangeOfString:@"ja"].location != NSNotFound)
             [_intentLabel setFrame:CGRectMake(screenWidth / 2 - 90, _negativeView.frame.origin.y - 32  - spacerSize * 2, 190, 32)];
         else
             [_intentLabel setFrame:CGRectMake(screenWidth / 2 - 70, _negativeView.frame.origin.y - 32  - spacerSize * 2, 140, 32)];

         mainControlsRect.origin.x = x + spacerSize;
         [_positiveView setFrame:mainControlsRect];
     }

     // timer
     if (![_ad isVoiceAd]) {
         mainControlsRect.origin.x += (_playView.image.size.width * 2);
     } else {
         mainControlsRect.origin.y = spacerSize * 2;
         mainControlsRect.origin.x = screenWidth / 2 - 25;
         [_voiceRequestAnimation setFrame:CGRectMake(screenWidth / 2 - _voiceRequestAnimation.frame.size.width / 2 + spacerSize / 3,
                                                  mainControlsRect.origin.y + spacerSize * 2 + _remainingTime.frame.size.height,
                                                  _voiceRequestAnimation.frame.size.width,
                                                  _voiceRequestAnimation.frame.size.height)];
     }
     mainControlsRect.size = CGSizeMake(100, 32);
     [_remainingTime setFrame:mainControlsRect];
}

- (void)rotateHorizontal {
    CGRect bounds = [[UIScreen mainScreen] bounds];

    int screenWidth = CGRectGetWidth(bounds);
    int screenHeight = CGRectGetHeight(bounds);
    int spacing = screenHeight * (UIControlsSpacerRatio * 2);

    // banner
    int side = screenHeight - spacing;
    CGRect bannerPoint = CGRectMake(0 + spacing / 2, 0 + spacing / 2, side, side);
    if (_ad.url) {
        if (screenHeight / 2 < bannerPoint.size.width) {
            int d = ((bannerPoint.size.width - screenWidth / 2) / 2);
            bannerPoint.size.width -= d;
            bannerPoint.size.height = bannerPoint.size.width;
            bannerPoint.origin.y += d / 2;
        }
        [_bannerView setFrame:bannerPoint];
    }

    CGRect control;
    if (![_ad isVoiceAd]) {
        // play/pause
        int xPos;
        if (_ad.url)
            xPos = screenWidth - (screenWidth - (bannerPoint.size.width + spacing)) / 1.8;
        else
            xPos = screenWidth / 2;
        control = CGRectMake(xPos,
                                    screenHeight - _playView.image.size.height * 2,
                                    _playView.image.size.width,
                                    _playView.image.size.height);
        [_playView setFrame:control];

        // back button
        CGRect rewind = CGRectMake(control.origin.x, control.origin.y + 4, _rewindView.image.size.width, _rewindView.image.size.height);
        rewind.origin.x -= (_rewindView.image.size.width + _rewindView.image.size.width * 1.2);
        [_rewindView setFrame:rewind];
    }

    // close button
    CGRect r = CGRectMake(screenWidth - _closeView.image.size.width * 1.7,
                          [UIApplication sharedApplication].statusBarFrame.size.height * 1.1,
                          _closeView.frame.size.width,
                          _closeView.frame.size.height);
    [_closeView setFrame:r];

    // timer
    if (![_ad isVoiceAd])
        control.origin.x += (_playView.image.size.width * 2);
    control.size = CGSizeMake(100, 32);
    [_remainingTime setFrame:control];

}

#pragma mark UI events

- (void)positiveIntent:(UITapGestureRecognizer *)tapGestureRecognizer {
    [NotificationAdmanControl sendEvent:AdmanControlEventPositive];
}

- (void)negativeIntent:(UITapGestureRecognizer *)tapGestureRecognizer {
    [NotificationAdmanControl sendEvent:AdmanControlEventNegative];
}

- (void)bannerTapped:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (self.ad.isClickable)
        [NotificationAdmanControl sendEvent:AdmanControlEventClick];
}

- (void)playStateChanged:(UITapGestureRecognizer *)tapGestureRecognizer {
    [NotificationAdmanControl sendEvent:AdmanControlEventStart];
    [self showPauseButton:tapGestureRecognizer];
}

- (void)pauseStateChanged:(UITapGestureRecognizer *)tapGestureRecognizer {
    [NotificationAdmanControl sendEvent:AdmanControlEventPause];
    [self showPlayButton:tapGestureRecognizer];
}

- (void)playbackRewindEvent:(UITapGestureRecognizer *)tapGestureRecognizer {
    [NotificationAdmanControl sendEvent:AdmanControlEventResume];
}

- (void)onSwipeBack:(id)sender {
    if (_rootVC == nil)
        return;
    [self hide];
    [NotificationAdmanControl sendEvent:AdmanControlEventSwipeBack];
}

- (void)onClose:(UITapGestureRecognizer *)tapGestureRecognizer {
    if ([_ad isVoiceAd]) {
        [AdmanUIBase setVisible:false view:[NSArray arrayWithObjects:_closeView, _bannerView, nil]];
    } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
        [NotificationAdmanControl sendEvent:AdmanControlEventSkip];
        [self hide];
    }
}

#pragma mark Adman notifications

- (void)admanPlayerNotification:(NSNotification *)notification {
    NSDictionary *dict = notification.userInfo;
    NotificationAdmanPlayer *message = [dict valueForKey:NOTIFICATION_ADMAN_PLAYER_KEY];
    if (message == nil)
        return;

    switch (message.event) {
        case AdmanPlayerEventPlaying: {
            NSLog(@"%@ triggerActionAdmanPlayer: %@", TAG, [NotificationAdmanPlayer getEventString:message.event]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self update:message.userData];
                [self showPauseButton:nil];
                [AdmanUIBase setVisible:false view:[NSArray arrayWithObjects:
                                                    self.negativeView,
                                                    self.positiveView,
                                                    self.intentLabel,
                                                    self.microphoneStatus,
                                                    self.voiceRequestAnimation, nil
                                                    ]];
            });
            break;
        }
        case AdmanPlayerEventReady:
            break;
        case AdmanPlayerEventPlay:
            [self showPauseButton:nil];
            break;
        case AdmanPlayerEventPause:
            [self showPlayButton:nil];
            break;
        case AdmanPlayerEventProgress:{
            NSNumber *estimetedTime = message.userData;
            [self setTime:[estimetedTime longValue]];

            if ([estimetedTime longValue] < 4)
                [self showCloseButton];
            break;
        }
        case AdmanPlayerEventComplete:{
            [self hide];
            break;
        }
        default:
            break;
    }
}

- (void)admanVoiceNotification:(NSNotification *)notification {
    NSDictionary *dict = notification.userInfo;
    NotificationAdmanVoice *message = [dict valueForKey:NOTIFICATION_ADMAN_VOICE_KEY];
    if (message == nil)
        return;

    NSLog(@"%@ triggerActionAdmanVoice: %@", TAG, [NotificationAdmanVoice getEventString:message.event]);
    switch (message.event) {
        case AdmanVoiceEventError:
        case AdmanVoiceEventResponsePlaying:{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.voiceRequestAnimation setHidden:YES];
            });
            break;
        }
        case AdmanVoiceEventInteractionStarted:{
            break;
        }
        case AdmanVoiceEventInteractionEnded:{
            break;
        }
        case AdmanVoiceEventRecognizerStarted:{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.playView setHidden:YES];
                [self.rewindView setHidden:YES];

                [AdmanUIBase setVisible:YES view:[NSArray arrayWithObjects:
                                                   self.microphoneStatus, nil
                                                   ]];
                if (self.ad.showControls)
                    [AdmanUIBase setVisible:YES view:[NSArray arrayWithObjects:
                                                       self.intentLabel,
                                                       self.positiveView,
                                                       self.negativeView, nil
                                                       ]];
            });
            break;
        }
        case AdmanVoiceEventRecognizerStopped:{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.playView setHidden:NO];
                [self.rewindView setHidden:NO];
                [self.voiceRequestAnimation setHidden:NO];

                [AdmanUIBase setVisible:NO view:[NSArray arrayWithObjects:
                                                    self.intentLabel,
                                                    self.positiveView,
                                                    self.negativeView,
                                                    self.microphoneStatus,
                                                    nil
                                                    ]];
            });
            break;
        }
        default:
            break;
    }
}


- (void)admanControlNotification:(NSNotification *)notification {
    NSDictionary *dict = notification.userInfo;
    NotificationAdmanControl *message = [dict valueForKey:NOTIFICATION_ADMAN_CONTROL_KEY];
    if (message == nil)
        return;

    switch (message.event) {
        case AdmanControlEventLink:{
            SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:message.userData];
            [_rootVC presentViewController:svc animated:YES completion:nil];
            break;
        }
        default:
            return;
    }
}

@end
