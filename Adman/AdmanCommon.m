/**
 * Copyright 2021 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import <AdSupport/ASIdentifierManager.h>

#import "AdmanCommon.h"
#import "AdmanEvents.h"

static NSString *const INSTREAMATIC_STAT_URL = @"https://%@.instreamatic.com/%@stat/%@.gif?v=%@";
static NSString *const INSTREAMATIC_LOGGING_URL = @"https://%@/admin/api/app-log";
static NSString *const INSTREAMATIC_MOBILE_IOS_VALUE = @"1";

#pragma mark RequestObject

@interface AdmanStatRequest ()

@property (nonnull) NSMutableArray<NSString *> *links;

@end

@implementation AdmanStatRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        self.links = [NSMutableArray arrayWithCapacity:2];
    }
    return self;
}

- (instancetype)initFromArray:(NSArray *)URIs {
    self = [super init];
    if (self) {
        self.links = [NSMutableArray arrayWithArray:URIs];
    }
    return self;
}

- (void)addURL:(NSString *)URL {
    NSCharacterSet *allowedCharacters = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *encodedURL = [URL stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    [self.links addObject:encodedURL];
}

- (void)report {
    if (self.isReported)
        return;

    for (NSInteger i = 0; i < self.links.count; i++) {
        [self request:[self.links objectAtIndex:i]];
    }
}

- (void)request:(NSString *)url {
    if (url == nil)
        return;
    if (self.isReported)
        return;

    @try {
        self.isReported = YES;
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.timeoutIntervalForRequest = 7.0;

        NSURL* requestURL = [NSURL URLWithString:url];
        NSURLSessionTask *task = [[NSURLSession sessionWithConfiguration:config] dataTaskWithURL:requestURL
                                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
            if (err == nil) {
                if (self.onComplete != nil)
                    self.onComplete();
            } else {
                if (self.onFail != nil)
                    self.onFail(err);
            }
        }];
        [task resume];
    } @catch (NSException *exception) {
        NSLog(@"Failed to report event for pixel: %@, recieved system error", url);
    } @finally {

    }
}

@end

#pragma mark Stat Interface

@interface AdmanStats ()
/**
 URLs for statistics
 */
@property (nonatomic,strong) NSMutableDictionary<NSString*, AdmanStatRequest *>* items;
@property (nonatomic) AdmanRegion region;

@end

@implementation AdmanStats

- (id)init {
    self = [super init];
    if (self) {
        _items = [[NSMutableDictionary alloc] init];
        _badRequests = [NSMutableArray arrayWithCapacity:4];

        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(admanPlayerNotification:) name:NOTIFICATION_ADMAN_PLAYER object:nil];
    }
    return self;
}

- (AdmanStatRequest *)valueForKey:(NSString *)key {
    return [_items valueForKey:key];
}

- (void)saveFor:(NSString *)event point:(NSString *)point admanId:(NSInteger)admanId siteId:(NSInteger)siteId playerId:(NSInteger)playerId campaignId:(NSString *)campaignId bannerId:(NSInteger)bannerId deviceId:(NSString*)deviceId {

    NSMutableDictionary *q = [[NSMutableDictionary alloc] init];
    [q setValue:INSTREAMATIC_MOBILE_IOS_VALUE forKey:@"m"];
    if (admanId) [q setValue:@(admanId).stringValue forKey:@"a"];
    if (siteId) [q setValue:@(siteId).stringValue forKey:@"s"];
    if (playerId) [q setValue:@(playerId).stringValue forKey:@"p"];
    if (campaignId) [q setValue:campaignId forKey:@"c"];
    if (bannerId) [q setValue:@(bannerId).stringValue forKey:@"b"];
    if (deviceId) [q setValue:deviceId forKey:@"i"];

    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:q options:0 error:&err];
    NSString *queryString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    NSData *data = [queryString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodedString = [[data base64EncodedStringWithOptions:0] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    NSString *url = [NSString stringWithFormat:INSTREAMATIC_STAT_URL, [self getServer],
                     (_region == AdmanRegionDemo) ? @"xs/" : @"", point, base64encodedString];
    NSURLComponents *components = [NSURLComponents componentsWithString:url];
    
    [self save:components.URL.absoluteString forEvent:event];
}

- (void)save:(NSString *)url forEvent:(NSString *)event {
    if ([self.items valueForKey:event] == nil)
        [self.items setValue:[[AdmanStatRequest alloc] init] forKey:event];

    [[self.items valueForKey:event] addURL:url];
}

- (void)reportEvent:(NSString *)eventName {
    AdmanStatRequest *request = [self.items valueForKey:eventName];
    NSLog(@"Reporting event %@", eventName);
    request.onComplete = ^() {
        NSLog(@"libAdman.stats: event %@ reported succefully", eventName);
    };
    request.onFail = ^(NSError *err) {
        NSLog(@"libAdman.stats: failed to report event %@, message: %@", eventName, err.localizedDescription);
    };
    [request report];
}

- (void)reportEvent:(NSString *)event forCompanion:(AdmanCreative *)companion {
    if (companion.tracking != nil && companion.tracking[event] != nil) {
        AdmanStatRequest *request = [[AdmanStatRequest alloc] initFromArray:companion.tracking[event]];
        [request report];
    }
}

- (NSString*)getServer {
    switch (_region) {
    case AdmanRegionGlobal:
        return @"xs3";
    case AdmanRegionVoice:
        return @"voice";
    case AdmanRegionDemo:
        return @"d1";
    default:
        return @"xs";
    }
}

- (void)setRegion:(AdmanRegion)region {
    _region = region;
}

#pragma mark Notifications

- (void)admanPlayerNotification:(NSNotification *)notification {
    NSDictionary *dict = notification.userInfo;
    NotificationAdmanPlayer *message = [dict valueForKey:NOTIFICATION_ADMAN_PLAYER_KEY];
    if (message == nil)
        return;

    switch (message.event) {
        case AdmanPlayerEventComplete:{
            for (NSString *key in self.items)
                [self.items valueForKey:key].isReported = NO;
            break;
        }
        default:
            break;
    }
}


@end
