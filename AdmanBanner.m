/**
 * Copyright 2021 Instreamatic
 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "AdmanBanner.h"
#import "Adman/AdmanVastParserDelegate.h"
#import "Adman/AdmanConstants.h"

@interface AdmanResponseValue ()

@end

@implementation AdmanResponseValue

- (id)initWithType:(NSString *)type andData:(NSString *)data
{
    self = [super init];
    if (self) {
        _type = type;
        _data = data;
    }
    return self;
}

@end

@interface AdmanInteraction ()

@end

@implementation AdmanInteraction

- (id)initWithAction:(NSString *)action andType:(NSString *)type
{
    self = [super init];
    if (self) {
        _action = action;
        _type = type;
        _values = [NSMutableArray array];
        _keywords = [NSMutableArray array];
    }
    return self;
}

@end

@implementation AdmanCreative

- (id)init {
    self = [super init];
    return self;
}

@end

@interface AdmanBanner()

@end

@implementation AdmanBanner

- (id)init {
    self = [super init];
    if (self) {
        _assets = [NSMutableDictionary dictionaryWithCapacity:5];
        _statData = [[AdmanStats alloc] init];
        _companionAds = [NSMutableDictionary dictionaryWithCapacity:2];
        _media = [NSMutableArray arrayWithCapacity:2];
        _expirationTime = AD_EXPIRATION_TIME_DEFAULT;
        _isClickable = TRUE;
        _showControls = FALSE;
    }
    return self;
}

- (bool)isVoiceAd {
    return _responseTime > 1;
}

@end
