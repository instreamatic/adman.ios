//
//  AdmanUiUITests.m
//  AdmanUiUITests
//
//  Created by test name on 12/13/16.
//  Copyright © 2016 Unisound. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Adman.h"

@interface AdmanUiUITests : XCTestCase

@property (strong) XCUIApplication *app;

@end

@implementation AdmanUiUITests

- (void)setUp {
    [super setUp];
    self.continueAfterFailure = NO;
    _app = [[XCUIApplication alloc] init];
    [_app launch];
}

- (void)tearDown {
    [super tearDown];
    [_app terminate];
}

- (void)testExample {

}

@end
